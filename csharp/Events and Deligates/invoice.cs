using System;

namespace BusinessObjects {
public class Invoice {
	private string _invoiceID;
	private System.DateTime _dateRaised;

	public event BusinessObjectEventHandler OnChanged;

	public Invoice() { }

	public string invoiceID {
		get {
			return this._invoiceID;
		}
		set {
			if (OnChanged != null)
				OnChanged(this, new BusinessObjectEventArgs("invoiceID", this._invoiceID, value));
			this._invoiceID = value;
		}
	}

	public System.DateTime dateRaised {
		get {
			return this._dateRaised;
		}
		set {
			if (OnChanged != null)
				OnChanged(this, new BusinessObjectEventArgs("dateRaised", this._dateRaised, value));
			this._dateRaised = value;
		}
	}

	public class BusinessObjectEventArgs : EventArgs {
		private string _propertyName;
		private object _oldValue;
		private object _newValue;

		public BusinessObjectEventArgs(string propertyName, object oldValue, object newValue) {
			this._propertyName = propertyName;
			this._oldValue = oldValue;
			this._newValue = newValue;
		}

		public string propertyName {
			get {
				return this._propertyName;
			}
		}

		public object oldValue {
			get {
				return this._oldValue;
			}
		}

		public object newValue {
			get {
				return this._newValue;
			}
		}

		public delegate void BusinessObjectEventHandler(object sender, BusinessObjectEventArgs e);
	}
}
}
