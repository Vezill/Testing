// A non usable example of how to use invoice. Example output:
// object 1 property dateRaised changed from 01/01/0001 00:00:00 to 21/10/2005 23:18:35
// object 1 property dateRaised changed from 01/01/0001 00:00:00 to 21/10/2005 23:18:35
using BusinessObjects;
using System.Diagnostics;
...

private void AnInvoiceClient() {
	BusinessObjects.Invoice i = new Invoice();
	i.invoiceID = "1";
	i.OnChanged += new BusinessObjectEventHandler(i_OnChanged1);
	i.OnChanged += new BusinessObjectEventHandler(i_OnChanged2);
	i.dateRaised = System.DateTime.Now;
}

private void i_OnChanged1(object sender, BusinessObjectEventArgs e) {
	string strMsg = "object {0} property {1} changed from {2} to {3}";
	BusinessObjects.Invoice i = (Invoice) sender;
	Debug.WriteLine(String.Format(strMsg, i.invoiceID, e.propertyName, e.oldValue, e.newValue));
}

private void i_OnChanged2(object sender, BusinessObjectEventArgs e) {
	string strMsg = "object {0} property {1} changed from {2} to {3}";
	BusinessObjects.Invoice i = (Invoice) sender;
	Debug.WriteLine(String.Format(strMsg, i.invoiceID, e.propertyName, e.oldValue, e.newValue));
}
