package shapes;


public abstract class Shape
{
    protected double width;
    protected double height;
    protected String name;

    public Shape(double width, double height)
    {
        this.width = width;
        this.height = height;
    }

    abstract public double getArea();

    final public void display()
    {
        System.out.println(String.format("This %s is %.2fx%.2f and has an area of %.2f",
            this.name, this.width, this.height, this.getArea()));
    }
}
