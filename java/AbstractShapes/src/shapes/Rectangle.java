package shapes;


public class Rectangle extends Shape
{
    public Rectangle(double width, double height)
    {
        super(width, height);
        this.name = "rectangle";
    }

    @Override
    public double getArea()
    {
        return (this.width * this.height);
    }
}
