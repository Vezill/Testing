import java.io.*;
import java.net.*;

import javax.swing.JOptionPane;


public class PacketServerProgram {
    public static int SERVER_PORT = 6000;
    private static int INPUT_BUFFER_LIMIT = 500;
    private int counter = 0;

    // handle all requests
    private void handleRequests() {
        System.out.println("SERVER online");

        // create a socket for communication
        DatagramSocket socket = null;

        try {
            socket = new DatagramSocket(SERVER_PORT);
        }
        catch (SocketException e) {
            JOptionPane.showMessageDialog(null, "SERVER: no network connection", "ERROR",
                                          JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }

        // now handle incoming requests
        while (true) {
            try {
                // wait for an incoming client request
                byte[] receive_buffer = new byte[INPUT_BUFFER_LIMIT];
                DatagramPacket receive_packet;
                receive_packet = new DatagramPacket(receive_buffer, receive_buffer.length);
                socket.receive(receive_packet);

                // extract the packet data that contains the request
                InetAddress address = receive_packet.getAddress();
                int client_port = receive_packet.getPort();
                String request = new String(receive_packet.getData(), 0, receive_packet.getLength());
                System.out.println("SERVER: Packet received: \"" + request + "\" from " + address
                                   + ": " + client_port);

                // decide what should be sent back to the client
                byte[] send_buffer;
                if (request.equals("What time is it ?")) {
                    System.out.println("SERVER: sending packet with time info");
                    sendResponse(socket, address, client_port, new java.util.Date().toString()
                                                                       .getBytes());
                    ++counter;
                }
                else if (request.equals("How many requests have you handled ?")) {
                    System.out.println("SERVER: sending packet with num requests");
                    sendResponse(socket, address, client_port, ("" + ++counter).getBytes());
                }
                else
                    System.out.println("SERVER: Unknown request: " + request);
            }
            catch (IOException e) {
                JOptionPane.showMessageDialog(null, "SERVER: Error receiving client requests",
                                              "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    // this helper method sends a given response back to the client
    private void sendResponse(DatagramSocket socket, InetAddress address, int client_port,
                              byte[] response) {
        try {
            // now create a packet to contain the response and send it
            DatagramPacket send_packet = new DatagramPacket(response, response.length, address,
                                                            client_port);
            socket.send(send_packet);
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, "SERVER: Error sending response to client" + address
                                          + ": " + client_port, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    public static void main(String[] args) {
        new PacketServerProgram().handleRequests();
    }
}
