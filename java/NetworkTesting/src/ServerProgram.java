import java.io.*;
import java.net.*;

import javax.swing.JOptionPane;


public class ServerProgram {
    public static int SERVER_PORT = 6000;
    private int counter = 0;

    // helper method to get the ServerSocket started
    private ServerSocket goOnline() {
        ServerSocket server_socket = null;

        try {
            server_socket = new ServerSocket(SERVER_PORT);
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, "SERVER: Error creating network connection.",
                                          "Error", JOptionPane.ERROR_MESSAGE);
        }

        System.out.println("SERVER online");
        return server_socket;
    }

    // handle all requests
    private void handleRequests(ServerSocket server_socket) {
        while (true) {
            Socket socket = null;
            BufferedReader in = null;
            PrintWriter out = null;

            try {
                // wait for an incoming client request
                socket = server_socket.accept();
                // at this point, a client connection has been made
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream());
            }
            catch (IOException e) {
                JOptionPane.showMessageDialog(null, "SERVER: Error connection to client", "ERROR",
                                              JOptionPane.ERROR_MESSAGE);
                System.exit(-1);
            }
            // read in the client's request
            try {
                String request = in.readLine();
                System.out.println("SERVER: Client Message Received: " + request);

                if (request.equals("What time is it ?")) {
                    out.println(new java.util.Date());
                    ++counter;
                }
                else if (request.equals("How many requests have you handled ?"))
                    out.println(counter++);
                else
                    System.out.println("SERVER: Unknown request: " + request);

                out.flush();  // now make sure that the response is sent
                socket.close();  // we are done with the client's request
            }
            catch (IOException e) {
                JOptionPane.showMessageDialog(null, "SERVER: Error communicating with client",
                                              "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public static void main(String[] args) {
        ServerProgram s = new ServerProgram();
        ServerSocket ss = s.goOnline();
        if (s != null)
            s.handleRequests(ss);
    }
}
