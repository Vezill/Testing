import java.io.*;
import java.net.*;

import javax.swing.*;


public class PacketClientProgram {
    private static int INPUT_BUFFER_LIMIT = 500;
    private InetAddress localhost;

    public PacketClientProgram() {
        try {
            localhost = InetAddress.getLocalHost();
        }
        catch (UnknownHostException e) {
            JOptionPane.showMessageDialog(null, "CLIENT: Error connecting to network", "ERROR",
                                          JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }
    }

    // ask the server for the current time
    private void askForTime() {
        DatagramSocket socket = null;
        try {
            socket = new DatagramSocket();
            byte[] send_buffer = "What time is it ?".getBytes();
            DatagramPacket send_packet = new DatagramPacket(send_buffer, send_buffer.length,
                                                            localhost,
                                                            PacketServerProgram.SERVER_PORT);
            System.out.println("CLIENT: Sending time request to server");
            socket.send(send_packet);
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, "CLIENT: Error sending time request to server",
                                          "ERROR", JOptionPane.ERROR_MESSAGE);
        }

        try {
            byte[] receive_buffer = new byte[INPUT_BUFFER_LIMIT];
            DatagramPacket receive_packet = new DatagramPacket(receive_buffer, receive_buffer.length);
            socket.receive(receive_packet);
            System.out.println("CLIENT: The time is " + new String(receive_packet.getData(), 0,
                                                                   receive_packet.getLength()));
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, "CLIENT: Cannot receive time from server", "ERROR",
                                          JOptionPane.ERROR_MESSAGE);
        }

        socket.close();
    }

    // ask the server for the number of requests obtained
    private void askForNumberOfRequests() {
        DatagramSocket socket = null;
        try {
            socket = new DatagramSocket();
            byte[] send_buffer = "How many requests have you handled ?".getBytes();
            DatagramPacket send_packet = new DatagramPacket(send_buffer, send_buffer.length,
                                                            localhost,
                                                            PacketServerProgram.SERVER_PORT);
            System.out.println("CLIENT: Sending request count to server");
            socket.send(send_packet);
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, "CLIENT: Error sending request to server", "ERROR",
                                          JOptionPane.ERROR_MESSAGE);
        }

        try {
            byte[] receive_buffer = new byte[INPUT_BUFFER_LIMIT];
            DatagramPacket receive_packet = new DatagramPacket(receive_buffer, receive_buffer.length);

            socket.receive(receive_packet);
            System.out.println("CLIENT: The number of requests are "
                               + new String(receive_packet.getData(), 0, receive_packet.getLength()));
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, "CLIENT: Cannot receive num requests from server",
                                          "ERROR", JOptionPane.ERROR_MESSAGE);
        }

        socket.close();
    }

    // ask the server to order a pizza
    private void askForPizza() {
        try {
            byte[] send_buffer = "Give me a pizza".getBytes();
            DatagramPacket send_packet = new DatagramPacket(send_buffer, send_buffer.length,
                                                            localhost,
                                                            PacketServerProgram.SERVER_PORT);
            DatagramSocket socket = new DatagramSocket();
            System.out.println("CLIENT: Sending pizza request to server");
            socket.send(send_packet);
            socket.close();
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, "CLIENT: Error sending request to server", "ERROR",
                                          JOptionPane.ERROR_MESSAGE);
        }
    }

    public static void main(String[] args) {
        PacketClientProgram c = new PacketClientProgram();
        c.askForTime();
        c.askForNumberOfRequests();
        c.askForPizza();
        c.askForTime();
        c.askForNumberOfRequests();
    }
}
