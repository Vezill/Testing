import java.net.*;
import java.io.*;

public class URLReaderProgram {
    public static void main(String[] args) {
        URL wiki = null;
        try {
            wiki = new URL("http", "en.wikipedia.org", 80, "/wiki/Computer_science");
            BufferedReader in = new BufferedReader(new InputStreamReader(wiki.openStream()));

            // now read the webpage file
            String line_of_webpage;
            while ((line_of_webpage = in.readLine()) != null)
                System.out.println(line_of_webpage);
            in.close();  // close the connection to the net
        }
        catch (MalformedURLException e) {
            System.out.println("Cannot find webpage " + wiki);
        }
        catch (IOException e) {
            System.out.println("Cannot read from webpage " + wiki);
        }
    }
}
