import java.io.*;
import java.net.*;

import javax.swing.JOptionPane;


public class ClientProgram {
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;

    // make a connection to the server
    private void connectToServer() {
        try {
            socket = new Socket(InetAddress.getLocalHost(), 6000);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream());
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, "CLIENT: Cannot connect to server", "ERROR",
                                          JOptionPane.ERROR_MESSAGE);
            System.exit(-1);
        }
    }

    // disconnect from the server
    private void disconnectFromServer() {
        try {
            socket.close();
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, "CLIENT: Cannot disconnect from server", "ERROR",
                                          JOptionPane.ERROR_MESSAGE);
        }
    }

    // ask the server for the current time
    private void askForTime() {
        connectToServer();
        out.println("What time is it ?");
        out.flush();

        try {
            String time = in.readLine();
            System.out.println("CLIENT: The time is " + time);
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, "CLIENT: Cannot receive time from server", "ERROR",
                                          JOptionPane.ERROR_MESSAGE);
        }
        disconnectFromServer();
    }

    // ask the server for the number of requests obtained
    private void askForNumberOfRequests() {
        connectToServer();
        out.println("How many requests have you handled ?");
        out.flush();
        int count = 0;

        try {
            count = Integer.parseInt(in.readLine());
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, "CLIENT: Cannot receive num requests from server",
                                          "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        System.out.println("CLIENT: The number of requests are " + count);
        disconnectFromServer();
    }

    // ask the server to order a pizza
    private void askForAPizza() {
        connectToServer();
        out.println("Give me a pizza");
        out.flush();
        disconnectFromServer();
    }

    public static void main(String[] args) {
        ClientProgram c = new ClientProgram();
        c.askForTime();
        c.askForNumberOfRequests();
        c.askForAPizza();
        c.askForTime();
        c.askForNumberOfRequests();
    }
}
