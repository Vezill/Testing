import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class URLConnectionReaderExample {
    public static void main(String[] args) {
        URL wiki = null;
        BufferedReader in = null;

        try {
            wiki = new URL("http", "en.wikipedia.org", 80, "/wiki/Computer_science");
        }
        catch (MalformedURLException e) {
            System.out.println("Cannot find webpage " + wiki);
            System.exit(-1);
        }

        try {
            URLConnection conn = wiki.openConnection();
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        }
        catch (IOException e) {
            System.out.println("Cannot connect to webpage " + wiki);
            System.exit(-1);
        }

        try {
            // now read the webpage file
            String line_of_webpage;
            while ((line_of_webpage = in.readLine()) != null)
                System.out.println(line_of_webpage);
            in.close();  // close the connection to the net
        }
        catch (IOException e) {
            System.out.println("Cannot read from webpage " + wiki);
        }
    }
}
