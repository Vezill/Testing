import java.io.*;
import java.net.*;

/**
 * Includes main method to get things going
 * Otherwise, this just prepares a new ServerSocket that
 * listens for new connections
 * and starts a new thread for each connection
 */
public class EchoServer {
	public void go(){
		try{
			ServerSocket serverSocket = new ServerSocket(5566);
			while(true){
				System.out.print("Listening for connections on port 5001... ");
				Socket client = serverSocket.accept();
				Thread t = new Thread(new EchoClientHandler(client));
				t.start();
				System.out.println("Connected - "+client.getInetAddress());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		EchoServer server = new EchoServer();
		server.go();
	}
}

/**
 * The Runnable job that takes in the new Socket
 */
class EchoClientHandler implements Runnable{

	private final BufferedReader reader; 
	private final PrintWriter output;
	private final Socket socket;
	private static final String MESSAGE = "ECHO... [?]\r\n";
	private static final String EXIT_MESSAGE = "Sad to see you go. Goodbye.\r\n";
	private static final String WELCOME_MESSAGE = "Welcome to the Echo Server. Type something to see it echoed back to you!\r\n";

	public EchoClientHandler(Socket incomingSocket) throws IOException{
		socket = incomingSocket;
		output = new PrintWriter(incomingSocket.getOutputStream());
		reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
	}

	public void run(){
		try{
			output.write(WELCOME_MESSAGE);
			output.flush();
			String line = null;
			while((line = reader.readLine()) != null){
				boolean quit = false;
				output.write(MESSAGE.replaceAll("\\?", line));
				if(line.trim().equalsIgnoreCase("exit")){
					output.write(EXIT_MESSAGE);
					quit = true;
				}
				output.flush();
				if(quit){
					break;
				}
			}
		}catch(Exception e){
			System.err.println("OUCH! "+e.getMessage());
		}finally{
			try{socket.close();}catch(Exception ee){}
		}
	}
}