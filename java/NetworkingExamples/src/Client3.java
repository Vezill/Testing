import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Client3 {
	public static void main(String[] args) 
	{
		String host;
		int port;
		
		
		if (args.length == 0)
		{
			host = "localhost";
			port = 5566;
		}else
		{
			host = args[0];
			port = Integer.parseInt(args[1]);
		}
		
			
			try{
				Socket s = new Socket(host,port);
				
				
				BufferedReader myInput = new BufferedReader(new InputStreamReader(s.getInputStream()));
				PrintStream myOutput = new PrintStream(s.getOutputStream());

				
				String S = myInput.readLine();
				System.out.println(S);
				
				// send a msg to server 
				myOutput.println("HELLO SERVER this is from Kaveh \n ");
				
				
				Scanner sc = new Scanner(System.in);
				String L = sc.nextLine();
				
				while (! L.equals("exit"))
				{
					// send a msg to server 
					myOutput.println(L);
					L = sc.nextLine();
				}
				
			
				s.close();
				System.out.println("client is exiting");
				
			}catch(Exception e)
			{
				System.out.println("Exception in listenSocket " + e);
				System.exit(1);
			}
		
		
		
	}
}
