import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Client1 {
	public static void main(String[] args)
	{
		String host;
		int port;


		if (args.length == 0)
		{
			host = "localhost";
			port = 9999;
		}else
		{
			host = args[0];
			port = Integer.parseInt(args[1]);
		}


			try{
				Socket s = new Socket(host,port);

				BufferedReader myInput = new BufferedReader(new InputStreamReader(s.getInputStream()));
				PrintStream myOutput = new PrintStream(s.getOutputStream());

				// send a msg to server
				myOutput.println("HELLO SERVER this is from Kaveh \n ");

				/*
				Scanner sc = new Scanner(System.in);
				String L = sc.nextLine();

				while (L !="exit")
				{
					// send a msg to server
					myOutput.println(L);
					L = sc.nextLine();
				}
				*/
				String buf = myInput.readLine();
				if (buf != null)
				{
					System.out.println("client recieved " + buf + " from the server!");
				}
				s.close();
				System.out.println("client is exiting");

			}catch(Exception e)
			{
				System.out.println("Exception in listenSocket " + e);
				System.exit(1);
			}



	}
}
