import java.io.*;
import java.net.*;

public class Server1 
{
	public static void main(String[] args) 
	{
		
			try{
				ServerSocket ss = new ServerSocket(9999);
				System.out.println("Server is waiting for an incoming connection on host="
						+InetAddress.getLocalHost().getCanonicalHostName()  
						+" port =" + ss.getLocalPort()
						);
				Socket s = ss.accept();

				// ok, got a connection
				//Lets use java.io.* .. to read and write from connection
				BufferedReader myInput = new BufferedReader(new InputStreamReader(s.getInputStream()));
				PrintStream myOutput = new PrintStream(s.getOutputStream());
				
				
				///attemp to read input from the stream
				String buf = myInput.readLine();
				
			    if (buf != null){
			    	System.out.println("server read: " + buf + " >>>");
			    	myOutput.println("GOT IT");
			    	
			    	
			    }
				
				ss.close();
				
		    	System.out.println("server is exiting");

				 
				
				

			}catch(Exception e)
			{
				System.out.println("Exception in listenSocket " + e);
				System.exit(1);
			}
	}
}
