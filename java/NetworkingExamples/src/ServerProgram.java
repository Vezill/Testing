import java.net.*; // all socket stuff is in here
import java.io.*;
import javax.swing.JOptionPane;
public class ServerProgram {
	public static int SERVER_PORT = 8001; // arbitrary, but above 1023
	private int counter = 0;
	// Helper method to get the ServerSocket started
	private ServerSocket goOnline() {
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(SERVER_PORT);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,
					"SERVER: Error creating network connection",
					"Error", JOptionPane.ERROR_MESSAGE);
		}
		System.out.println("SERVER online");
		return serverSocket;
	}
	// Handle all requests
	private void handleRequests(ServerSocket serverSocket) {
		while(true) {
			Socket socket = null;
			BufferedReader in = null;
			PrintWriter out = null;
			try {
				// Wait for an incoming client request
				socket = serverSocket.accept();
				// At this point, a client connection has been made
				in = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));
				out = new PrintWriter(socket.getOutputStream());
			} catch(IOException e) {
				JOptionPane.showMessageDialog(null,
						"SERVER: Error connecting to client",
						"Error", JOptionPane.ERROR_MESSAGE);
				System.exit(-1);
			}
			// Read in the client's request
			try {
				String request = in.readLine();
				System.out.println("SERVER: Client Message Received: " + request);
				if (request.equals("What Time is It ?")) {
					out.println(new java.util.Date());
					counter++;
				}
				else if (request.equals("How many requests have you handled ?"))
					out.println(counter++);
				else
					System.out.println("SERVER: Unknown request: " + request);
				out.flush(); // Now make sure that the response is sent
				socket.close(); // We are done with the client's request
			} catch(IOException e) {
				JOptionPane.showMessageDialog(null,
						"SERVER: Error communicating with client",
						"Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	public static void main (String[] args) 
	{
		ServerProgram s = new ServerProgram();
		ServerSocket ss = s.goOnline();
		if (s != null)
			s.handleRequests(ss);
	}
}