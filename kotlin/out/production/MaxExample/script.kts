// Module script for production
import kotlin.modules.*
fun project() {
    module("MaxExample") {
        sources += "C:/Users/ndonais/Dropbox/Programming/kotlin/Testing/MaxExample/src/maxindex/indexOfMax.kt"
        // Classpath
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/charsets.jar"
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/deploy.jar"
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/javaws.jar"
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/jce.jar"
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/jfr.jar"
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/jfxrt.jar"
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/jsse.jar"
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/management-agent.jar"
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/plugin.jar"
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/resources.jar"
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/rt.jar"
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/ext/access-bridge-64.jar"
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/ext/dnsns.jar"
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/ext/jaccess.jar"
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/ext/localedata.jar"
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/ext/sunec.jar"
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/ext/sunjce_provider.jar"
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/ext/sunmscapi.jar"
        classpath += "C:/Program Files/Java/jdk1.7.0_10/jre/lib/ext/zipfs.jar"
        // Output directory, commented out
        //         classpath += "C:/Users/ndonais/Dropbox/Programming/kotlin/Testing/out/production/MaxExample"
        classpath += "C:/Users/ndonais/Dropbox/Programming/kotlin/Testing/lib/kotlin-runtime.jar"
        // Java Source Roots
        classpath += "C:/Users/ndonais/Dropbox/Programming/kotlin/Testing/MaxExample/src"
        // External annotations
        annotationsPath += "C:/Applications/JetBrains/IntelliJ IDEA Community Edition 12.0.1/lib/jdkAnnotations.jar"
        annotationsPath += "C:/Users/ndonais/.IdeaIC12/config/plugins/Kotlin/kotlinc/lib/kotlin-jdk-annotations.jar"
    }
}
