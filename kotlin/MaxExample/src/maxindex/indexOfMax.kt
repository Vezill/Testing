package maxindex


import java.util.*
import kotlin.test.assertEquals


fun indexOfMax(a : IntArray) : Int?
{
    if (a.size == 0) return null

    var maxIndex = 0
    var maxNum = 0

    for (i in 0..a.size-1)
    {
        if (a[i] > maxNum)
        {
            maxNum = a[i]
            maxIndex = i
        }
    }

    return maxIndex
}


fun main(args : Array<String>)
{
    test(null)
    test(0, 0)
    test(1, -1, 0)
    test(0, -1, -2)
    test(1, 1, 3, 2, 1)
    test(2, 1, 3, 4, 1)
    test(2, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE)
    test(2, Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE)

    println("Success")
}


// HELPER FUNCTIONS
fun test(expected : Int?, vararg data : Int)
{
    val actual = indexOfMax(data)
    assertEquals(actual, expected, "\ndata = ${Arrays.toString(data)}\n" +
                 "indexOfMax(data) = ${actual}, but must be $expected ")
}
