#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import os
import time
import PCANBasic as PCB
from PySide import QtCore, QtGui

# defaults: 30 delay, 8 size
CHUNK_DELAY = 30
CHUNK_SIZE = 8

CHANNELS = {
    "USB1":PCB.PCAN_USBBUS1, "USB2":PCB.PCAN_USBBUS2,
    "USB3":PCB.PCAN_USBBUS3, "USB4":PCB.PCAN_USBBUS4,
    "USB5":PCB.PCAN_USBBUS5, "USB6":PCB.PCAN_USBBUS6,
    "USB7":PCB.PCAN_USBBUS7, "USB8":PCB.PCAN_USBBUS8,
    "PCI1":PCB.PCAN_PCIBUS1, "PCI2":PCB.PCAN_PCIBUS2,
    "PCI3":PCB.PCAN_PCIBUS3, "PCI4":PCB.PCAN_PCIBUS4,
    "PCI5":PCB.PCAN_PCIBUS5, "PCI6":PCB.PCAN_PCIBUS6,
    "PCI7":PCB.PCAN_PCIBUS7, "PCI8":PCB.PCAN_PCIBUS8,
    "PC1":PCB.PCAN_PCCBUS1,  "PC2":PCB.PCAN_PCCBUS2}

BAUDRATES = {
    "1m":PCB.PCAN_BAUD_1M,     "800k":PCB.PCAN_BAUD_800K,
    "500k":PCB.PCAN_BAUD_500K, "250k":PCB.PCAN_BAUD_250K,
    "125k":PCB.PCAN_BAUD_125K, "100k":PCB.PCAN_BAUD_100K,
    "95k":PCB.PCAN_BAUD_95K,   "83k":PCB.PCAN_BAUD_83K,
    "50k":PCB.PCAN_BAUD_50K,   "47k":PCB.PCAN_BAUD_47K,
    "33k":PCB.PCAN_BAUD_33K,   "20k":PCB.PCAN_BAUD_20K,
    "10k":PCB.PCAN_BAUD_10K,   "5k":PCB.PCAN_BAUD_5K}


class PCANDataThread(QtCore.QThread):

    data_update_signal = QtCore.Signal(str)
    connection_error_signal = QtCore.Signal(str)

    def __init__(self, pcb_object, handle):
        QtCore.QThread.__init__(self)

        self._pcan_object = pcb_object
        self._handle = handle
        self._connected = True

    def run(self):
        self.connected = True

        while self._connected:
            msg_string = ""
            result, msg, _ = self._pcan_object.Read(self._handle)

            if result == PCB.PCAN_ERROR_OK:
                if msg.ID == 0x6FF:
                    for i in range(len(msg.DATA)):
                        msg_string += chr(msg.DATA[i])

                    self.data_update_signal.emit(msg_string)

            elif result != PCB.PCAN_ERROR_QRCVEMPTY:
                self.connection_error_signal.emit(self._pcan_object.GetErrorText(result)[1])
                break

    def stop(self):
        self._connected = False


class FirmwareUpdateWidget(QtGui.QWidget):

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self._update_file = None
        self._connected = False

        self._settings = QtCore.QSettings("./settings.ini", QtCore.QSettings.IniFormat)
        self._settings.beginGroup("General")
        self._channel = self._settings.value("Channel", "USB1")
        self._baudrate = self._settings.value("BaudRate", "125k")
        self._settings.endGroup()

        self._pcan_object = PCB.PCANBasic()
        self._handle = CHANNELS[self._channel]
        self._read_position = 0

        self._data_thread = PCANDataThread(self._pcan_object, self._handle)
        self._data_thread.data_update_signal[str].connect(self.printThreadMessage)
        self._data_thread.connection_error_signal[str].connect(self.printThreadErrorMessage)

        self._send_chunk_timer = QtCore.QTimer()

        self.initUi()

    def initUi(self):
        self.ui_select_file_btn = QtGui.QPushButton("Select File")
        self.ui_connect_btn = QtGui.QPushButton("Connect")
        self.ui_send_update_btn = QtGui.QPushButton("Send Update")
        self.ui_close_btn = QtGui.QPushButton("Close Window")

        self.ui_send_update_btn.setEnabled(False)

        self.ui_output_browse = QtGui.QTextBrowser()
        self.ui_output_browse.ensureCursorVisible()

        self.ui_button_layout = QtGui.QVBoxLayout()
        self.ui_button_layout.addWidget(self.ui_select_file_btn)
        self.ui_button_layout.addWidget(self.ui_connect_btn)
        self.ui_button_layout.addWidget(self.ui_send_update_btn)
        self.ui_button_layout.addWidget(self.ui_close_btn)
        self.ui_button_layout.setAlignment(self.ui_send_update_btn, QtCore.Qt.AlignTop)
        self.ui_button_layout.setAlignment(self.ui_close_btn, QtCore.Qt.AlignBottom)

        self.ui_main_layout = QtGui.QHBoxLayout()
        self.ui_main_layout.addLayout(self.ui_button_layout)
        self.ui_main_layout.addWidget(self.ui_output_browse)
        self.setLayout(self.ui_main_layout)

        self.ui_select_file_btn.released.connect(self.selectFile)
        self.ui_connect_btn.released.connect(self.connectToDevice)
        self.ui_send_update_btn.released.connect(self.sendUpdateRequest)
        self.ui_close_btn.released.connect(self.close)

        self.resize(500, 160)
        self.setMinimumSize(500, 160)
        self.setWindowTitle("Updater")

    def selectFile(self):
        self._update_file, _ = QtGui.QFileDialog.getOpenFileName(self, "Select File",
            os.path.expanduser(r"~/Downloads"), "*.txt")

        if os.path.isfile(self._update_file):
            self.printMessageToWindow("File opened successfully.\n")

        else:
            self.printMessageToWindow("File could not be opened, try again.")
            self._update_file = None

        self.enableSendUpdateButton()

    def connectToDevice(self):
        result = self._pcan_object.Initialize(self._handle, BAUDRATES[self._baudrate])
        if result == PCB.PCAN_ERROR_OK:
            self._connected = True
            self._data_thread.start()

            self.printMessageToWindow("Successfully connected to battery packs.\n")
            self.ui_connect_btn.setText("Disconnect")
            self.ui_connect_btn.released.disconnect()
            self.ui_connect_btn.released.connect(self.disconnectFromDevice)

        else:
            self.printMessageToWindow("Error connecting to battery packs: '{0}'\n".format(
                self._pcan_object.GetErrorText(result)[1]))

        self.enableSendUpdateButton()

    def disconnectFromDevice(self):
        self._data_thread.stop()
        while self._data_thread.isRunning():
            pass

        result = self._pcan_object.Uninitialize(self._handle)
        if result == PCB.PCAN_ERROR_OK:
            self._connected = False
            self.printMessageToWindow("Successfully disconnected from battery packs.\n")
            self.ui_connect_btn.setText("Connect")
            self.ui_connect_btn.released.disconnect()
            self.ui_connect_btn.released.connect(self.connectToDevice)

        else:
            self.printMessageToWindow("Error disconnecting from battery packs: '{0}'\n".format(
                self._pcan_object.GetErrorText(result)[1]))

        self.enableSendUpdateButton()

    def sendUpdateRequest(self):
        wmsg = PCB.TPCANMsg()
        wmsg.ID = 0x000
        wmsg.LEN = 8
        wmsg.MSGTYPE = PCB.PCAN_MESSAGE_STANDARD
        for i in range(8):
            wmsg.DATA[i] = ord("upgrade0"[i])
            
        result = self._pcan_object.Write(self._handle, wmsg)
        if result == PCB.PCAN_ERROR_OK:
            self.printMessageToWindow("Update request successfully sent. Waiting for rdy...\n")

        else:
            self.printMessageToWindow("Error writing to battery packs: '{0}'\n".format(
                self._pcan_object.GetErrorText(result)[1]))
            
    def sendFileStart(self):
        if self._data_thread.isRunning():
            self._send_chunk_timer.singleShot(CHUNK_DELAY, self.sendFileChunk)

        else:
            self.printMessageToWindow("Error, not reading data from battery packs.\n")
            self.disconnectFromDevice()

    def sendFileFinish(self):
        self.ui_output_browse.insertPlainText("\n")
        self.printMessageToWindow("Success code received, the update was successful. Please wait "
            "for the battery pack to reboot.\n")

    def sendFileChunk(self):
        wmsg = PCB.TPCANMsg()
        wmsg.ID = 0x000
        wmsg.LEN = 8
        wmsg.MSGTYPE = PCB.PCAN_MESSAGE_STANDARD

        end_reached = False
        try:
            with open(self._update_file, "rb") as rfile:
                rfile.seek(self._read_position, 0)

                for i in range(CHUNK_SIZE):
                    for j in range(8):
                        self._read_position += 1

                        try:
                            data_byte = rfile.read(1)

                        except Exception as error:
                            self.printMessageToWindow("Error occurred while reading from file: "
                                "'{0}'".format(error))
                            return

                        if data_byte:
                            wmsg.DATA[j] = ord(data_byte)

                        else:
                            end_reached = True
                            break

                    if not end_reached:
                        for k in range(8):
                            print hex(wmsg.DATA[k]),
                        print
                        
                        result = self._pcan_object.Write(self._handle, wmsg)
                        if result != PCB.PCAN_ERROR_OK:
                            if self._data_thread.isRunning():
                                self.printMessageToWindow("Error writing update. Please check "
                                    "physical connection and update file before retrying.\n")
                            return

                    else:
                        break

                if not end_reached:
                    self._send_chunk_timer.singleShot(CHUNK_DELAY, self.sendFileChunk)

                else:
                    self.ui_output_browse.insertPlainText("\n")
                    self.printMessageToWindow("Finished sending update, please wait for success "
                        "code to confirm the update installed correctly.\n")

        except IOError as error:
            self.printMessageToWindow("Error reading data from the file.\n")
            print str(error)

    def printMessageToWindow(self, msg):
        self.ui_output_browse.insertPlainText("{0}: {1}".format(time.strftime("%H:%M"), msg))
        self.ui_output_browse.moveCursor(QtGui.QTextCursor.End)

    def printThreadMessage(self, msg):
        #print repr(msg)

        if "\x4F\x4B\x21" in msg:
            msg = msg[:5]

        self.ui_output_browse.insertPlainText("{0}".format(msg))
        self.ui_output_browse.moveCursor(QtGui.QTextCursor.End)

        if "\x52\x44\x59" in msg:
            self.sendFileStart()

        if "\x4F\x4B\x21" in msg:
            self.sendFileFinish()

        if "\x42\x4F\x4F\x54" in msg:
            self.ui_output_browse.insertPlainText("\n")
            self.printMessageToWindow("Battery Pack has rebooted, this window can now be closed.\n")

    def printThreadErrorMessage(self, msg):
        self.printMessageToWindow("Thread connection error: '{0}'\n".format(msg))

    def closeEvent(self, event):
        if self._connected:
            self.disconnectFromDevice()

        self._settings.beginGroup("General")
        self._settings.setValue("Channel", self._channel)
        self._settings.setValue("BaudRate", self._baudrate)
        self._settings.endGroup()

        event.accept()

    def enableSendUpdateButton(self):
        if self._connected and self._update_file:
            self._read_position = 0
            self.ui_send_update_btn.setEnabled(True)

        else:
            self.ui_send_update_btn.setEnabled(False)


if __name__ == "__main__":
    import sys

    app = QtGui.QApplication([])
    root = FirmwareUpdateWidget()
    root.show()

    sys.exit(app.exec_())