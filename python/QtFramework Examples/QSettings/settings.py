#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# settings.py

# imports
from PySide import QtCore, QtGui

settings = QtCore.QSettings("./settings.ini", QtCore.QSettings.IniFormat)

# Load values
settings.beginGroup("Group1")
someBool = settings.value("SomeBool", True)
someInt = settings.value("SomeInt", 5)
someFloat = settings.value("SomeFloat", 1.3)
someStr = settings.value("SomeStr", "string")
settings.endGroup()

settings.beginGroup("Group2")
someUni = settings.value("SomeUni", "unicode")
someTuple = settings.value("SomeTuple", ("one", "two"))
someList = settings.value("SomeList", ["three"])
someDict = settings.value("SomeDict", {"key":"value"})
settings.endGroup()

if not isinstance(someBool, bool):
    print "someBool %s is not a bool" % someBool, type(someBool)
if not isinstance(someInt, int):
    print "someInt %s is not an integer" % someInt, type(someInt)
if not isinstance(someFloat, float):
    print "someFloat %s is not a float" % someFloat, type(someFloat)
if not isinstance(someStr, str):
    print "someStr %s is not a string" % someStr, type(someStr)
if not isinstance(someUni, unicode):
    print "someUni %s is not unicode" % someUni, type(someUni)
if not isinstance(someTuple, tuple):
    print "someTuple %s is not a tuple" % someTuple, type(someTuple)
if not isinstance(someList, list):
    print "someList %s is not a list" % someList, type(someList)
if not isinstance(someDict, dict):
    print "someDict %s is not a dictionary" % someDict, type(someDict)

# Save values
settings.beginGroup("Group1")
settings.setValue("SomeBool", someBool)
settings.setValue("SomeInt", someInt)
settings.setValue("SomeFloat", someFloat)
settings.setValue("SomeStr", someStr)
settings.endGroup()

settings.beginGroup("Group2")
settings.setValue("SomeUni", someUni)
settings.setValue("SomeTuple", someTuple)
settings.setValue("SomeList", someList)
settings.setValue("SomeDict", someDict)
settings.endGroup()