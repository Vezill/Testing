#!/usr/bin/env python


#import sip
#sip.setapi('QString', 2)
from PySide import QtCore, QtGui
import dockwidgets_rc


class MainWindow(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)

        self.ui_input_txt = QtGui.QTextEdit()
        self.setCentralWidget(self.ui_input_txt)

        ##### Create Actions #####
        self.ui_new_act = QtGui.QAction(QtGui.QIcon(':/images/new.png'), '&New Letter', self,
            shortcut=QtGui.QKeySequence.New, statusTip='Create a new form letter',
            triggered=self.newLetter)

        self.ui_save_act = QtGui.QAction(QtGui.QIcon(':/images/save.png'), '&Save...', self,
            shortcut=QtGui.QKeySequence.Save, statusTip='Save the current form letter',
            triggered=self.save)

        self.ui_print_act = QtGui.QAction(QtGui.QIcon(':/images/print.png'), '&Print...', self,
            shortcut=QtGui.QKeySequence.Print, statusTip='Print the current form letter',
            triggered=self.print_)

        self.ui_undo_act = QtGui.QAction(QtGui.QIcon(':/images/undo.png'), '&Undo', self,
            shortcut=QtGui.QKeySequence.Undo, statusTip='Undo the last editing action',
            triggered=self.undo)

        self.ui_quit_act = QtGui.QAction('&Quit', self, shortcut='Ctrl+Q',
            statusTip='Quit the application', triggered=self.close)

        self.ui_about_act = QtGui.QAction('&About', self,
            statusTip="Show the application's About box", triggered=self.about)

        self.ui_about_qt_act = QtGui.QAction('About &Qt', self,
            statusTip="Show the Qt Library's About box", triggered=QtGui.qApp.aboutQt)

        ##### Create Menus #####
        self.ui_file_menu = self.menuBar().addMenu('&File')
        self.ui_file_menu.addAction(self.ui_new_act)
        self.ui_file_menu.addAction(self.ui_save_act)
        self.ui_file_menu.addAction(self.ui_print_act)
        self.ui_file_menu.addSeparator()
        self.ui_file_menu.addAction(self.ui_quit_act)

        self.ui_edit_menu = self.menuBar().addMenu('&Edit')
        self.ui_edit_menu.addAction(self.ui_undo_act)

        self.ui_view_menu = self.menuBar().addMenu('&View')

        self.menuBar().addSeparator()

        self.ui_help_menu = self.menuBar().addMenu('&Help')
        self.ui_help_menu.addAction(self.ui_about_act)
        self.ui_help_menu.addAction(self.ui_about_qt_act)

        ##### Create ToolBars #####
        self.ui_file_tbar = self.addToolBar('File')
        self.ui_file_tbar.addAction(self.ui_new_act)
        self.ui_file_tbar.addAction(self.ui_save_act)
        self.ui_file_tbar.addAction(self.ui_print_act)
        
        self.ui_edit_tbar = self.addToolBar('Edit')
        self.ui_edit_tbar.addAction(self.ui_undo_act)

        ##### Create StatusBar #####
        self.statusBar().showMessage('Ready')

        ##### Create Dock Windows #####
        ui_customer_dock = QtGui.QDockWidget('Customers', self)
        ui_customer_dock.setAllowedAreas(QtCore.Qt.LeftDockWidgetArea | QtCore.Qt.RightDockWidgetArea)
        self.ui_customer_list = QtGui.QListWidget(ui_customer_dock)
        self.ui_customer_list.addItems((
            "John Doe, Harmony Enterprises, 12 Lakeside, Ambleton",
            "Jane Doe, Memorabilia, 23 Watersedge, Beaton",
            "Tammy Shea, Tiblanka, 38 Sea Views, Carlton",
            "Tim Sheen, Caraba Gifts, 48 Ocean Way, Deal",
            "Sol Harvey, Chicos Coffee, 53 New Springs, Eccleston",
            "Sally Hobart, Tiroli Tea, 67 Long River, Fedula" ))
        ui_customer_dock.setWidget(self.ui_customer_list)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, ui_customer_dock)
        self.ui_view_menu.addAction(ui_customer_dock.toggleViewAction())

        ui_paragraph_dock = QtGui.QDockWidget('Paragraphs', self)
        self.ui_paragraph_list = QtGui.QListWidget(ui_paragraph_dock)
        self.ui_paragraph_list.addItems((
            "Thank you for your payment which we have received today.",
            "Your order has been dispatched and should be with you within 28 days.",
            "We have dispatched those items that were in stock. The rest of your order will be "
                "dispatched once all the remaining items have arrived at our warehouse. No "
                "additional shipping charges will be made.",
            "You made a small overpayment (less than $5) which we will keep on account for you, or "
                "return at your request.",
            "You made a small underpayment (less than $1), but we have sent your order anyway. "
                "We'll add this underpayment to your next bill.",
            "Unfortunately you did not send enough money. Please remit an additional $. Your order "
                "will be dispatched as soon as the complete amount has been received.",
            "You made an overpayment (more than $5). Do you wish to buy more items, or should we "
                "return the excess to you?" ))
        ui_paragraph_dock.setWidget(self.ui_paragraph_list)
        self.addDockWidget(QtCore.Qt.RightDockWidgetArea, ui_paragraph_dock)
        self.ui_view_menu.addAction(ui_paragraph_dock.toggleViewAction())

        self.ui_customer_list.currentTextChanged.connect(self.insertCustomer)
        self.ui_paragraph_list.currentTextChanged.connect(self.addParagraph)

        self.setWindowTitle('Dock Widgets')

        self.newLetter()

    def newLetter(self):
        self.ui_input_txt.clear()

        cursor = self.ui_input_txt.textCursor()
        cursor.movePosition(QtGui.QTextCursor.Start)
        top_frame = cursor.currentFrame()
        top_frame_format = top_frame.frameFormat()
        top_frame_format.setPadding(16)
        top_frame.setFrameFormat(top_frame_format)

        text_format = QtGui.QTextCharFormat()
        bold_format = QtGui.QTextCharFormat()
        bold_format.setFontWeight(QtGui.QFont.Bold)
        italic_format = QtGui.QTextCharFormat()
        italic_format.setFontItalic(True)

        table_format = QtGui.QTextTableFormat()
        table_format.setBorder(1)
        table_format.setCellPadding(16)
        table_format.setAlignment(QtCore.Qt.AlignRight)

        cursor.insertTable(1, 1, table_format)
        cursor.insertText('The Firm', bold_format)
        cursor.insertBlock()
        cursor.insertText('321 City Street', text_format)
        cursor.insertBlock()
        cursor.insertText('Industry Park')
        cursor.insertBlock()
        cursor.insertText('Some Country')
        cursor.setPosition(top_frame.lastPosition())
        cursor.insertText(QtCore.QDate.currentDate().toString('d MMMM yyyy'), text_format)
        cursor.insertBlock()
        cursor.insertBlock()
        cursor.insertText('Dear ', text_format)
        cursor.insertText('NAME', italic_format)
        cursor.insertText(',', text_format)
        for i in range(3):
            cursor.insertBlock()
        cursor.insertText('Yours sincerely,', text_format)
        for i in range(3):
            cursor.insertBlock()
        cursor.insertText('The Boss', text_format)
        cursor.insertBlock()
        cursor.insertText('ADDRESS', italic_format)

    def print_(self):
        document = self.ui_input_txt.document()
        printer = QtGui.QPrinter()

        dlg = QtGui.QPrintDialog(printer, self)
        if dlg.exec_() != QtGui.QDialog.Accepted:
            return

        document.print_(printer)

        self.statusBar().showMessage('Ready', 2000)

    def save(self):
        file_name, filter = QtGui.QFileDialog.getSaveFileName(self, 'Choose a file name', '.',
            'HTML (*.html *.htm)')
        if not file_name:
            return

        file = QtCore.QFile(file_name)
        if not file.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
            QtGui.QMessageBox.warning(self, 'Dock Widgets', 'Cannot write file {0}:{1}.'.format(
                file_name, file.errorString()))
            return

        out = QtCore.QTextStream(file)
        QtGui.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
        out << self.ui_input_txt.toHtml()
        QtGui.QApplication.restoreOverrideCursor()

        self.statusBar().showMessage("Saved '{0}'".format(file_name), 2000)

    def undo(self):
        document = self.ui_input_txt.document()
        document.undo()

    def insertCustomer(self, customer):
        if not customer:
            return

        customer_list = customer.split(', ')
        document = self.ui_input_txt.document()
        cursor = document.find('NAME')

        if not cursor.isNull():
            cursor.beginEditBlock()
            cursor.insertText(customer_list[0])
            old_cursor = cursor
            cursor = document.find('ADDRESS')

            if not cursor.isNull():
                for i in customer_list[1:]:
                    cursor.insertBlock()
                    cursor.insertText(i)
                cursor.endEditBlock()

            else:
                old_cursor.endEditBlock()

    def addParagraph(self, paragraph):
        if not paragraph:
            return

        document = self.ui_input_txt.document()
        cursor = document.find('Yours sincerely,')

        if cursor.isNull():
            return

        cursor.beginEditBlock()
        cursor.movePosition(QtGui.QTextCursor.PreviousBlock, QtGui.QTextCursor.MoveAnchor, 2)
        cursor.insertBlock()
        cursor.insertText(paragraph)
        cursor.insertBlock()
        cursor.endEditBlock()

    def about(self):
        QtGui.QMessageBox.about(self, "About Dock Widgets",
            "The <b>Dock Widgets</b> example demonstrates how to use Qt's dock widgets. You can "
            "enter your own text, click a customer to add a customer name and address, and click "
            "standard paragraphs to add them.")


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    root = MainWindow()
    root.show()

    sys.exit(app.exec_())
