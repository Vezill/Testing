#!/usr/bin/env python
# -*- encoding: utf-8 -*-

""" PySide port of the widgets/tetrix example from Qt v4.x """


import random
from PySide import QtCore, QtGui


NOSHAPE, ZSHAPE, SSHAPE, LINESHAPE, TSHAPE, SQUARESHAPE, LSHAPE, MIRROREDLSHAPE = range(8)


class TetrixWindow(QtGui.QWidget):
    def __init__(self):
        QtGui.QWidget.__init__(self)

        self.ui_board = TetrixBoard()

        ui_next_piece_lbl = QtGui.QLabel()
        ui_next_piece_lbl.setFrameStyle(QtGui.QFrame.Box | QtGui.QFrame.Raised)
        ui_next_piece_lbl.setAlignment(QtCore.Qt.AlignCenter)
        self.ui_board.setNextPieceLabel(ui_next_piece_lbl)

        ui_score_lcd = QtGui.QLCDNumber(5)
        ui_score_lcd.setSegmentStyle(QtGui.QLCDNumber.Filled)
        ui_level_lcd = QtGui.QLCDNumber(2)
        ui_level_lcd.setSegmentStyle(QtGui.QLCDNumber.Filled)
        ui_lines_lcd = QtGui.QLCDNumber(5)
        ui_lines_lcd.setSegmentStyle(QtGui.QLCDNumber.Filled)

        ui_start_btn = QtGui.QPushButton("&Start")
        ui_start_btn.setFocusPolicy(QtCore.Qt.NoFocus)
        ui_quit_btn = QtGui.QPushButton("&Quit")
        ui_quit_btn.setFocusPolicy(QtCore.Qt.NoFocus)
        ui_pause_btn = QtGui.QPushButton("&Pause")
        ui_pause_btn.setFocusPolicy(QtCore.Qt.NoFocus)

        ui_start_btn.clicked.connect(self.ui_board.start)
        ui_pause_btn.clicked.connect(self.ui_board.pause)
        ui_quit_btn.clicked.connect(QtGui.qApp.quit)
        self.ui_board.scoreChanged.connect(ui_score_lcd.display)
        self.ui_board.levelChanged.connect(ui_level_lcd.display)
        self.ui_board.linesRemovedChanged.connect(ui_lines_lcd.display)

        glayout = QtGui.QGridLayout()
        glayout.addWidget(self.createLabel("NEXT"), 0, 0)
        glayout.addWidget(ui_next_piece_lbl, 1, 0)
        glayout.addWidget(self.createLabel("LEVEL"), 2, 0)
        glayout.addWidget(ui_level_lcd, 3, 0)
        glayout.addWidget(ui_start_btn, 4, 0)
        glayout.addWidget(self.ui_board, 0, 1, 6, 1)
        glayout.addWidget(self.createLabel("SCORE"), 0, 2)
        glayout.addWidget(ui_score_lcd, 1, 2)
        glayout.addWidget(self.createLabel("LINES REMOVED"), 2, 2)
        glayout.addWidget(ui_lines_lcd, 3, 2)
        glayout.addWidget(ui_quit_btn, 4, 2)
        glayout.addWidget(ui_pause_btn, 5, 2)
        self.setLayout(glayout)

        self.setWindowTitle("Tetrix")
        self.resize(550, 370)

    def createLabel(self, text):
        lbl = QtGui.QLabel(text)
        lbl.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignBottom)
        return lbl


class TetrixBoard(QtGui.QFrame):
    BOARDWIDTH = 10
    BOARDHEIGHT = 22

    scoreChanged = QtCore.Signal(int)
    levelChanged = QtCore.Signal(int)
    linesRemovedChanged = QtCore.Signal(int)

    def __init__(self, parent=None):
        QtGui.QFrame.__init__(self, parent)

        self.timer = QtCore.QBasicTimer()
        self.ui_next_piece_lbl = None
        self.is_waiting_after_line = False
        self.current_piece = TetrixPiece()
        self.next_piece = TetrixPiece()
        self.current_x = 0
        self.current_y = 0
        self.number_lines_removed = 0
        self.number_pieces_dropped = 0
        self.score = 0
        self.level = 0
        self.ui_board = None

        self.setFrameStyle(QtGui.QFrame.Panel | QtGui.QFrame.Sunken)
        self.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.is_started = False
        self.is_paused = False
        self.clearBoard()

        self.next_piece.setRandomShape()

    def shapeAt(self, x, y):
        return self.ui_board[(y * TetrixBoard.BOARDWIDTH) + x]

    def setShapeAt(self, x, y, shape):
        self.ui_board[(y * TetrixBoard.BOARDWIDTH) + x] = shape

    def timeoutTime(self):
        return 1000 / (1 + self.level)

    def squareWidth(self):
        return self.contentsRect().width() / TetrixBoard.BOARDWIDTH

    def squareHeight(self):
        return self.contentsRect().height() / TetrixBoard.BOARDHEIGHT

    def setNextPieceLabel(self, lbl):
        self.ui_next_piece_lbl = lbl

    def sizeHint(self):
        return QtCore.QSize(TetrixBoard.BOARDWIDTH * 15 + self.frameWidth() * 2,
            TetrixBoard.BOARDHEIGHT * 15 + self.frameWidth() * 2)

    def minimumSizeHint(self):
        return QtCore.QSize(TetrixBoard.BOARDWIDTH * 5 + self.frameWidth() * 2,
            TetrixBoard.BOARDHEIGHT * 5 + self.frameWidth() * 2)

    def start(self):
        if self.is_paused:
            return

        self.is_started = True
        self.is_waiting_after_line = False
        self.number_lines_removed = 0
        self.number_pieces_dropped = 0
        self.score = 0
        self.level = 1
        self.clearBoard()

        self.linesRemovedChanged.emit(self.number_lines_removed)
        self.scoreChanged.emit(self.score)
        self.levelChanged.emit(self.level)

        self.newPiece()
        self.timer.start(self.timeoutTime(), self)

    def pause(self):
        if not self.is_started:
            return

        self.is_paused = not self.is_paused
        if self.is_paused:
            self.timer.stop()
        else:
            self.timer.start(self.timeoutTime(), self)

        self.update()

    def paintEvent(self, event):
        QtGui.QFrame.paintEvent(self, event)

        painter = QtGui.QPainter(self)
        rect = self.contentsRect()

        if self.is_paused:
            painter.drawText(rect, QtCore.Qt.AlignCenter, "Pause")
            return

        board_top = rect.bottom() - TetrixBoard.BOARDHEIGHT * self.squareHeight()

        for i in range(TetrixBoard.BOARDHEIGHT):
            for j in range(TetrixBoard.BOARDWIDTH):
                shape = self.shapeAt(j, TetrixBoard.BOARDHEIGHT - i - 1)

                if shape != NOSHAPE:
                    self.drawSquare(painter, rect.left() + j * self.squareWidth(),
                        board_top + i * self.squareHeight(), shape)

        if self.current_piece.shape() != NOSHAPE:
            for i in range(4):
                x = self.current_x + self.current_piece.x(i)
                y = self.current_y - self.current_piece.y(i)
                self.drawSquare(painter, rect.left() + x * self.squareWidth(),
                    board_top + (TetrixBoard.BOARDHEIGHT - y - 1) * self.squareHeight(),
                    self.current_piece.shape())

    def keyPressEvent(self, event):
        if not self.is_started or self.is_paused or self.current_piece.shape() == NOSHAPE:
            QtGui.QFrame.keyPressEvent(event)
            return

        key = event.key()
        if key == QtCore.Qt.Key_Left:
            self.tryMove(self.current_piece, self.current_x - 1, self.current_y)
        elif key == QtCore.Qt.Key_Right:
            self.tryMove(self.current_piece, self.current_x + 1, self.current_y)
        elif key == QtCore.Qt.Key_Down:
            self.tryMove(self.current_piece.rotatedRight(), self.current_x, self.current_y)
        elif key == QtCore.Qt.Key_Up:
            self.tryMove(self.current_piece.rotatedLeft(), self.current_x, self.current_y)
        elif key == QtCore.Qt.Key_Space:
            self.dropDown()
        elif key == QtCore.Qt.Key_D:
            self.oneLineDown()
        else:
            QtGui.QFrame.keyPressEvent(event)

    def timerEvent(self, event):
        if event.timerId() == self.timer.timerId():
            if self.is_waiting_after_line:
                self.is_waiting_after_line = False
                self.newPiece()
                self.timer.start(self.timeoutTime(), self)
            else:
                self.oneLineDown()

        else:
            QtGui.QFrame.timerEvent(event)

    def clearBoard(self):
        self.ui_board = [NOSHAPE for _ in range(TetrixBoard.BOARDHEIGHT * TetrixBoard.BOARDWIDTH)]

    def dropDown(self):
        drop_height = 0
        new_y = self.current_y

        while new_y > 0:
            if not self.tryMove(self.current_piece, self.current_x, new_y - 1):
                break

            new_y -= 1
            drop_height += 1

        self.pieceDropped(drop_height)

    def oneLineDown(self):
        if not self.tryMove(self.current_piece, self.current_x, self.current_y - 1):
            self.pieceDropped(0)

    def pieceDropped(self, drop_height):
        for i in range(4):
            x = self.current_x + self.current_piece.x(i)
            y = self.current_y - self.current_piece.y(i)
            self.setShapeAt(x, y, self.current_piece.shape())

        self.number_pieces_dropped += 1
        if self.number_pieces_dropped % 25 == 0:
            self.level += 1
            self.timer.start(self.timeoutTime(), self)
            self.levelChanged.emit(self.level)

        self.score += drop_height + 7
        self.scoreChanged.emit(self.score)
        self.removeFullLines()

        if not self.is_waiting_after_line:
            self.newPiece()

    def removeFullLines(self):
        number_full_lines = 0

        for i in range(TetrixBoard.BOARDHEIGHT - 1, -1, -1):
            line_is_full = True

            for j in range(TetrixBoard.BOARDWIDTH):
                if self.shapeAt(j, i) == NOSHAPE:
                    line_is_full = False
                    break

            if line_is_full:
                number_full_lines += 1
                for k in range(TetrixBoard.BOARDHEIGHT - 1):
                    for j in range(TetrixBoard.BOARDWIDTH):
                        self.setShapeAt(j, k, self.shapeAt(j, k + 1))

                for j in range(TetrixBoard.BOARDWIDTH):
                    self.setShapeAt(j, TetrixBoard.BOARDHEIGHT - 1, NOSHAPE)

        if number_full_lines > 0:
            self.number_lines_removed += number_full_lines
            self.score += 10 * number_full_lines
            self.linesRemovedChanged.emit(self.number_lines_removed)
            self.scoreChanged.emit(self.score)

            self.timer.start(500, self)
            self.is_waiting_after_line = True
            self.current_piece.setShape(NOSHAPE)
            self.update()

    def newPiece(self):
        self.current_piece = self.next_piece
        self.next_piece.setRandomShape()
        self.showNextPiece()
        self.current_x = TetrixBoard.BOARDWIDTH // 2 + 1
        self.current_y = TetrixBoard.BOARDHEIGHT - 1 + self.current_piece.minY()

        if not self.tryMove(self.current_piece, self.current_x, self.current_y):
            self.current_piece.setShape(NOSHAPE)
            self.timer.stop()
            self.is_started = False

    def showNextPiece(self):
        if self.ui_next_piece_lbl is not None:
            return

        dx = self.next_piece.maxX() - self.next_piece.minX() + 1
        dy = self.next_piece.maxY() - self.next_piece.minY() + 1

        pixmap = QtGui.QPixmap(dx * self.squareWidth(), dy * self.squareHeight())
        painter = QtGui.QPainter(pixmap)
        painter.fillRect(pixmap.rect(), self.ui_next_piece_lbl.palette().background())

        for i in range(4):
            x = self.next_piece.x(i) - self.next_piece.minX()
            y = self.next_piece.y(i) - self.next_piece.minY()
            self.drawSquare(painter, x * self.squareWidth(), y * self.squareHeight(),
                self.next_piece.shape())

        self.ui_next_piece_lbl.setPixmap(pixmap)

    def tryMove(self, new_piece, new_x, new_y):
        for i in range(4):
            x = new_x + new_piece.x(i)
            y = new_y - new_piece.y(i)

            if x < 0 or x >= TetrixBoard.BOARDWIDTH or y < 0 or y >= TetrixBoard.BOARDHEIGHT:
                return False
            if self.shapeAt(x, y) != NOSHAPE:
                return False

        self.current_piece = new_piece
        self.current_x = new_x
        self.current_y = new_y
        self.update()

        return True

    def drawSquare(self, painter, x, y, shape):
        color_table = [ 0x000000, 0xCC6666, 0x66CC66, 0x6666CC,
                        0xCCCC66, 0xCC66CC, 0x66CCCC, 0xDAAA00 ]

        color = QtGui.QColor(color_table[shape])
        painter.fillRect(x + 1, y + 1, self.squareWidth() - 2, self.squareHeight() - 2, color)

        painter.setPen(color.lighter())
        painter.drawLine(x, y + self.squareHeight() - 1, x , y)
        painter.drawLine(x, y, x + self.squareWidth() - 1, y)

        painter.setPen(color.darker())
        painter.drawLine(x + 1, y + self.squareHeight() - 1, x + self.squareWidth() - 1,
            y + self.squareHeight() - 1)
        painter.drawLine(x + self.squareWidth() - 1, y + self.squareHeight() - 1,
            x + self.squareWidth() - 1, y + 1)


class TetrixPiece(object):
    COORDSTABLE = ( (( 0, 0), ( 0, 0), ( 0, 0), ( 0, 0)),
                    (( 0,-1), ( 0, 0), (-1, 0), (-1, 1)),
                    (( 0,-1), ( 0, 0), ( 1, 0), ( 1, 1)),
                    (( 0,-1), ( 0, 0), ( 0, 1), ( 0, 2)),
                    ((-1, 0), ( 0, 0), ( 1, 0), ( 0, 1)),
                    (( 0, 0), ( 1, 0), ( 0, 1), ( 1, 1)),
                    ((-1,-1), ( 0,-1), ( 0, 0), ( 0, 1)),
                    (( 1,-1), ( 0,-1), ( 0, 0), ( 0, 1)) )

    def __init__(self):
        self.coords = [[0,0] for _ in range(4)]
        self.piece_shape = NOSHAPE

        self.setShape(NOSHAPE)

    def shape(self):
        return self.piece_shape

    def setShape(self, shape):
        table = TetrixPiece.COORDSTABLE[shape]
        for i in range(4):
            for j in range(2):
                self.coords[i][j] = table[i][j]

        self.piece_shape = shape

    def setRandomShape(self):
        self.setShape(random.randint(1, 7))

    def x(self, index):
        return self.coords[index][0]

    def y(self, index):
        return self.coords[index][1]

    def setX(self, index, x):
        self.coords[index][0] = x

    def setY(self, index, y):
        self.coords[index][1] = y

    def minX(self):
        m = self.coords[0][0]
        for i in range(4):
            m = min(m, self.coords[i][0])

        return m

    def maxX(self):
        m = self.coords[0][0]
        for i in range(4):
            m = max(m, self.coords[i][0])

        return m

    def minY(self):
        m = self.coords[0][1]
        for i in range(4):
            m = min(m, self.coords[i][1])

        return m

    def maxY(self):
        m = self.coords[0][1]
        for i in range(4):
            m = max(m, self.coords[i][1])

        return m

    def rotatedLeft(self):
        if self.piece_shape == SQUARESHAPE:
            return self

        result = TetrixPiece()
        result.piece_shape = self.piece_shape

        for i in range(4):
            result.setX(i, self.y(i))
            result.setY(i, -self.x(i))

        return result

    def rotatedRight(self):
        if self.piece_shape == SQUARESHAPE:
            return self

        result = TetrixPiece()
        result.piece_shape = self.piece_shape

        for i in range(4):
            result.setX(i, -self.y(i))
            result.setY(i, self.x(i))

        return result


if __name__ == "__main__":
    import sys

    app = QtGui.QApplication([])
    root = TetrixWindow()
    root.show()

    random.seed(None)
    sys.exit(app.exec_())
