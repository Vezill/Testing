#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from PySide import QtCore, QtGui


class AnalogClock(QtGui.QWidget):
    hour_hand = QtGui.QPolygon([QtCore.QPoint(7, 8), QtCore.QPoint(-7, 8), QtCore.QPoint(0, -40)])
    minute_hand = QtGui.QPolygon([QtCore.QPoint(7, 8), QtCore.QPoint(-7, 8), QtCore.QPoint(0, -70)])

    hour_color = QtGui.QColor(127, 0, 127)
    minute_color = QtGui.QColor(0, 127, 127, 191)

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update)
        timer.start(1000)

        self.setWindowTitle("Analog Clock")
        self.resize(200, 200)

    def paintEvent(self, event):
        side = min(self.width(), self.height())
        time = QtCore.QTime.currentTime()

        painter = QtGui.QPainter(self)
        painter.setRenderHint(QtGui.QPainter.Antialiasing)
        painter.translate(self.width() / 2, self.height() / 2)
        painter.scale(side / 200.0, side / 200.0)

        painter.setPen(QtCore.Qt.NoPen)
        painter.setBrush(AnalogClock.hour_color)

        painter.save()
        painter.rotate(30.0 * ((time.hour() + time.minute() / 60.0)))
        painter.drawConvexPolygon(AnalogClock.hour_hand)
        painter.restore()

        painter.setPen(AnalogClock.hour_color)

        for i in range(12):
            painter.drawLine(88, 0, 96, 0)
            painter.rotate(30.0)

        painter.setPen(QtCore.Qt.NoPen)
        painter.setBrush(AnalogClock.minute_color)

        painter.save()
        painter.rotate(6.0 * (time.minute() + time.second() / 60.0))
        painter.drawConvexPolygon(AnalogClock.minute_hand)
        painter.restore()

        painter.setPen(AnalogClock.minute_color)

        for i in range(60):
            if (i % 5) != 0:
                painter.drawLine(92, 0, 96, 0)
            painter.rotate(6.0)


if __name__ == "__main__":
    import sys

    app = QtGui.QApplication([])
    root = AnalogClock()
    root.show()

    sys.exit(app.exec_())
