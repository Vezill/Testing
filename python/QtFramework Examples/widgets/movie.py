#!/usr/bin/env python


from PySide import QtCore, QtGui


class MoviePlayer(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.movie = QtGui.QMovie(self)
        self.movie.setCacheMode(QtGui.QMovie.CacheAll)

        self.ui_movie_lbl = QtGui.QLabel("No Movie Loaded")
        self.ui_movie_lbl.setAlignment(QtCore.Qt.AlignCenter)
        self.ui_movie_lbl.setSizePolicy(QtGui.QSizePolicy.Ignored, QtGui.QSizePolicy.Ignored)
        self.ui_movie_lbl.setBackgroundRole(QtGui.QPalette.Dark)
        self.ui_movie_lbl.setAutoFillBackground(True)

        self.current_movie_directory = 'movies'

        self.createControls()
        self.createButtons()

        self.movie.frameChanged.connect(self.updateFrameSlider)
        self.movie.stateChanged.connect(self.updateButtons)
        self.ui_fit_chk.clicked.connect(self.fitToWindow)
        self.ui_frame_slide.valueChanged.connect(self.goToFrame)
        self.ui_speed_spn.valueChanged.connect(self.movie.setSpeed)

        ui_main_layout = QtGui.QVBoxLayout()
        ui_main_layout.addWidget(self.ui_movie_lbl)
        ui_main_layout.addLayout(self.ui_controls_layout)
        ui_main_layout.addLayout(self.ui_buttons_layout)
        self.setLayout(ui_main_layout)

        self.updateFrameSlider()
        self.updateButtons()

        self.setWindowTitle('Movie Player')
        self.resize(400, 400)

    def open(self):
        file_name = QtGui.QFileDialog.getOpenFileName(self, 'Open a Movie',
            self.current_movie_directory)[0]

        if file_name:
            self.openFile(file_name)

    def openFile(self, file_name):
        print 'file_name:', file_name
        self.current_movie_directory = QtCore.QFileInfo(file_name).path()

        self.movie.stop()
        self.ui_movie_lbl.setMovie(self.movie)
        self.movie.setFileName(file_name)
        self.movie.start()
        
        self.updateFrameSlider()
        self.updateButtons()
        
    def goToFrame(self, frame):
        self.movie.jumpToFrame(frame)
        
    def fitToWindow(self):
        self.ui_movie_lbl.setScaledContents(self.ui_fit_chk.isChecked())
        
    def updateFrameSlider(self):
        has_frames = (self.movie.currentFrameNumber() >= 0)

        if has_frames:
            if self.movie.frameCount() > 0:
                self.ui_frame_slide.setMaximum(self.movie.frameCount() - 1)
            elif self.movie.currentFrameNumber() > self.ui_frame_slide.maximum():
                self.ui_frame_slide.setMaximum(self.movie.currentFrameNumber())

            self.ui_frame_slide.setValue(self.movie.currentFrameNumber())

        else:
            self.ui_frame_slide.setMaximum(0)

        self.ui_movie_lbl.setEnabled(has_frames)
        self.ui_frame_slide.setEnabled(has_frames)

    def updateButtons(self):
        state = self.movie.state()

        self.ui_play_tbtn.setEnabled(self.movie.isValid() and self.movie.frameCount() != 1 and
            state == QtGui.QMovie.NotRunning)
        self.ui_pause_tbtn.setEnabled(state != QtGui.QMovie.NotRunning)
        self.ui_pause_tbtn.setChecked(state == QtGui.QMovie.Paused)
        self.ui_stop_tbtn.setEnabled(state != QtGui.QMovie.NotRunning)

    def createControls(self):
        self.ui_fit_chk = QtGui.QCheckBox('Fit to Window')
        self.ui_frame_lbl = QtGui.QLabel('Current Frame:')

        self.ui_frame_slide = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.ui_frame_slide.setTickPosition(QtGui.QSlider.TicksBelow)
        self.ui_frame_slide.setTickInterval(10)

        ui_speed_lbl = QtGui.QLabel('Speed:')

        self.ui_speed_spn = QtGui.QSpinBox()
        self.ui_speed_spn.setRange(1, 9999)
        self.ui_speed_spn.setValue(100)
        self.ui_speed_spn.setSuffix('%')

        self.ui_controls_layout = QtGui.QGridLayout()
        self.ui_controls_layout.addWidget(self.ui_fit_chk, 0, 0, 1, 2)
        self.ui_controls_layout.addWidget(self.ui_frame_lbl, 1, 0)
        self.ui_controls_layout.addWidget(self.ui_frame_slide, 1, 1, 1, 2)
        self.ui_controls_layout.addWidget(ui_speed_lbl, 2, 0)
        self.ui_controls_layout.addWidget(self.ui_speed_spn, 2, 1)

    def createButtons(self):
        icon_size = QtCore.QSize(36, 36)

        ui_open_tbtn = QtGui.QToolButton()
        ui_open_tbtn.setIcon(self.style().standardIcon(QtGui.QStyle.SP_DialogOpenButton))
        ui_open_tbtn.setIconSize(icon_size)
        ui_open_tbtn.clicked.connect(self.open)

        self.ui_play_tbtn = QtGui.QToolButton()
        self.ui_play_tbtn.setIcon(self.style().standardIcon(QtGui.QStyle.SP_MediaPlay))
        self.ui_play_tbtn.setIconSize(icon_size)
        self.ui_play_tbtn.setToolTip('Play')
        self.ui_play_tbtn.clicked.connect(self.movie.start)

        self.ui_pause_tbtn = QtGui.QToolButton()
        self.ui_pause_tbtn.setCheckable(True)
        self.ui_pause_tbtn.setIcon(self.style().standardIcon(QtGui.QStyle.SP_MediaPause))
        self.ui_pause_tbtn.setIconSize(icon_size)
        self.ui_pause_tbtn.setToolTip('Pause')
        self.ui_pause_tbtn.toggled.connect(self.movie.setPaused)

        self.ui_stop_tbtn = QtGui.QToolButton()
        self.ui_stop_tbtn.setIcon(self.style().standardIcon(QtGui.QStyle.SP_MediaStop))
        self.ui_stop_tbtn.setIconSize(icon_size)
        self.ui_stop_tbtn.setToolTip('Stop')
        self.ui_stop_tbtn.clicked.connect(self.movie.stop)

        ui_quit_tbtn = QtGui.QToolButton()
        ui_quit_tbtn.setIcon(self.style().standardIcon(QtGui.QStyle.SP_DialogCloseButton))
        ui_quit_tbtn.setIconSize(icon_size)
        ui_quit_tbtn.setToolTip('Quit')
        ui_quit_tbtn.clicked.connect(self.close)

        self.ui_buttons_layout = QtGui.QHBoxLayout()
        self.ui_buttons_layout.addStretch()
        self.ui_buttons_layout.addWidget(ui_open_tbtn)
        self.ui_buttons_layout.addWidget(self.ui_play_tbtn)
        self.ui_buttons_layout.addWidget(self.ui_pause_tbtn)
        self.ui_buttons_layout.addWidget(self.ui_stop_tbtn)
        self.ui_buttons_layout.addWidget(ui_quit_tbtn)
        self.ui_buttons_layout.addStretch()


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    root = MoviePlayer()
    root.show()
    sys.exit(app.exec_())
