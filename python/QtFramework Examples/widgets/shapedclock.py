#!/usr/bin/env python


from PySide import QtCore, QtGui


class ShapedClock(QtGui.QWidget):
    hour_hand = QtGui.QPolygon([
        QtCore.QPoint(7, 8),
        QtCore.QPoint(-7, 8),
        QtCore.QPoint(0, -40)
    ])

    minute_hand = QtGui.QPolygon([
        QtCore.QPoint(7, 8),
        QtCore.QPoint(-7, 8),
        QtCore.QPoint(0, -70)
    ])

    hour_color = QtGui.QColor(127, 0, 127)
    minute_color = QtGui.QColor(0, 127, 127, 191)

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent, QtCore.Qt.FramelessWindowHint |
            QtCore.Qt.WindowSystemMenuHint)

        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.update)
        timer.start(1000)

        ui_quit_act = QtGui.QAction('E&xit', self, shortcut='Ctrl+Q', triggered=QtGui.qApp.quit)
        self.addAction(ui_quit_act)

        self.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        self.setToolTip('Drag the clock with the left mouse button.\nUse the right mouse button to '
            'open a context menu.')
        self.setWindowTitle('Shaped Analog Clock')

    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self.drag_position = event.globalPos() - self.frameGeometry().topLeft()
            event.accept()

    def mouseMoveEvent(self, event):
        if event.buttons() == QtCore.Qt.LeftButton:
            self.move(event.globalPos() - self.drag_position)
            event.accept()

    def PaintEvent(self, event):
        side = min(self.width(), self.height())
        time = QtCore.QTime.currentTime()

        painter = QtGui.QPainter(self)
        painter.setRenderHints(QtGui.QPainter.Antialiasing)
        painter.translate(self.width() / 2, self.height() / 2)
        painter.scale(side / 200., side / 200.)

        painter.setPen(QtCore.Qt.NoPen)
        painter.setBrush(ShapedClock.hour_color)

        painter.save()
        painter.rotate(30. * ((time.hour() + time.minute() / 60.)))
        painter.drawConvexPolygon(ShapedClock.hour_hand)
        painter.restore()

        painter.setPen(ShapedClock.hour_color)
        for i in range(12):
            painter.drawLine(88, 0, 96, 0)
            painter.rotate(30.)

        painter.setPen(QtCore.Qt.NoPen)
        painter.setBrush(ShapedClock.minute_color)

        painter.save()
        painter.rotate(6. * (time.minute() + time.second() / 60.))
        painter.drawConvexPolygon(ShapedClock.minute_hand)
        painter.restore()

        painter.setPen(ShapedClock.minute_color)
        for i in range(60):
            if (i % 5) != 0:
                painter.drawLine(92, 0, 96, 0)
            painter.rotate(6.)

    def resizeEvent(self, event):
        side = min(self.width(), self.height())

        masked_region = QtGui.QRegion(self.width() / 2 - side / 2, self.height() / 2 - side / 2,
            side, side, QtGui.QRegion.Ellipse)
        self.setMask(masked_region)

    def sizeHint(self):
        return QtCore.QSize(100, 100)


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    root = ShapedClock()
    root.show()

    sys.exit(app.exec_)
