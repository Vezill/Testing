#!/usr/bin/env python


from PySide import QtCore, QtGui


class WigglyWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.setBackgroundRole(QtGui.QPalette.Midlight)
        self.setAutoFillBackground(True)

        new_font = self.font()
        new_font.setPointSize(new_font.pointSize() + 20)
        self.setFont(new_font)

        self.timer = QtCore.QBasicTimer()
        self.text = ''

        self.step = 0
        self.timer.start(60, self)

    def paintEvent(self, event):
        sine_table = (0, 38, 71, 92, 100, 92, 71, 38, 0, -38, -71, -92, -100, -92, -71, -38)

        metrics = QtGui.QFontMetrics(self.font())
        x = (self.width() - metrics.width(self.text)) / 2
        y = (self.height() + metrics.ascent() - metrics.descent()) / 2
        color = QtGui.QColor()

        painter = QtGui.QPainter(self)

        for i, ch in enumerate(self.text):
            index = (self.step + i) % 16
            color.setHsv((15 - index) * 16, 255, 191)
            painter.setPen(color)
            painter.drawText(x, y - ((sine_table[index] * metrics.height()) / 400), ch)
            x += metrics.width(ch)

    def setText(self, new_text):
        self.text = new_text

    def timerEvent(self, event):
        if event.timerId() == self.timer.timerId():
            self.step += 1
            self.update()
        else:
            QtGui.QWidget.timerEvent(self, event)


class Dialog(QtGui.QDialog):
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)

        ui_wiggly_widget = WigglyWidget()
        ui_text_edit = QtGui.QLineEdit()

        ui_main_layout = QtGui.QVBoxLayout()
        ui_main_layout.addWidget(ui_wiggly_widget)
        ui_main_layout.addWidget(ui_text_edit)
        self.setLayout(ui_main_layout)

        ui_text_edit.textChanged.connect(ui_wiggly_widget.setText)
        ui_text_edit.setText('Hello World!')

        self.setWindowTitle('Wiggly')
        self.resize(360, 145)


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    root = Dialog()
    root.show()
    sys.exit(app.exec_())
