#!/usr/bin/env python
# -*- encoding: utf-8 -*-


from PySide import QtCore, QtGui


class DigitalClock(QtGui.QLCDNumber):
    def __init__(self, parent=None):
        QtGui.QLCDNumber.__init__(self, parent)

        self.setSegmentStyle(QtGui.QLCDNumber.Filled)

        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.showTime)
        timer.start(1000)

        self.showTime()

        self.setWindowTitle("Digital Clock")
        self.resize(150, 60)

    def showTime(self):
        time = QtCore.QTime.currentTime()
        text = time.toString("hh:mm")
        if (time.second() % 2) == 0:
            text = text[:2] + " " + text[3:]

        self.display(text)


if __name__ == "__main__":
    import sys

    app = QtGui.QApplication([])
    root = DigitalClock()
    root.show()

    sys.exit(app.exec_())
