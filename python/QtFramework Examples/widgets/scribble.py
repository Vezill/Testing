#!/usr/bin/env python


from PySide import QtCore, QtGui


class ScribbleArea(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.setAttribute(QtCore.Qt.WA_StaticContents)
        self.modified = False
        self.scribbling = False
        self.my_pen_width = 1
        self.my_pen_color = QtCore.Qt.blue
        self.image = QtGui.QImage()
        self.last_point = QtCore.QPoint()

    def openImage(self, file_name):
        loaded_image = QtGui.QImage()
        if not loaded_image.load(file_name):
            return False

        new_size = loaded_image.size().expandedTo(self.size())
        self.resizeImage(loaded_image, new_size)
        self.image = loaded_image
        self.modified = False
        self.update()

        return True

    def saveImage(self, file_name, file_format):
        visible_image = self.image
        self.resizeImage(visible_image, size())

        if visible_image.save(file_name, file_format):
            self.modified = False
            return True
        else:
            return False

    def setPenColor(self, new_color):
        self.my_pen_color = new_color

    def setPenWidth(self, new_width):
        self.my_pen_width = new_width

    def clearImage(self):
        self.image.fill(QtGui.qRgb(255, 255, 255))
        self.modified = True
        self.update()

    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self.last_point = event.pos()
            self.scribbling = True

    def mouseMoveEvent(self, event):
        if (event.buttons() & QtCore.Qt.LeftButton) and self.scribbling:
            self.drawLineTo(event.pos())

    def mouseReleaseEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton and self.scribbling:
            self.drawLineTo(event.pos())
            self.scribbling = False

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.drawImage(QtCore.QPoint(0, 0), self.image)

    def resizeEvent(self, event):
        if self.width() > self.image.width() or self.height() > self.image.height():
            new_width = max(self.width() + 128, self.image.width())
            new_height = max(self.height() + 128, self.image.height())
            self.resizeImage(self.image, QtCore.QSize(new_width, new_height))
            self.update()

        QtGui.QWidget.resizeEvent(self, event)

    def drawLineTo(self, end_point):
        painter = QtGui.QPainter(self.image)
        painter.setPen(QtGui.QPen(self.my_pen_color, self.my_pen_width, QtCore.Qt.SolidLine,
            QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin))
        painter.drawLine(self.last_point, end_point)
        self.modified = True

        rad = self.my_pen_width / 2 + 2
        self.update(QtCore.QRect(self.last_point, end_point).normalized().adjusted(-rad, -rad, +rad,
            +rad))
        self.last_point = QtCore.QPoint(end_point)

    def resizeImage(self, image, new_size):
        if image.size() == new_size:
            return

        new_image = QtGui.QImage(new_size, QtGui.QImage.Format_RGB32)
        new_image.fill(QtGui.qRgb(255, 255, 255))
        painter = QtGui.QPainter(new_image)
        painter.drawImage(QtCore.QPoint(0, 0), image)
        self.image = new_image

    def print_(self):
        printer = QtGui.QPrinter(QtGui.QPrinter.HighResolution)

        print_dialog = QtGui.QPrintDialog(printer, self)
        if print_dialog.exec_() == QtGui.QDialog.Accepted:
            painter = QtGui.QPainter(printer)
            rect = painter.viewport()
            size = self.image.size()
            size.scale(rect.size(), QtCore.Qt.KeepAspectRatio)
            painter.setViewport(rect.x(), rect.y(), size.width(), size.height())
            painter.setWindow(self.image.rect())
            painter.drawImage(0, 0, self.image)

    def isModified(self):
        return self.modified

    def penColor(self):
        return self.my_pen_color

    def penWidth(self):
        return self.my_pen_width


class MainWindow(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)

        self.save_as_acts = []

        self.ui_scribble_widget = ScribbleArea()
        self.setCentralWidget(self.ui_scribble_widget)

        self.createActions()
        self.createMenus()

        self.setWindowTitle('Scribble')
        self.resize(500, 500)

    def closeEvent(self, event):
        if self.maybeSave():
            event.accept()
        else:
            event.ignore()

    def open(self):
        if self.maybeSave():
            file_name, _ = QtGui.QFileDialog.getOpenFileName(self, 'Open File',
                QtCore.QDir.currentPath())

            if file_name:
                self.ui_scribble_widget.openImage(file_name)

    def save(self):
        action = self.sender()
        file_format = action.data()
        self.saveFile(file_format)

    def penColor(self):
        new_color = QtGui.QColorDialog.getColor(self.ui_scribble_widget.penColor())
        if new_color.isValid():
            self.ui_scribble_widget.setPenColor(new_color)

    def penWidth(self):
        new_width, ok = QtGui.QInputDialog.getInteger(self, 'Scribble', 'Select Pen Width:',
            self.ui_scribble_widget.penWidth(), 1, 50, 1)
        if ok:
            self.ui_scribble_widget.setPenWidth(new_width)

    def about(self):
        QtGui.QMessageBox.about(self, "About Scribble",
            "<p>The <b>Scribble</b> example shows how to use "
            "QMainWindow as the base widget for an application, and how "
            "to reimplement some of QWidget's event handlers to receive "
            "the events generated for the application's widgets:</p>"
            "<p> We reimplement the mouse event handlers to facilitate "
            "drawing, the paint event handler to update the application "
            "and the resize event handler to optimize the application's "
            "appearance. In addition we reimplement the close event "
            "handler to intercept the close events before terminating "
            "the application.</p>"
            "<p> The example also demonstrates how to use QPainter to "
            "draw an image in real time, as well as to repaint "
            "widgets.</p>")

    def createActions(self):
        self.ui_open_act = QtGui.QAction('&Open...', self, shortcut='Ctrl+O', triggered=self.open)

        for format in QtGui.QImageWriter.supportedImageFormats():
            text = str(format.toUpper() + '...')
            action = QtGui.QAction(text, self, triggered=self.save)
            action.setData(format)
            self.save_as_acts.append(action)

        self.ui_print_act = QtGui.QAction('&Print...', self,
            triggered=self.ui_scribble_widget.print_)

        self.ui_exit_act = QtGui.QAction('E&xit', self, shortcut='Ctrl+Q', triggered=self.close)

        self.ui_pen_color_act = QtGui.QAction('&Pen Color...', self, triggered=self.penColor)

        self.ui_pen_width_act = QtGui.QAction('Pen &Width...', self, triggered=self.penWidth)

        self.ui_clear_act = QtGui.QAction('&Clear Screen', self, shortcut='Ctrl+L',
            triggered=self.ui_scribble_widget.clearImage)

        self.ui_about_act = QtGui.QAction('&About', self, triggered=self.about)
        self.ui_about_qt_act = QtGui.QAction('About &Qt', self, triggered=QtGui.qApp.aboutQt)

    def createMenus(self):
        self.ui_save_as_menu = QtGui.QMenu('&Save As', self)
        for action in self.save_as_acts:
            self.ui_save_as_menu.addAction(action)

        ui_file_menu = QtGui.QMenu('&File', self)
        ui_file_menu.addAction(self.ui_open_act)
        ui_file_menu.addMenu(self.ui_save_as_menu)
        ui_file_menu.addAction(self.ui_print_act)
        ui_file_menu.addSeparator()
        ui_file_menu.addAction(self.ui_exit_act)

        ui_option_menu = QtGui.QMenu('&Options', self)
        ui_option_menu.addAction(self.ui_pen_color_act)
        ui_option_menu.addAction(self.ui_pen_width_act)
        ui_option_menu.addSeparator()
        ui_option_menu.addAction(self.ui_clear_act)

        ui_help_menu = QtGui.QMenu('&Help', self)
        ui_help_menu.addAction(self.ui_about_act)
        ui_help_menu.addAction(self.ui_about_qt_act)

        self.menuBar().addMenu(ui_file_menu)
        self.menuBar().addMenu(ui_option_menu)
        self.menuBar().addMenu(ui_help_menu)

    def maybeSave(self):
        if self.ui_scribble_widget.isModified():
            ret = QtGui.QMessageBox.warning(self, 'Scribble', 'The image has been modified.\n'
                'Do you want to save your changed?', QtGui.QMessageBox.Save |
                QtGui.QMessageBox.Discard | QtGui.QMessageBox.Cancel)

            if ret == QtGui.QMessageBox.Save:
                return self.saveFile('png')
            elif ret == QtGui.QMessageBox.Cancel:
                return False

        return True

    def saveFile(self, file_format):
        initial_path = QtCore.QDir.currentPath() + '/untitled.' + file_format

        file_name, _ = QtGui.QFileDialog.getSaveFileName(self, 'Save As', initial_path,
            '{0} Files (*.{1});;All Files (*)'.format(str(file_format).upper(), file_format))

        if file_name:
            return self.ui_scribble_widget.saveImage(file_name, file_format)

        return False


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    root = MainWindow()
    root.show()

    sys.exit(app.exec_())
