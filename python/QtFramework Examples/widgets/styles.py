#!/usr/bin/env python


from PySide import QtCore, QtGui


class WidgetGallery(QtGui.QDialog):
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)

        self.original_palette = QtGui.QApplication.palette()

        ui_style_combo = QtGui.QComboBox()
        ui_style_combo.addItems(QtGui.QStyleFactory.keys())

        ui_style_lbl = QtGui.QLabel('&Style')
        ui_style_lbl.setBuddy(ui_style_combo)

        self.ui_use_style_chk = QtGui.QCheckBox("&Use Style's Standard Palette")
        self.ui_use_style_chk.setChecked(True)

        ui_disable_widgets_chk = QtGui.QCheckBox('&Disable Widgets')

        self.createTopLeftGroupBox()
        self.createTopRightGroupBox()
        self.createBottomLeftTabWidget()
        self.createBottomRightGroupBox()
        self.createProgressBar()

        ui_style_combo.activated[str].connect(self.changeStyle)
        self.ui_use_style_chk.toggled.connect(self.changePalette)
        ui_disable_widgets_chk.toggled.connect(self.ui_top_left_grp.setDisabled)
        ui_disable_widgets_chk.toggled.connect(self.ui_top_right_grp.setDisabled)
        ui_disable_widgets_chk.toggled.connect(self.ui_bottom_left_tab.setDisabled)
        ui_disable_widgets_chk.toggled.connect(self.ui_bottom_right_grp.setDisabled)

        ui_top_layout = QtGui.QHBoxLayout()
        ui_top_layout.addWidget(ui_style_lbl)
        ui_top_layout.addWidget(ui_style_combo)
        ui_top_layout.addStretch(1)
        ui_top_layout.addWidget(self.ui_use_style_chk)
        ui_top_layout.addWidget(ui_disable_widgets_chk)

        ui_main_layout = QtGui.QGridLayout()
        ui_main_layout.addLayout(ui_top_layout, 0, 0, 1, 2)
        ui_main_layout.addWidget(self.ui_top_left_grp, 1, 0)
        ui_main_layout.addWidget(self.ui_top_right_grp, 1, 1)
        ui_main_layout.addWidget(self.ui_bottom_left_tab, 2, 0)
        ui_main_layout.addWidget(self.ui_bottom_right_grp, 2, 1)
        ui_main_layout.addWidget(self.ui_style_prog, 3, 0, 1, 2)
        ui_main_layout.setRowStretch(1, 1)
        ui_main_layout.setRowStretch(2, 1)
        ui_main_layout.setColumnStretch(0, 1)
        ui_main_layout.setColumnStretch(1, 1)
        self.setLayout(ui_main_layout)

        self.setWindowTitle('Styles')
        self.changeStyle('Windows')

    def changeStyle(self, style_name):
        QtGui.QApplication.setStyle(QtGui.QStyleFactory.create(style_name))
        self.changePalette()

    def changePalette(self):
        if (self.ui_use_style_chk.isChecked()):
            QtGui.QApplication.setPalette(QtGui.QApplication.style().standardPalette())
        else:
            QtGui.QApplication.setPalette(self.original_palette)

    def advanceProgressBar(self):
        cur_val = self.ui_style_prog.value()
        max_val = self.ui_style_prog.maximum()
        self.ui_style_prog.setValue(cur_val + (max_val - cur_val) / 100)
        
    def createTopLeftGroupBox(self):
        self.ui_top_left_grp = QtGui.QGroupBox('Group 1')
        
        ui_radio1_rbtn = QtGui.QRadioButton('Radio Button 1')
        ui_radio2_rbtn = QtGui.QRadioButton('Radio Button 2')
        ui_radio3_rbtn = QtGui.QRadioButton('Radio Button 3')
        ui_radio1_rbtn.setChecked(True)
        
        ui_tristate_chk = QtGui.QCheckBox('Tri-State Check Box')
        ui_tristate_chk.setTristate(True)
        ui_tristate_chk.setCheckState(QtCore.Qt.PartiallyChecked)
        
        ui_layout = QtGui.QVBoxLayout()
        ui_layout.addWidget(ui_radio1_rbtn)
        ui_layout.addWidget(ui_radio2_rbtn)
        ui_layout.addWidget(ui_radio3_rbtn)
        ui_layout.addWidget(ui_tristate_chk)
        ui_layout.addStretch(1)
        
        self.ui_top_left_grp.setLayout(ui_layout)
        
    def createTopRightGroupBox(self):
        self.ui_top_right_grp = QtGui.QGroupBox('Group 2')
        
        ui_default_btn = QtGui.QPushButton('Default Push Button')
        ui_default_btn.setDefault(True)
        
        ui_toggle_btn = QtGui.QPushButton('Toggle Push Button')
        ui_toggle_btn.setCheckable(True)
        ui_toggle_btn.setChecked(True)
        
        ui_flat_btn = QtGui.QPushButton('Flat Push Button')
        ui_flat_btn.setFlat(True)
        
        ui_layout = QtGui.QVBoxLayout()
        ui_layout.addWidget(ui_default_btn)
        ui_layout.addWidget(ui_toggle_btn)
        ui_layout.addWidget(ui_flat_btn)
        ui_layout.addStretch(1)
        
        self.ui_top_right_grp.setLayout(ui_layout)
        
    def createBottomLeftTabWidget(self):
        self.ui_bottom_left_tab = QtGui.QTabWidget()
        self.ui_bottom_left_tab.setSizePolicy(QtGui.QSizePolicy.Preferred, 
            QtGui.QSizePolicy.Ignored)
        
        ui_tab1_widget = QtGui.QWidget()
        ui_tab1_table = QtGui.QTableWidget(10, 10)
        
        ui_tab1_layout = QtGui.QHBoxLayout()
        ui_tab1_layout.setContentsMargins(5, 5, 5, 5)
        ui_tab1_layout.addWidget(ui_tab1_table)
        ui_tab1_widget.setLayout(ui_tab1_layout)
        
        ui_tab2_widget = QtGui.QWidget()
        ui_tab2_txt = QtGui.QTextEdit()
        
        ui_tab2_txt.setPlainText('Twinkle, twinkle, little star,\n'
                                 'How I wonder what you are.\n'
                                 'Up above the world so high,\n'
                                 'Like a diamond in the sky.\n'
                                 'Twinkle, twinkle, little star,\n'
                                 'How I wonder what you are!\n')
        
        ui_tab2_layout = QtGui.QHBoxLayout()
        ui_tab2_layout.setContentsMargins(5, 5, 5, 5)
        ui_tab2_layout.addWidget(ui_tab2_txt)
        ui_tab2_widget.setLayout(ui_tab2_layout)

        self.ui_bottom_left_tab.addTab(ui_tab1_widget, '&Table')
        self.ui_bottom_left_tab.addTab(ui_tab2_widget, 'Text &Edit')

    def createBottomRightGroupBox(self):
        self.ui_bottom_right_grp = QtGui.QGroupBox('Group 3')
        self.ui_bottom_right_grp.setCheckable(True)
        self.ui_bottom_right_grp.setChecked(True)

        ui_line_edit = QtGui.QLineEdit('s3cRe7')
        ui_line_edit.setEchoMode(QtGui.QLineEdit.Password)

        ui_spin_spn = QtGui.QSpinBox(self.ui_bottom_right_grp)
        ui_spin_spn.setValue(50)

        ui_date_time_dtime = QtGui.QDateTimeEdit(self.ui_bottom_right_grp)
        ui_date_time_dtime.setDateTime(QtCore.QDateTime.currentDateTime())

        ui_slider_slide = QtGui.QSlider(QtCore.Qt.Horizontal, self.ui_bottom_right_grp)
        ui_slider_slide.setValue(40)

        ui_bar_scroll = QtGui.QScrollBar(QtCore.Qt.Horizontal, self.ui_bottom_right_grp)
        ui_bar_scroll.setValue(60)

        ui_test_dial = QtGui.QDial(self.ui_bottom_right_grp)
        ui_test_dial.setValue(30)
        ui_test_dial.setNotchesVisible(True)

        ui_layout = QtGui.QGridLayout()
        ui_layout.addWidget(ui_line_edit, 0, 0, 1, 2)
        ui_layout.addWidget(ui_spin_spn, 1, 0, 1, 2)
        ui_layout.addWidget(ui_date_time_dtime, 2, 0, 1, 2)
        ui_layout.addWidget(ui_slider_slide, 3, 0)
        ui_layout.addWidget(ui_bar_scroll, 4, 0)
        ui_layout.addWidget(ui_test_dial, 3, 1, 2, 1)
        ui_layout.setRowStretch(5, 1)

        self.ui_bottom_right_grp.setLayout(ui_layout)

    def createProgressBar(self):
        self.ui_style_prog = QtGui.QProgressBar()
        self.ui_style_prog.setRange(0, 100000)
        self.ui_style_prog.setValue(0)

        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.advanceProgressBar)
        timer.start(1000)


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    root = WidgetGallery()
    root.show()

    sys.exit(app.exec_())
