#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from PySide import QtCore, QtGui


class MainWindow(QtGui.QMainWindow):

    def __init__(self):
        QtGui.QMainWindow.__init__(self)

        button = QtGui.QPushButton(self)
        button.setGeometry(QtCore.QRect(100, 100, 100, 100))

        machine = QtCore.QStateMachine(self)
        s1 = QtCore.QState()
        s1.assignProperty(button, 'text', 'Outside')
        s2 = QtCore.QState()
        s2.assignProperty(button, 'text', 'Inside')

        enter_transition = QtCore.QEventTransition(button, QtCore.QEvent.Enter)
        enter_transition.setTargetState(s2)
        s1.addTransition(enter_transition)

        leave_transition = QtCore.QEventTransition(button, QtCore.QEvent.Leave)
        leave_transition.setTargetState(s1)
        s2.addTransition(leave_transition)

        s3 = QtCore.QState()
        s3.assignProperty(button, 'text', 'Pressing...')

        press_transition = QtCore.QEventTransition(button, QtCore.QEvent.MouseButtonPress)
        press_transition.setTargetState(s3)
        s2.addTransition(press_transition)

        release_transition = QtCore.QEventTransition(button, QtCore.QEvent.MouseButtonRelease)
        release_transition.setTargetState(s2)
        s3.addTransition(release_transition)

        machine.addState(s1)
        machine.addState(s2)
        machine.addState(s3)

        machine.setInitialState(s1)
        machine.start()

        self.setCentralWidget(button)
        self.show()


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    root = MainWindow()

    sys.exit(app.exec_())