#!/usr/bin/env python2.7
# -*- encoding: utf-8 -*-

import sys
import time
from PySide import QtGui, QtCore

class MySignal(QtCore.QObject):
    sig = QtCore.Signal(str)

class MyLongThread(QtCore.QThread):
    def __init__(self, parent=None):
        QtCore.QThread.__init__(self, parent)
        self.exiting = False
        self.signal = MySignal()

    def run(self):
        end = time.time() + 10
        while not self.exiting:
            sys.stdout.write("*")
            sys.stdout.flush()
            time.sleep(1)
            now = time.time()
            if now >= end:
                self.exiting = True
        self.signal.sig.emit("OK")

class MyThread(QtCore.QThread):
    def __init__(self, parent=None):
        QtCore.QThread.__init__(self, parent)
        self.exiting = False

    def run(self):
        while not self.exiting:
            sys.stdout.write(".")
            sys.stdout.flush()
            time.sleep(1)
