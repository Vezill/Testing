#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import sys
import random
from PySide import QtCore

DATA_SIZE = 100000
BUFFER_SIZE = 8192
buffer = range(BUFFER_SIZE)

free_bytes = QtCore.QSemaphore(BUFFER_SIZE)
used_bytes = QtCore.QSemaphore()


class Producer(QtCore.QThread):

    def run(self):
        for i in range(DATA_SIZE):
            free_bytes.acquire()
            buffer[i % BUFFER_SIZE] = 'ACGT'[random.randint(0, 3)]
            used_bytes.release()


class Consumer(QtCore.QThread):

    def run(self):
        for i in range(DATA_SIZE):
            used_bytes.acquire()
            sys.stderr.write(buffer[i % BUFFER_SIZE])
            free_bytes.release()


if __name__ == "__main__":
    app = QtCore.QCoreApplication([])
    producer = Producer()
    consumer = Consumer()

    producer.start()
    consumer.start()

    producer.wait()
    producer.wait()