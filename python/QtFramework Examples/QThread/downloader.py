#!/usr/bin/env python

import urllib2

from PySide import QtCore, QtGui, QtNetwork


class DownloadThread(QtCore.QThread):

    data_downloaded = QtCore.Signal()

    def __init__(self, url):
        QtCore.QThread.__init__(self)
        self.url = url
        self._data = None

    def run(self):
        self._data = urllib2.urlopen(self.url).read()
        self.data_downloaded.emit()

    def get_data(self):
        return self._data


class ImagePreview(QtGui.QWidget):

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.setMinimumSize(64, 64)
        self.text = None
        self.pixmap = None
        self.dl_n = 0

    def paintEvent(self, paintEvent):
        painter = QtGui.QPainter(self)

        if(self.pixmap):
            painter.drawPixmap(0, 0, self.pixmap)

        if(self.text):
            painter.setPen(QtCore.Qt.blue)
            painter.setFont(QtGui.QFont("Arial", 30))
            painter.drawText(self.rect(), QtCore.Qt.AlignCenter, self.text)

    def startDownload(self):
        self.setText(str(self.dl_n))
        self.dl_n += 1
        print("Starting Download {0}".format(self.dl_n))

        self.download_thread = DownloadThread("http://dl.dropbox.com/u/12505117/28008.png")
        self.download_thread.start()
        self.download_thread.data_downloaded.connect(self.onDownloadFinished)

    def onDownloadFinished(self):
        self.text = None
        self.paintImage()
        print("downloaded finished?")
        if self.dl_n < 5:
            self.startDownload()

    def paintImage(self):
        print("Painting")
        pixmap = QtGui.QPixmap()
        pixmap.loadFromData(self.download_thread.get_data())
        self.setPixmap(pixmap)

    def setPixmap(self, pixmap):
        self.pixmap = pixmap
        self.setMinimumSize(pixmap.width(), pixmap.height())
        self.update()

    def setText(self, text):
        self.text = text
        self.update()


class MainWindow(QtGui.QWidget):

    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.imagepreview = ImagePreview()
        self.button = QtGui.QPushButton("Start")
        self.button.clicked.connect(self.imagepreview.startDownload)
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.button)
        layout.addWidget(self.imagepreview)
        self.setLayout(layout)


if __name__ == "__main__":
    import sys

    try:
        app = QtGui.QApplication([])

    except RuntimeError:
        pass

    root = MainWindow()
    root.show()

    sys.exit(app.exec_())