#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from PySide import QtCore, QtGui


DEFAULT_CENTER_X = -0.647011
DEFAULT_CENTER_Y = -0.0395159
DEFAULT_SCALE = 0.00403897

ZOOM_IN_FACTOR = 0.8
ZOOM_OUT_FACTOR = 1 / ZOOM_IN_FACTOR
SCROLL_STEP = 20


class RenderThread(QtCore.QThread):

    COLORMAP_SIZE = 512
    rendered_image_signal = QtCore.Signal(QtGui.QImage, float)

    def __init__(self, parent=None):
        QtCore.QThread.__init__(self, parent)

        self.mutex = QtCore.QMutex()
        self.condition = QtCore.QWaitCondition()
        self.center_x = 0.0
        self.center_y = 0.0
        self.scale_factor = 0.0
        self.result_size = QtCore.QSize()
        self.colormap = []

        self.restart = False
        self.abort = False

        for i in range(RenderThread.COLORMAP_SIZE):
            self.colormap.append(self.rgbFromWaveLength(380.0 + (i * 400.0
                / RenderThread.COLORMAP_SIZE)))

    def stop(self):
        self.mutex.lock()
        self.abort = True
        self.condition.wakeOne()
        self.mutex.unlock()

        self.wait()

    def render(self, center_x, center_y, scale_factor, result_size):
        locker = QtCore.QMutexLocker(self.mutex)

        self.center_x = center_x
        self.center_y = center_y
        self.scale_factor = scale_factor
        self.result_size = result_size

        if not self.isRunning():
            self.start(QtCore.QThread.LowPriority)
        else:
            self.restart = True
            self.condition.wakeOne()

    def run(self):
        while True:
            self.mutex.lock()
            result_size = self.result_size
            scale_factor = self.scale_factor
            center_x = self.center_x
            center_y = self.center_y
            self.mutex.unlock()
            
            half_width = result_size.width() / 2
            half_height = result_size.height() / 2
            image = QtGui.QImage(result_size, QtGui.QImage.Format_RGB32)
            
            num_passes = 8
            current_pass = 0
            
            while current_pass < num_passes:
                max_iterations = (1 << (2 * current_pass + 6)) + 32
                limit = 4
                all_black = True
                
                for y in range(-half_height, half_height):
                    if self.restart:
                        break
                    if self.abort:
                        return
                    
                    ay = 1j * (center_y + (y * scale_factor))
                    
                    for x in range(-half_width, half_width):
                        c0 = center_x + (x * scale_factor) + ay
                        c = c0
                        num_iterations = 0
                        
                        while num_iterations < max_iterations:
                            num_iterations += 1
                            c = c * c + c0
                            if abs(c) >= limit:
                                break
                            
                            num_iterations += 1
                            c = c * c + c0
                            if abs(c) >= limit:
                                break
                                
                            num_iterations += 1
                            c = c * c + c0
                            if abs(c) >= limit:
                                break
                                
                            num_iterations += 1
                            c = c * c + c0
                            if abs(c) >= limit:
                                break
                                
                        if num_iterations < max_iterations:
                            image.setPixel(x + half_width, y + half_height, self.colormap[
                                num_iterations % RenderThread.COLORMAP_SIZE])
                            all_black = False
                        else:
                            image.setPixel(x + half_width, y + half_height, QtGui.qRgb(0, 0, 0))

                if all_black and current_pass == 0:
                    current_pass = 4
                else:
                    if not self.restart:
                        self.rendered_image_signal.emit(image, scale_factor)
                    current_pass += 1

            self.mutex.lock()
            if not self.restart:
                self.condition.wait(self.mutex)
            self.restart = False
            self.mutex.unlock()

    def rgbFromWaveLength(self, wave):
        r = 0.0
        g = 0.0
        b = 0.0

        if 380.0 <= wave <= 440.0:
            r = -1.0 * (wave - 440.0) / (440.0 - 380.0)
            b = 1.0

        elif 440.0 <= wave <= 490.0:
            g = (wave - 440.0) / (490.0 - 440.0)
            b = 1.0

        elif 490.0 <= wave <= 510.0:
            g = 1.0
            b = -1.0 * (wave - 510.0) / (510.0 - 490.0)

        elif 510.0 <= wave <= 580.0:
            r = (wave - 510.0) / (580.0 - 510.0)
            g = 1.0

        elif 580.0 <= wave <= 645.0:
            r = 1.0
            g = -1.0 * (wave - 645.0) / (645.0 - 580.0)

        elif 645.0 <= wave <= 780.0:
            r = 1.0

        s = 1.0
        if wave > 700.0:
            s = 0.3 + 0.7 * (780.0 - wave) / (780.0 - 700.0)
        elif wave < 420.0:
            s = 0.3 + 0.7 * (wave - 380.0) / (420.0 - 380.0)

        r = pow(r * s, 0.8)
        g = pow(g * s, 0.8)
        b = pow(b * s, 0.8)

        return QtGui.qRgb(int(r * 255), int(g * 255), int(b * 255))


class MandelbrotWidget(QtGui.QWidget):

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.thread = RenderThread()
        self.pixmap = QtGui.QPixmap()
        self.pixmap_offset = QtCore.QPoint()
        self.last_drag_position = QtCore.QPoint()

        self.center_x = DEFAULT_CENTER_X
        self.center_y = DEFAULT_CENTER_Y
        self.pixmap_scale = DEFAULT_SCALE
        self.current_scale = DEFAULT_SCALE

        self.thread.rendered_image_signal.connect(self.updatePixmap)

        self.setWindowTitle('Mandelbrot')
        self.setCursor(QtCore.Qt.CrossCursor)
        self.resize(550, 400)

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.fillRect(self.rect(), QtCore.Qt.black)

        if self.pixmap.isNull():
            painter.setPen(QtCore.Qt.white)
            painter.drawText(self.rect(), QtCore.Qt.AlignCenter, 'Rendering initial image, please '
                'wait...')
            return

        if self.current_scale == self.pixmap_scale:
            painter.drawPixmap(self.pixmap_offset, self.pixmap)

        else:
            scale_factor = self.pixmap_scale / self.current_scale
            new_width = int(self.pixmap.width() * scale_factor)
            new_height = int(self.pixmap.height() * scale_factor)
            new_x = self.pixmap_offset.x() + (self.pixmap.width() - new_width) / 2
            new_y = self.pixmap_offset.y() + (self.pixmap.height() - new_height) / 2

            painter.save()
            painter.translate(new_x, new_y)
            painter.scale(scale_factor, scale_factor)
            painter.drawPixmap(0, 0, self.pixmap)
            painter.restore()

        text = 'Use mouse wheel to zoom. Press and hold left mouse button to scroll.'
        metrics = painter.fontMetrics()
        text_width = metrics.width(text)

        painter.setPen(QtCore.Qt.NoPen)
        painter.setBrush(QtGui.QColor(0, 0, 0, 127))
        painter.drawRect((self.width() - text_width) / 2 - 5, 0, text_width + 10,
            metrics.lineSpacing() + 5)
        painter.setPen(QtCore.Qt.white)
        painter.drawText((self.width() - text_width) / 2, metrics.leading() + metrics.ascent(),
            text)

    def resizeEvent(self, event):
        self.thread.render(self.center_x, self.center_y, self.current_scale, self.size())

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Plus:
            self.zoom(ZOOM_IN_FACTOR)
        elif event.key() == QtCore.Qt.Key_Minus:
            self.zoom(ZOOM_OUT_FACTOR)
        elif event.key() == QtCore.Qt.Key_Left:
            self.scroll(-SCROLL_STEP, 0)
        elif event.key() == QtCore.Qt.Key_Right:
            self.scroll(+SCROLL_STEP, 0)
        elif event.key() == QtCore.Qt.Key_Down:
            self.scroll(0, -SCROLL_STEP)
        elif event.key() == QtCore.Qt.Key_Up:
            self.scroll(0, +SCROLL_STEP)
        else:
            QtGui.QWidget.keyPressEvent(self, event)

    def wheelEvent(self, event):
        num_degrees = event.delta() / 8
        num_steps = num_degrees / 15.0
        self.zoom(pow(ZOOM_IN_FACTOR, num_steps))

    def mousePressEvent(self, event):
        if event.buttons() == QtCore.Qt.LeftButton:
            self.last_drag_position = QtCore.QPoint(event.pos())

    def mouseMoveEvent(self, event):
        if event.buttons() & QtCore.Qt.LeftButton:
            self.pixmap_offset += event.pos() - self.last_drag_position
            self.last_drag_position = QtCore.QPoint(event.pos())
            self.update()

    def mouseReleaseEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self.pixmap_offset += event.pos() - self.last_drag_position
            self.last_drag_position = QtCore.QPoint()

            delta_x = (self.width() - self.pixmap.width()) / 2 - self.pixmap_offset.x()
            delta_y = (self.height() - self.pixmap.height()) / 2 - self.pixmap_offset.y()
            self.scroll(delta_x, delta_y)

    def updatePixmap(self, image, scale_factor):
        if not self.last_drag_position.isNull():
            return

        self.pixmap = QtGui.QPixmap.fromImage(image)
        self.pixmap_offset = QtCore.QPoint()
        self.last_drag_position = QtCore.QPoint()
        self.pixmap_scale = scale_factor
        self.update()

    def zoom(self, zoom_factor):
        self.current_scale *= zoom_factor
        self.update()
        self.thread.render(self.center_x, self.center_y, self.current_scale, self.size())

    def scroll(self, delta_x, delta_y):
        self.center_x += delta_x * self.current_scale
        self.center_y += delta_y * self.current_scale
        self.update()
        self.thread.render(self.center_x, self.center_y, self.current_scale, self.size())


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    widget = MandelbrotWidget()
    widget.show()
    r = app.exec_()

    widget.thread.stop()
    sys.exit(r)