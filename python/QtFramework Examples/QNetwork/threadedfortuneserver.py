#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import random

from PySide import QtCore, QtGui, QtNetwork


class FortuneThread(QtCore.QThread):

    error = QtCore.Signal(QtNetwork.QTcpSocket.SocketError)

    def __init__(self, socketDescriptor, fortune, parent):
        QtCore.QThread.__init__(self, parent)

        self.socketDescriptor = socketDescriptor
        self.text = fortune

    def run(self):
        tcpSocket = QtNetwork.QTcpSocket()
        if not tcpSocket.setSocketDescriptor(self.socketDescriptor):
            self.error.emit(tcpSocket.error())
            return

        block = QtCore.QByteArray()
        outstr = QtCore.QDataStream(block, QtCore.QIODevice.WriteOnly)
        outstr.setVersion(QtCore.QDataStream.Qt_4_0)
        outstr.writeUInt16(0)
        outstr.writeString(self.text)
        outstr.device().seek(0)
        outstr.writeUInt16(block.count() - 20)

        tcpSocket.write(block)
        tcpSocket.disconnectFromHost()
        tcpSocket.waitForDisconnected()


class FortuneServer(QtNetwork.QTcpServer):

    def __init__(self, parent=None):
        QtNetwork.QTcpServer.__init__(self, parent)

        self.fortunes = (
            "You've been leading a dog's life. Stay off the furniture.",
            "You've got to think about tomorrow.",
            "You will be surprised by a loud noise.",
            "You will feel hungry again in another hour.",
            "You might have mail.",
            "You cannot kill time without injuring eternity.",
            "Computers are not intelligent. They only think they are.")

    def incomingConnection(self, socketDescriptor):
        fortune = self.fortunes[random.randint(0, len(self.fortunes) - 1)]

        try:
            # python3
            fortune = bytes(fortune, encoding="ascii")
        except:
            # python2
            pass

        thread = FortuneThread(socketDescriptor, fortune, self)
        thread.finished.connect(thread.deleteLater)
        thread.start()


class Dialog(QtGui.QDialog):

    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)

        self.server = FortuneServer()

        statusLabel = QtGui.QLabel()
        quitButton = QtGui.QPushButton("Quit")
        quitButton.setAutoDefault(False)

        if not self.server.listen():
            QtGui.QMessageBox.critical(self, "Threaded Fortune Server", "Unable to start the "
                "server: {0}".format(self.server.errorString()))
            self.close()
            return

        statusLabel.setText("The server is running on port {0}.\nRun the Fortune Client example "
            "now.".format(self.server.serverPort()))

        quitButton.clicked.connect(self.close)

        buttonLayout = QtGui.QHBoxLayout()
        buttonLayout.addStretch(1)
        buttonLayout.addWidget(quitButton)
        buttonLayout.addStretch(1)

        mainLayout = QtGui.QVBoxLayout()
        mainLayout.addWidget(statusLabel)
        mainLayout.addLayout(buttonLayout)
        self.setLayout(mainLayout)

        self.setWindowTitle("Threaded Fortune Server")


if __name__ == "__main__":
    import sys

    app = QtGui.QApplication([])
    dialog = Dialog()
    dialog.show()
    sys.exit(app.exec_())