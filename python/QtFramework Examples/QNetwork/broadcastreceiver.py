#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from PySide import QtCore, QtGui, QtNetwork


class Receiver(QtGui.QDialog):

    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)

        self.statusLabel = QtGui.QLabel("Listening for broadcasted messages")
        quitButton = QtGui.QPushButton("&Quit")

        self.udpSocket = QtNetwork.QUdpSocket(self)
        self.udpSocket.bind(45454)

        self.udpSocket.readyRead.connect(self.processPendingDatagrams)
        quitButton.clicked.connect(self.close)

        buttonLayout = QtGui.QHBoxLayout()
        buttonLayout.addStretch(1)
        buttonLayout.addWidget(quitButton)
        buttonLayout.addStretch(1)

        mainLayout = QtGui.QVBoxLayout()
        mainLayout.addWidget(self.statusLabel)
        mainLayout.addLayout(buttonLayout)
        self.setLayout(mainLayout)

        self.setWindowTitle("Broadcast Receiver")

    def processPendingDatagrams(self):
        while self.udpSocket.hasPendingDatagrams():
            datagram, host, port = self.udpSocket.readDatagram(self.udpSocket.pendingDatagramSize())

            try:
                # python3
                datagram = str(datagram, encoding="ascii")

            except TypeError:
                # python2
                pass

            self.statusLabel.setText("Received datagram: \"{0}\"".format(datagram))


if __name__ == "__main__":
    import sys

    app = QtGui.QApplication([])
    receiver = Receiver()
    receiver.show()
    sys.exit(receiver.exec_())