#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from PySide import QtCore, QtGui, QtNetwork


class HttpWindow(QtGui.QDialog):

    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)

        self.urlLineEdit = QtGui.QLineEdit('http://www.ietf.org/iesg/1rfc_index.txt')

        self.urlLabel = QtGui.QLabel('&URL:')
        self.urlLabel.setBuddy(self.urlLineEdit)
        self.statusLabel = QtGui.QLabel('Please enter the URL of a file you want to download.')

        self.quitButton = QtGui.QPushButton('Quit')
        self.downloadButton = QtGui.QPushButton('Download')
        self.downloadButton.setDefault(True)

        self.progressDialog = QtGui.QProgressDialog(self)

        self.http = QtNetwork.QHttp(self)
        self.outFile = None
        self.httpGetId = 0
        self.httpRequestAborted = False

        self.urlLineEdit.textChanged.connect(self.enableDownloadButton)
        self.http.requestFinished.connect(self.httpRequestFinished)
        self.http.dataReadProgress.connect(self.updateDataReadProgress)
        self.http.responseHeaderReceived.connect(self.readResponseHeader)
        self.progressDialog.canceled.connect(self.cancelDownload)
        self.downloadButton.clicked.connect(self.downloadFile)
        self.quitButton.clicked.connect(self.close)

        topLayout = QtGui.QHBoxLayout()
        topLayout.addWidget(self.urlLabel)
        topLayout.addWidget(self.urlLineEdit)

        buttonLayout = QtGui.QHBoxLayout()
        buttonLayout.addStretch(1)
        buttonLayout.addWidget(self.downloadButton)
        buttonLayout.addWidget(self.quitButton)

        mainLayout = QtGui.QVBoxLayout()
        mainLayout.addLayout(topLayout)
        mainLayout.addWidget(self.statusLabel)
        mainLayout.addLayout(buttonLayout)
        self.setLayout(mainLayout)

        self.setWindowTitle('HTTP')
        self.urlLineEdit.setFocus()

    def downloadFile(self):
        url = QtCore.QUrl(self.urlLineEdit.text())
        fileInfo = QtCore.QFileInfo(url.path())
        fileName = fileInfo.fileName()

        if QtCore.QFile.exists(fileName):
            QtGui.QMessageBox.information(self, 'HTTP', 'There already exists a file called {0} in '
                'the current directory'.format(fileName))
            return

        self.outFile = QtCore.QFile(fileName)
        if not self.outFile.open(QtCore.QIODevice.WriteOnly):
            QtGui.QMessageBox.information(self, 'HTTP', 'Unable to save the file {0}: {1}.'.format(
                fileName, self.outFile.errorString()))
            self.outFile = None
            return

        if url.port() != -1:
            self.http.setHost(url.host(), url.port())
        else:
            self.http.setHost(url.host(), 80)
        if url.userName():
            self.http.setUser(url.userName(), url.password())

        self.httpRequestAborted = False
        self.httpGetId = self.http.get(url.path(), self.outFile)

        self.progressDialog.setWindowTitle('HTTP')
        self.progressDialog.setLabelText('Downloading {0}.'.format(fileName))
        self.downloadButton.setEnabled(False)

    def cancelDownload(self):
        self.statusLabel.setText('Download canceled.')
        self.httpRequestAborted = True
        self.http.abort()
        self.downloadButton.setEnabled(True)

    def httpRequestFinished(self, requestId, error):
        if self.httpRequestAborted:
            if self.outFile is not None:
                self.outFile.close()
                self.outFile.remove()
                self.outFile = None

            self.progressDialog.hide()
            return

        if requestId != self.httpGetId:
            return

        self.progressDialog.hide()
        self.outFile.close()

        if error:
            self.outFile.remove()
            QtGui.QMessageBox.information(self, 'HTTP', 'Download failed: {0}.'.format(
                self.http.errorString()))
        else:
            fileName = QtCore.QFileInfo(QtCore.QUrl(self.urlLineEdit.text()).path()).fileName()
            self.statusLabel.setText('Downloaded {0} to current directory.'.format(fileName))

        self.downloadButton.setEnabled(True)
        self.outFIle = None

    def readResponseHeader(self, responseHeader):
        if responseHeader.statusCode() != 200:
            QtGui.QMessageBox.information(self, 'HTTP', 'Download failed: {0}.'.format(
                responseHeader.reasonPhrase()))
            self.httpRequestAborted = True
            self.progressDialog.hide()
            self.http.abort()
            return

    def updateDataReadProgress(self, bytesRead, totalBytes):
        if self.httpRequestAborted:
            return

        self.progressDialog.setMaximum(totalBytes)
        self.progressDialog.setValue(bytesRead)

    def enableDownloadButton(self):
        self.downloadButton.setEnabled(not self.urlLineEdit.text())


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    httpWin = HttpWindow()
    sys.exit(httpWin.exec_())