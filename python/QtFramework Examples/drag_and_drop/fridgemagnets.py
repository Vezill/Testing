#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from PySide import QtCore, QtGui
import fridgemagnets_rc


class DragLabel(QtGui.QLabel):

    def __init__(self, text, parent=None):
        QtGui.QLabel.__init__(self, parent)

        metric = QtGui.QFontMetrics(self.font())
        size = metric.size(QtCore.Qt.TextSingleLine, text)

        image = QtGui.QImage(size.width() + 12, size.height() + 12,
            QtGui.QImage.Format_ARGB32_Premultiplied)
        image.fill(QtGui.qRgba(0, 0, 0, 0))

        font = QtGui.QFont()
        font.setStyleStrategy(QtGui.QFont.ForceOutline)

        painter = QtGui.QPainter()
        painter.begin(image)
        painter.setRenderHint(QtGui.QPainter.Antialiasing)
        painter.setBrush(QtCore.Qt.white)
        painter.drawRoundedRect(QtCore.QRectF(0.5, 0.5, image.width() - 1, image.height() - 1),
            25, 25, QtCore.Qt.RelativeSize)

        painter.setFont(font)
        painter.setBrush(QtCore.Qt.black)
        painter.drawText(QtCore.QRect(QtCore.QPoint(6, 6), size), QtCore.Qt.AlignCenter, text)
        painter.end()

        self.setPixmap(QtGui.QPixmap.fromImage(image))
        self.label_text = text

    def mousePressEvent(self, event):
        item_data = QtCore.QByteArray()
        data_stream = QtCore.QDataStream(item_data, QtCore.QIODevice.WriteOnly)
        data_stream << QtCore.QByteArray(str(self.label_text)) << QtCore.QPoint(event.pos() -
            self.rect().topLeft())

        mime_data = QtCore.QMimeData()
        mime_data.setData('application/x-fridgemagnet', item_data)
        mime_data.setText(self.label_text)

        drag = QtGui.QDrag(self)
        drag.setMimeData(mime_data)
        drag.setHotSpot(event.pos() - self.rect().topLeft())
        drag.setPixmap(self.pixmap())

        self.hide()

        if (drag.exec_(QtCore.Qt.MoveAction | QtCore.Qt.CopyAction, QtCore.Qt.CopyAction) ==
            QtCore.Qt.MoveAction):
            self.close()
        else:
            self.show()


class DragWidget(QtGui.QWidget):

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        dictionary_file = QtCore.QFile('./words.txt')
        dictionary_file.open(QtCore.QFile.ReadOnly)

        x = 5
        y = 5

        for word in QtCore.QTextStream(dictionary_file).readAll().split():
            word_lbl = DragLabel(word, self)
            word_lbl.move(x, y)
            word_lbl.show()
            x += word_lbl.width() + 2
            if x >= 245:
                x = 5
                y += word_lbl.height() + 2

        new_palette = self.palette()
        new_palette.setColor(QtGui.QPalette.Window, QtCore.Qt.white)
        self.setPalette(new_palette)

        self.setMinimumSize(400, max(200, 6))
        self.setWindowTitle('Fridge Magnets')
        self.setAcceptDrops(True)

    def dragEnterEvent(self, event):
        if event.mimeData().hasFormat('application/x-fridgemagnet'):
            if event.source() in self.children():
                event.setDropAction(QtCore.Qt.MoveAction)
                event.accept()
            else:
                event.acceptProposedAction()

        elif event.mimeData().hasText():
            event.acceptProposedAction()
        else:
            event.ignore()

    dragMoveEvent = dragEnterEvent

    def dropEvent(self, event):
        if event.mimeData().hasFormat('application/x-fridgemagnet'):
            mime = event.mimeData()
            item_data = mime.data('application/x-fridgemagnet')
            data_stream = QtCore.QDataStream(item_data, QtCore.QIODevice.ReadOnly)

            text = QtCore.QByteArray()
            offset = QtCore.QPoint()
            data_stream >> text >> offset

            text = str(text)

            new_lbl = DragLabel(text, self)
            new_lbl.move(event.pos() - offset)
            new_lbl.show()

            if event.source() in self.children():
                event.setDropAction(QtCore.Qt.MoveAction)
                event.accept()
            else:
                event.acceptProposedAction()

        elif event.mimeData().hasText():
            pieces = event.mimeData().text().split()
            position = event.pos()

            for piece in pieces:
                new_lbl = DragLabel(piece, self)
                new_lbl.move(position)
                new_lbl.show()

                position += QtCore.QPoint(new_lbl.width(), 0)

            event.acceptProposedAction()

        else:
            event.ignore()


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    root = DragWidget()
    root.show()

    sys.exit(app.exec_())