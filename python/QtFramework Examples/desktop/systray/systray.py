#!/usr/bin/env python


from PySide import QtCore, QtGui
import systray_rc


class Window(QtGui.QDialog):
    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)

        self.initUi()

    def initUi(self):
        ##### Create Icon Group Box #####
        self.ui_icon_grp = QtGui.QGroupBox('Tray Icon')

        self.ui_icon_lbl = QtGui.QLabel('Icon: ')
        self.ui_icon_combo = QtGui.QComboBox()
        self.ui_icon_combo.addItem(QtGui.QIcon(':/images/bad.svg'), 'Bad')
        self.ui_icon_combo.addItem(QtGui.QIcon(':/images/heart.svg'), 'Heart')
        self.ui_icon_combo.addItem(QtGui.QIcon(':/images/trash.svg'), 'Trash')

        self.ui_show_icon_chk = QtGui.QCheckBox('Show Icon')
        self.ui_show_icon_chk.setChecked(True)

        ui_icon_layout = QtGui.QHBoxLayout()
        ui_icon_layout.addWidget(self.ui_icon_lbl)
        ui_icon_layout.addWidget(self.ui_icon_combo)
        ui_icon_layout.addStretch()
        ui_icon_layout.addWidget(self.ui_show_icon_chk)
        self.ui_icon_grp.setLayout(ui_icon_layout)

        ##### Create Message Group Box #####
        self.ui_message_grp = QtGui.QGroupBox('Balloon Message')

        ui_type_lbl = QtGui.QLabel('Type: ')
        self.ui_type_combo = QtGui.QComboBox()
        self.ui_type_combo.addItem('None', QtGui.QSystemTrayIcon.NoIcon)
        self.ui_type_combo.addItem(self.style().standardIcon(QtGui.QStyle.SP_MessageBoxInformation),
            'Information', QtGui.QSystemTrayIcon.Information)
        self.ui_type_combo.addItem(self.style().standardIcon(QtGui.QStyle.SP_MessageBoxWarning),
            'Warning', QtGui.QSystemTrayIcon.Warning)
        self.ui_type_combo.addItem(self.style().standardIcon(QtGui.QStyle.SP_MessageBoxCritical),
            'Critical', QtGui.QSystemTrayIcon.Critical)
        self.ui_type_combo.setCurrentIndex(1)

        self.ui_duration_lbl = QtGui.QLabel('Duration:')
        self.ui_duration_spin = QtGui.QSpinBox()
        self.ui_duration_spin.setRange(5, 60)
        self.ui_duration_spin.setSuffix(' s')
        self.ui_duration_spin.setValue(15)

        ui_duration_warning_lbl = QtGui.QLabel('(some systems might ignore this hint)')
        ui_duration_warning_lbl.setIndent(10)

        ui_title_lbl = QtGui.QLabel('Title:')
        self.ui_title_edit = QtGui.QLineEdit('Cannot connect to network')

        ui_body_lbl = QtGui.QLabel('Body:')
        self.ui_body_txt = QtGui.QTextEdit()
        self.ui_body_txt.setPlainText("Don't believe me. Honestly, I don't have a clue.\nClick "
            "this balloon for details.")

        self.ui_show_message_btn = QtGui.QPushButton('Show Message')
        self.ui_show_message_btn.setDefault(True)

        ui_message_layout = QtGui.QGridLayout()
        ui_message_layout.addWidget(ui_type_lbl, 0, 0)
        ui_message_layout.addWidget(self.ui_type_combo, 0, 1, 1, 2)
        ui_message_layout.addWidget(self.ui_duration_lbl, 1, 0)
        ui_message_layout.addWidget(self.ui_duration_spin, 1, 1)
        ui_message_layout.addWidget(ui_duration_warning_lbl, 1, 2, 1, 2)
        ui_message_layout.addWidget(ui_title_lbl, 2, 0)
        ui_message_layout.addWidget(self.ui_title_edit, 2, 1, 1, 4)
        ui_message_layout.addWidget(ui_body_lbl, 3, 0)
        ui_message_layout.addWidget(self.ui_body_txt, 3, 1, 2, 4)
        ui_message_layout.addWidget(self.ui_show_message_btn, 5, 4)
        ui_message_layout.setColumnStretch(3, 1)
        ui_message_layout.setRowStretch(4, 1)
        self.ui_message_grp.setLayout(ui_message_layout)

        self.ui_icon_lbl.setMinimumWidth(self.ui_duration_lbl.sizeHint().width())

        ##### Create Actions #####
        self.ui_minimize_act = QtGui.QAction('Mi&nimize', self, triggered=self.hide)
        self.ui_maximize_act = QtGui.QAction('Ma&ximize', self, triggered=self.showMaximized)
        self.ui_restore_act = QtGui.QAction('&Restore', self, triggered=self.showNormal)
        self.ui_quit_act = QtGui.QAction('&Quit', self, triggered=QtGui.qApp.quit)

        ##### Create Tray Icon #####
        self.ui_tray_icon_menu = QtGui.QMenu(self)
        self.ui_tray_icon_menu.addAction(self.ui_minimize_act)
        self.ui_tray_icon_menu.addAction(self.ui_maximize_act)
        self.ui_tray_icon_menu.addAction(self.ui_restore_act)
        self.ui_tray_icon_menu.addSeparator()
        self.ui_tray_icon_menu.addAction(self.ui_quit_act)

        self.ui_tray_icon = QtGui.QSystemTrayIcon(self)
        self.ui_tray_icon.setContextMenu(self.ui_tray_icon_menu)

        ##### Configure Signals #####
        self.ui_show_message_btn.clicked.connect(self.showMessage)
        self.ui_show_icon_chk.toggled.connect(self.ui_tray_icon.setVisible)
        self.ui_icon_combo.currentIndexChanged[int].connect(self.setIcon)
        self.ui_tray_icon.messageClicked.connect(self.messageClicked)
        self.ui_tray_icon.activated.connect(self.iconActivated)

        ##### Setup Main Layout #####
        ui_main_layout = QtGui.QVBoxLayout()
        ui_main_layout.addWidget(self.ui_icon_grp)
        ui_main_layout.addWidget(self.ui_message_grp)
        self.setLayout(ui_main_layout)

        ##### Misc #####
        self.ui_icon_combo.setCurrentIndex(1)
        self.ui_tray_icon.show()

        self.setWindowTitle('Systray')
        self.resize(400, 300)

    def setVisible(self, visible):
        self.ui_minimize_act.setEnabled(visible)
        self.ui_maximize_act.setEnabled(not self.isMaximized())
        self.ui_restore_act.setEnabled(self.isMaximized() or not visible)
        QtGui.QDialog.setVisible(self, visible)

    def closeEvent(self, event):
        if self.ui_tray_icon.isVisible():
            QtGui.QMessageBox.information(self, 'Systray', 'The program will keep running in the '
                'system tray. To terminate the program, choose <b>Quit</b> in the context menu of '
                'the system tray entry.')
            self.hide()
            event.ignore()

    def setIcon(self, index):
        icon = self.ui_icon_combo.itemIcon(index)
        self.ui_tray_icon.setIcon(icon)
        self.setWindowIcon(icon)

        self.ui_tray_icon.setToolTip(self.ui_icon_combo.itemText(index))

    def iconActivated(self, reason):
        if reason in (QtGui.QSystemTrayIcon.Trigger, QtGui.QSystemTrayIcon.DoubleClick):
            self.ui_icon_combo.setCurrentIndex((self.ui_icon_combo.currentIndex() + 1) %
                self.ui_icon_combo.count())
        elif reason == QtGui.QSystemTrayIcon.MiddleClick:
            self.showMessage()

    def showMessage(self):
        icon = QtGui.QSystemTrayIcon.MessageIcon(self.ui_type_combo.itemData(
            self.ui_type_combo.currentIndex()))
        self.ui_tray_icon.showMessage(self.ui_title_edit.text(), self.ui_body_txt.toPlainText(),
            icon, self.ui_duration_spin.value() * 1000)

    def messageClicked(self):
        QtGui.QMessageBox.information(None, 'Systray', 'Sorry, I already gave what help I could.\n'
            'Maybe you should try asking a human?')


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])

    if not QtGui.QSystemTrayIcon.isSystemTrayAvailable():
        QtGui.QMessageBox.critical(None, 'Systray', "I couldn't detect any system tray on this "
            "system.")
        sys.exit(1)

    QtGui.QApplication.setQuitOnLastWindowClosed(False)

    root = Window()
    root.show()

    sys.exit(app.exec_())
