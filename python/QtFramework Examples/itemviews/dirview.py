#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import sys
from PySide import QtGui


if __name__ == '__main__':
    app = QtGui.QApplication([])

    model = QtGui.QDirModel()
    tree = QtGui.QTreeView()
    tree.setModel(model)

    tree.setAnimated(False)
    tree.setIndentation(20)
    tree.setSortingEnabled(True)

    tree.setWindowTitle('Dir View')
    tree.resize(640, 480)
    tree.show()

    sys.exit(app.exec_())