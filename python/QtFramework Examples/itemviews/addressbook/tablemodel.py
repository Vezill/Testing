#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from PySide import QtCore, QtGui


class TableModel(QtCore.QAbstractTableModel):

    def __init__(self, address=None, parent=None):
        QtCore.QAbstractTableModel.__init__(self, parent)

        if address is None:
            self.addresses = []
        else:
            self.addresses = addresses

    def rowCount(self, index=QtCore.QModelIndex()):
        """ Returns the number of rows the model holds. """
        return len(self.addresses)

    def columnCount(self, index=QtCore.QModelIndex()):
        """ Returns the number of columns the model holds. """
        return 2

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """
        Depending on the index and role given, return data. If not returning data, return None
        (Pyside equivalent of QT's 'invalid QVariant').
        """
        if not index.isValid():
            return None

        if not 0 <= index.row() < len(self.addresses):
            return None

        if role == QtCore.Qt.DisplayRole:
            name = self.addresses[index.row()]['name']
            address = self.addresses[index.row()]['address']

            if index.column() == 0:
                return name
            elif index.column() == 1:
                return address

        return None

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        """ Set the headers to be displayed. """
        if role != QtCore.Qt.DisplayRole:
            return None

        if orientation == QtCore.Qt.Horizontal:
            if section == 0:
                return 'Name'
            elif section == 1:
                return 'Address'

        return None

    def insertRows(self, position, rows=1, index=QtCore.QModelIndex()):
        """ Insert a row into the model. """
        self.beginInsertRows(QtCore.QModelIndex(), position, position + rows - 1)

        for row in range(rows):
            self.addresses.insert(position + row, {'name':'', 'address':''})

        self.endInsertRows()
        return True

    def removeRows(self, position, rows=1, index=QtCore.QModelIndex()):
        """ Remove a row from the model. """
        self.beginRemoveRows(QtCore.QModelIndex(), position, position + rows - 1)

        del self.addresses[position:position+rows]

        self.endRemoveRows()
        return True

    def setData(self, index, value, role=QtCore.Qt.EditRole):
        """ Adjust the data (set it to <value>) depending on the given index and role. """
        if role != QtCore.Qt.EditRole:
            return False

        if index.isValid() and 0 <= index.row() < len(self.addresses):
            address = self.addresses[index.row()]
            if index.column() == 0:
                address['name'] = value
            elif index.column() == 1:
                address['address'] = value
            else:
                return False

            self.dataChanged.emit(index, index)
            return True

        return False

    def flags(self, index):
        """
        Set the item flags at the given index. Seems like we're implimenting this function just to
        see how it's done, as we manually adjust each tableView to have NoEditTriggers.
        """
        if not index.isValid():
            return QtCore.Qt.ItemIsEnabled
        return QtCore.Qt.ItemFlags(QtCore.QAbstractTableModel.flags(self, index)
            | QtCore.Qt.ItemIsEditable)