#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from PySide import QtCore, QtGui
from adddialogwidget import AddDialogWidget


class NewAddressTab(QtGui.QWidget):
    """
    An extra tab that prompts the user to add new contacts. To be displayed only when there are no
    contacts in the model.
    """

    send_details_signal = QtCore.Signal(str, str)

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        description_label = QtGui.QLabel('There are no contacts in your address book.\nClick Add '
            'to add new contacts.')
        add_button = QtGui.QPushButton('Add')

        layout = QtGui.QVBoxLayout()
        layout.addWidget(description_label)
        layout.addWidget(add_button, 0, QtCore.Qt.AlignCenter)

        add_button.clicked.connect(self.addEntry)

        self.setLayout(layout)

    def addEntry(self):
        add_dialog = AddDialogWidget()

        if add_dialog.exec_():
            name = add_dialog.name
            address = add_dialog.address
            self.send_details_signal.emit(name, address)


if __name__ == '__main__':
    import sys

    def printAddress(name, address):
        print 'Name: {0}'.format(name)
        print 'Address: {0}'.format(address)

    app = QtGui.QApplication([])
    new_address_tab = NewAddressTab()
    new_address_tab.send_details_signal.connect(printAddress)
    new_address_tab.show()

    sys.exit(app.exec_())