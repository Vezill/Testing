#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from PySide import  QtGui
from addresswidget import AddressWidget


class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)

        self.address_widget = AddressWidget()
        self.setCentralWidget(self.address_widget)
        self.createMenus()
        self.setWindowTitle('Address Book')

    def createMenus(self):
        # Create the main menuBar menu items
        file_menu = self.menuBar().addMenu('&File')
        tool_menu = self.menuBar().addMenu('&Tools')

        # Populate the File menu
        open_act = self.createAction('&Open...', file_menu, self.openFile)
        save_act = self.createAction('&Save As...', file_menu, self.saveFile)
        file_menu.addSeparator()
        exit_act = self.createAction('E&xit', file_menu, self.close)

        # Populate the Tools menu
        add_act = self.createAction('&Add Entry...', tool_menu, self.address_widget.addEntry)
        self.edit_act = self.createAction('&Edit Entry...', tool_menu,
            self.address_widget.editEntry)
        tool_menu.addSeparator()
        self.remove_act = self.createAction('&Remove Entry', tool_menu,
            self.address_widget.removeEntry)

        # Disable the edit and remove items initially, as there are no items yet.
        self.edit_act.setEnabled(False)
        self.remove_act.setEnabled(False)

        # Wire up the updateActions slot
        self.address_widget.selection_changed_signal.connect(self.updateActions)

    def createAction(self, text, menu, slot):
        """ Helper function to save typing when populating menus with action. """
        action = QtGui.QAction(text, self)
        menu.addAction(action)
        action.triggered.connect(slot)
        return action

    # Quick gotcha:
    # QFileDialog.getOpenFileName and QFileDialog.getSaveFileName don't behave in PySide as they do
    # in Qt, where they return a QString containing the filename. In PySide these functions return
    # a tuple: (filename, filter)

    def openFile(self):
        filename, _ = QtGui.QFileDialog.getOpenFileName(self)
        if filename:
            self.address_widget.readFromFile(filename)

    def saveFile(self):
        filename, _ = QtGui.QFileDialog.getSaveFileName(self)
        if filename:
            self.address_widget.writeToFile(filename)

    def updateActions(self, selection):
        """ Only allow the user to remove or edit an item if an item is actually selected. """
        indexes = selection.indexes()

        if len(indexes) > 0:
            self.remove_act.setEnabled(True)
            self.edit_act.setEnabled(True)

        else:
            self.remove_act.setEnabled(False)
            self.edit_act.setEnabled(False)


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication(sys.argv)
    root = MainWindow()
    root.show()

    sys.exit(app.exec_())