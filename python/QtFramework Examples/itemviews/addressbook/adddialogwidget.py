#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from PySide import QtCore, QtGui


class AddDialogWidget(QtGui.QDialog):
    """ A dialog to add a new address to the address book. """

    def __init__(self, parent=None):
        QtGui.QDialog.__init__(self, parent)

        name_label = QtGui.QLabel('Name')
        address_label = QtGui.QLabel('Address')
        button_box = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok
            | QtGui.QDialogButtonBox.Cancel)

        self.name_text = QtGui.QLineEdit()
        self.address_text = QtGui.QTextEdit()

        grid = QtGui.QGridLayout()
        grid.setColumnStretch(1, 2)
        grid.addWidget(name_label, 0, 0)
        grid.addWidget(self.name_text, 0, 1)
        grid.addWidget(address_label, 1, 0, QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        grid.addWidget(self.address_text, 1, 1, QtCore.Qt.AlignLeft)

        layout = QtGui.QVBoxLayout()
        layout.addLayout(grid)
        layout.addWidget(button_box)

        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)

        self.setLayout(layout)
        self.setWindowTitle('Add a Contact')

    @property
    def name(self):
        return self.name_text.text()

    @property
    def address(self):
        return self.address_text.toPlainText()


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    dialog = AddDialogWidget()
    if dialog.exec_():
        name = dialog.name
        address = dialog.address
        print 'Name: {0}'.format(name)
        print 'Address: {0}'.format(address)