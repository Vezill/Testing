#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import pickle
from PySide import QtCore, QtGui
from tablemodel import TableModel
from newaddresstab import NewAddressTab
from adddialogwidget import AddDialogWidget


class AddressWidget(QtGui.QTabWidget):
    """
    The central widget of the application. Most of the addressbook's functionality is contained in
    this class.
    """

    selection_changed_signal = QtCore.Signal(QtGui.QItemSelection)

    def __init__(self, parent=None):
        QtGui.QTabWidget.__init__(self, parent)

        self.table_model = TableModel()
        self.new_address_tab = NewAddressTab()
        self.new_address_tab.send_details_signal.connect(self.addEntry)

        self.addTab(self.new_address_tab, 'Address Book')

        self.setupTabs()

    def addEntry(self, name=None, address=None):
        """ Add an entry to the address book. """
        if name is None and address is None:
            add_dialog = AddDialogWidget()

            if add_dialog.exec_():
                name = add_dialog.name
                address = add_dialog.address

        address = {'name': name, 'address': address}
        addresses = self.table_model.addresses[:]

        # The Qt docs for this example state that what we're doing here is checking if the entered
        # name already exists. What they (and we here) are actually doing is checking if the whole
        # name/address pair exists already - ok for the purposes of this example, but obviously not
        # how a real address book application should behave.
        try:
            addresses.remove(address)
            QtGui.QMessageBox.information(self, 'Duplicate Name', 'The name "{0}" already exists'.
                format(name))

        except ValueError:
            # The address didn't already exist, so let's add it to the model.
            # Step 1: create the row
            self.table_model.insertRows(0)

            # Step 2: get the index of the newly created row and use it to set the name.
            ix = self.table_model.index(0, 0, QtCore.QModelIndex())
            self.table_model.setData(ix, address['name'], QtCore.Qt.EditRole)

            # Step 3: lather, rinse, repeat for the address.
            ix = self.table_model.index(0, 1, QtCore.QModelIndex())
            self.table_model.setData(ix, address['address'], QtCore.Qt.EditRole)

            # Remove the newAddressTab, as we have at least one in the model.
            self.removeTab(self.indexOf(self.new_address_tab))

            # The screenshot for the Qt example shows nicely formatted multiline cells, but the
            # actual application doesn't behave quite so nicely, at least on Ubuntu. Here we resize
            # the newly created row so that multiline addresses look reasonable.
            table_view = self.currentWidget()
            table_view.resizeRowToContents(ix.row())

    def editEntry(self):
        """ Edit an entry in the address book. """
        table_view = self.currentWidget()
        proxy_model = table_view.model()
        selection_model = table_view.selectionModel()

        # Get the name and address of the currently selected row.
        indexes = selection_model.selectedRows()

        for index in indexes:
            row = proxy_model.mapToSource(index).row()
            ix = self.table_model.index(row, 0, QtCore.QModelIndex())
            name = self.table_model.data(ix, QtCore.Qt.DisplayRole)
            ix = self.table_model.index(row, 1, QtCore.QModelIndex())
            address = self.table_model.data(ix, QtCore.Qt.DisplayRole)

        # Open an addDialogWidget, and only allow the user to edit the address.
        add_dialog = AddDialogWidget()
        add_dialog.setWindowTitle('Edit a Contact')

        add_dialog.name_text.setReadOnly(True)
        add_dialog.name_text.setText(name)
        add_dialog.address_text.setText(address)

        # If the address is different, add it to the model.
        if add_dialog.exec_():
            new_address = add_dialog.address
            if new_address != address:
                ix = self.table_model.index(row, 1, QtCore.QModelIndex())
                self.table_model.setData(ix, new_address, QtCore.Qt.EditRole)

    def removeEntry(self):
        """ Remove an entry from the address book. """
        table_view = self.currentWidget()
        proxy_model = table_view.model()
        selection_model = table_view.selectionModel()

        # Just like editEntry, but this time remove the selected row.
        indexes = selection_model.selectedRows()

        for index in indexes:
            row = proxy_model.mapToSource(index).row()
            self.table_model.removeRows(row)

        # If we've removed the last address in the model, display newAddressTab
            if self.table_model.rowCount() == 0:
                self.insertTab(0, self.new_address_tab, 'Address Book')

    def setupTabs(self):
        """ Setup the various tabs in the addressWidget. """
        groups = ['ABC', 'DEF', 'GHI', 'JKL', 'MNO', 'PQR', 'STU', 'VW', 'XYZ']

        for group in groups:
            proxy_model = QtGui.QSortFilterProxyModel(self)
            proxy_model.setSourceModel(self.table_model)
            proxy_model.setDynamicSortFilter(True)

            table_view = QtGui.QTableView()
            table_view.setModel(proxy_model)
            table_view.setSortingEnabled(True)
            table_view.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
            table_view.horizontalHeader().setStretchLastSection(True)
            table_view.verticalHeader().hide()
            table_view.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
            table_view.setSelectionMode(QtGui.QAbstractItemView.SingleSelection)

            # This here be the magic: we use the group name (e.g. 'ABC') to build the regex for the
            # QSortFilterProxyModel for the group's tab. The regex will end up looking like
            # '^[ABC]^', only allowing this tab to display items where the name starts with 'A',
            # 'B', or 'C'. Notice that we set it to be case-insensitive.
            re_filter = '^[{0}].*'.format(group)

            proxy_model.setFilterRegExp(QtCore.QRegExp(re_filter, QtCore.Qt.CaseInsensitive))
            proxy_model.setFilterKeyColumn(0) # Filter on the 'name' column
            proxy_model.sort(0, QtCore.Qt.AscendingOrder)

            table_view.selectionModel().selectionChanged.connect(self.selection_changed_signal)

            self.addTab(table_view, group)

    # Note: The Qt example uses a QDataStream for the saving and loading. Here we're using a python
    # dictionary to store the addresses, which can't be streamed using QDataStream, so we just use
    # pickle for this example.
    def ReadFromFile(self, filename):
        """ Read contacts in from a file. """
        try:
            with open(filename, 'rb') as f:
                addresses = pickle.load(f)
        except IOError:
            QtGui.QMessageBox.information(self, 'Unable to open file: {0}'.format(filename))

        if len(addresses) == 0:
            QtGui.QMessageBox.information(self, 'No contacts in file: {0}'.format(filename))
        else:
            for address in addresses:
                self.addEntry(address['name'], address['address'])

    def writeToFile(self, filename):
        """ Save all contacts in the model to a file. """
        try:
            with open(filename, 'wb') as f:
                pickle.dump(self.table_model.addresses, f)
        except IOError:
            QtGui.QMessageBox.information(self, 'Unable to open file: {0}'.format(filename))


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    address_widget = AddressWidget()
    address_widget.show()

    sys.exit(app.exec_())