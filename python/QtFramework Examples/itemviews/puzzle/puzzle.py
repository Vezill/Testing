#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import random
from PySide import QtCore, QtGui
import puzzle_rc


class PuzzleWidget(QtGui.QWidget):

    puzzle_completed_signal = QtCore.Signal()

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.piece_pixmaps = []
        self.piece_rects = []
        self.piece_locations = []
        self.highlighted_rect = QtCore.QRect()
        self.in_place = 0

        self.setAcceptDrops(True)
        self.setMinimumSize(400, 400)
        self.setMaximumSize(400, 400)

    def clear(self):
        self.piece_pixmaps = []
        self.piece_rects = []
        self.piece_locations = []
        self.highlighted_rect = QtCore.QRect()
        self.in_place = 0
        self.update()

    def dragEnterEvent(self, event):
        if event.mimeData().hasFormat('image/x-puzzle-piece'):
            event.accept()
        else:
            event.ignore()

    def dragLeaveEvent(self, event):
        update_rect = self.highlighted_rect
        self.highlighted_rect = QtCore.QRect()
        self.update(update_rect)
        event.accept()

    def dragMoveEvent(self, event):
        update_rect = self.highlighted_rect.unite(self.targetSquare(event.pos()))

        if event.mimeData().hasFormat('image/x-puzzle-piece') and self.findPiece(
            self.targetSquare(event.pos())) == -1:
            self.highlighted_rect = self.targetSquare(event.pos())
            event.setDropAction(QtCore.Qt.MoveAction)
            event.accept()
        else:
            self.highlighted_rect = QtCore.QRect()
            event.ignore()

        self.update(update_rect)

    def dropEvent(self, event):
        if event.mimeData().hasFormat('image/x-puzzle-piece') and self.findPiece(
            self.targetSquare(event.pos())) == -1:
            piece_data = event.mimeData().data('image/x-puzzle-piece')
            stream = QtCore.QDataStream(piece_data, QtCore.QIODevice.ReadOnly)
            square = self.targetSquare(event.pos())
            pixmap = QtGui.QPixmap()
            location = QtCore.QPoint()
            stream >> pixmap >> location

            self.piece_locations.append(location)
            self.piece_pixmaps.append(pixmap)
            self.piece_rects.append(square)

            self.highlighted_rect = QtCore.QRect()
            self.update(square)

            event.setDropAction(QtCore.Qt.MoveAction)
            event.accept()

            if location == QtCore.QPoint(square.x() / 80, square.y() / 80):
                self.in_place += 1
                if self.in_place == 25:
                    self.puzzle_completed_signal.emit()

        else:
            self.highlighted_rect = QtCore.QRect()
            event.ignore()

    def findPiece(self, piece_rect):
        try:
            return self.piece_rects.index(piece_rect)
        except ValueError:
            return -1

    def mousePressEvent(self, event):
        square = self.targetSquare(event.pos())
        found = self.findPiece(square)

        if found == -1:
            return

        location = self.piece_locations[found]
        pixmap = self.piece_pixmaps[found]

        del self.piece_locations[found]
        del self.piece_pixmaps[found]
        del self.piece_rects[found]

        if location == QtCore.QPoint(square.x() + 80, square.y() + 80):
            self.in_place -= 1

        self.update(square)

        item_data = QtCore.QByteArray()
        data_stream = QtCore.QDataStream(item_data, QtCore.QIODevice.WriteOnly)

        data_stream << pixmap << location

        mime_data = QtCore.QMimeData()
        mime_data.setData('image/x-puzzle-piece', item_data)

        drag = QtGui.QDrag(self)
        drag.setMimeData(mime_data)
        drag.setHotSpot(event.pos() - square.topLeft())
        drag.setPixmap(pixmap)

        if drag.start(QtCore.Qt.MoveAction) == 0:
            self.piece_locations.insert(found, location)
            self.piece_pixmaps.insert(found, pixmap)
            self.piece_rects.insert(found, square)
            self.update(self.targetSquare(event.pos()))

            if location == QtCore.QPoint(square.x() / 80, square.y() / 80):
                self.in_place += 1

    def paintEvent(self, event):
        painter = QtGui.QPainter()
        painter.begin(self)
        painter.fillRect(event.rect(), QtCore.Qt.white)

        if self.highlighted_rect.isValid():
            painter.setBrush(QtGui.QColor('#FFCCCC'))
            painter.setPen(QtCore.Qt.NoPen)
            painter.drawRect(self.highlighted_rect.adjusted(0, 0, -1, -1))

        for i, piece_rect in enumerate(self.piece_rects):
            painter.drawPixmap(piece_rect, self.piece_pixmaps[i])

        painter.end()

    def targetSquare(self, position):
        return QtCore.QRect(position.x() // 80 * 80, position.y() // 80 * 80, 80, 80)


class PiecesModel(QtCore.QAbstractListModel):

    def __init__(self, parent=None):
        QtCore.QAbstractListModel.__init__(self, parent)

        self.locations = []
        self.pixmaps = []

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if not index.isValid():
            return None

        if role == QtCore.Qt.DecorationRole:
            return QtGui.QIcon(self.pixmaps[index.row()].scaled(60, 60, QtCore.Qt.KeepAspectRatio,
                QtCore.Qt.SmoothTransformation))

        if role == QtCore.Qt.UserRole:
            return self.pixmaps[index.row()]

        if role == QtCore.Qt.UserRole + 1:
            return self.locations[index.row()]

        return None

    def addPiece(self, pixmap, location):
        if random.random() < 0.5:
            row = 0
        else:
            row = len(self.pixmaps)

        self.beginInsertRows(QtCore.QModelIndex(), row, row)
        self.pixmaps.insert(row, pixmap)
        self.locations.insert(row, location)
        self.endInsertRows()

    def flags(self, index):
        if index.isValid():
            return (QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
                | QtCore.Qt.ItemIsDragEnabled)

        return QtCore.Qt.ItemIsDropEnabled

    def removeRows(self, row, count, parent):
        if parent.isValid():
            return False

        if row >= len(self.pixmaps) or row + count <= 0:
            return False

        begin_row = max(0, row)
        end_row = min(row + count - 1, len(self.pixmaps) - 1)

        self.beginRemoveRows(parent, begin_row, end_row)

        del self.pixmaps[begin_row:end_row+1]
        del self.pixmaps[begin_row:end_row+1]

        self.endRemoveRows()
        return True

    def mimeTypes(self):
        return ['image/x-puzzle-piece']

    def mimeData(self, indexes):
        mime_data = QtCore.QMimeData()
        encoded_data = QtCore.QByteArray()

        stream = QtCore.QDataStream(encoded_data, QtCore.QIODevice.WriteOnly)

        for index in indexes:
            if index.isValid():
                pixmap = QtGui.QPixmap(self.data(index, QtCore.Qt.UserRole))
                location = self.data(index, QtCore.Qt.UserRole + 1)
                stream << pixmap << location

        mime_data.setData('image/x-puzzle-piece', encoded_data)
        return mime_data

    def dropMimeData(self, data, action, row, column, parent):
        if not data.hasFormat('image/x-puzzle-piece'):
            return False

        if action == QtCore.Qt.IgnoreAction:
            return True

        if column > 0:
            return False

        if not parent.isValid():
            if row < 0:
                end_row = len(self.pixmaps)
            else:
                end_row = min(row, len(self.pixmaps))
        else:
            end_row = parent.row()

        encoded_data = data.data('image/x-puzzle-piece')
        stream = QtCore.QDataStream(encoded_data, QtCore.QIODevice.ReadOnly)

        while not stream.atEnd():
            pixmap = QtGui.QPixmap()
            location = QtGUi.QPoint()
            stream >> pixmap >> location

            self.beginInsertRows(QtCore.QModelIndex(), end_row, end_row)
            self.pixmaps.insert(end_row, pixmap)
            self.locations.insert(end_row, location)
            self.endInsertRows()

            end_row += 1

        return True

    def rowCount(self, parent):
        if parent.isValid():
            return 0
        else:
            return len(self.pixmaps)

    def supportedDropActions(self):
        return QtCore.Qt.CopyAction | QtCore.Qt.MoveAction

    def addPieces(self, pixmap):
        self.beginRemoveRows(QtCore.QModelIndex(), 0, 24)
        self.pixmaps = []
        self.locations = []
        self.endRemoveRows()

        for y in range(5):
            for x in range(5):
                piece_image = pixmap.copy(x * 80, y * 80, 80, 80)
                self.addPiece(piece_image, QtCore.QPoint(x, y))


class MainWindow(QtGui.QMainWindow):

    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)

        self.puzzle_image = QtGui.QPixmap()

        self.setupMenus()
        self.setupWidgets()

        self.setSizePolicy(QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed))
        self.setWindowTitle('Puzzle')

    def openImage(self, path=None):
        if not path:
            path = QtGui.QFileDialog.getOpenFileName(self, 'Open Image', '',
                'Image Files (*.png *.jpg *.bmp')

        if path:
            new_image = QtGui.QPixmap()
            if not new_image.load(path):
                QtGui.QMessageBox.warning(self, 'Open Image', 'The image file count not be loaded.',
                    QtGui.QMessageBox.Cancel)
                return

            self.puzzle_image = new_image
            self.setupPuzzle()

    def setCompleted(self):
        QtGui.QMessageBox.information(self, 'Puzzle Completed', 'Congratulations! You have '
            'completed the puzzle!\nClick OK to start again.', QtGui.QMessageBox.Ok)

        self.setupPuzzle()

    def setupPuzzle(self):
        size = min(self.puzzle_image.width(), self.puzzle_image.height())
        self.puzzle_image = self.puzzle_image.copy((self.puzzle_image.width() - size) / 2,
            (self.puzzle_image.height() - size) / 2, size, size).scaled(400, 400,
            QtCore.Qt.IgnoreAspectRatio, QtCore.Qt.SmoothTransformation)

        random.seed(QtGui.QCursor.pos().x() ^ QtGui.QCursor.pos().y())

        self.model.addPieces(self.puzzle_image)
        self.puzzle_widget.clear()

    def setupMenus(self):
        file_menu = self.menuBar().addMenu('&File')

        open_act = file_menu.addAction('&Open...')
        open_act.setShortcut('Ctrl+O')

        exit_act = file_menu.addAction('E&xit')
        exit_act.setShortcut('Ctrl+Q')

        game_menu = self.menuBar().addMenu('&Game')

        restart_act = game_menu.addAction('&Restart')

        open_act.triggered.connect(self.openImage)
        exit_act.triggered.connect(QtGui.qApp.quit)
        restart_act.triggered.connect(self.setupPuzzle)

    def setupWidgets(self):
        frame = QtGui.QFrame()
        frame_layout = QtGui.QHBoxLayout(frame)

        self.pieces_listv = QtGui.QListView()
        self.pieces_listv.setDragEnabled(True)
        self.pieces_listv.setViewMode(QtGui.QListView.IconMode)
        self.pieces_listv.setIconSize(QtCore.QSize(60, 60))
        self.pieces_listv.setGridSize(QtCore.QSize(80, 80))
        self.pieces_listv.setSpacing(10)
        self.pieces_listv.setMovement(QtGui.QListView.Snap)
        self.pieces_listv.setAcceptDrops(True)
        self.pieces_listv.setDropIndicatorShown(True)

        self.model = PiecesModel(self)
        self.pieces_listv.setModel(self.model)

        self.puzzle_widget = PuzzleWidget()

        self.puzzle_widget.puzzle_completed_signal.connect(self.setCompleted,
            QtCore.Qt.QueuedConnection)

        frame_layout.addWidget(self.pieces_listv)
        frame_layout.addWidget(self.puzzle_widget)
        self.setCentralWidget(frame)


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    root = MainWindow()
    root.openImage('./example.jpg')
    root.show()

    sys.exit(app.exec_())