#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from PySide import QtCore, QtGui


class StarEditor(QtGui.QWidget):
    """ The custom editor for editing StarRatings. """

    # A signal to tell the delegate when we've finished editing.
    editing_finished_signal = QtCore.Signal()

    def __init__(self, parent=None):
        """ Initialize the editor object, making sure we can watch mouse events. """
        QtGui.QWidget.__init__(self, parent)

        self.setMouseTracking(True)
        self.setAutoFillBackground(True)

    def sizeHint(self):
        """ Tell the caller how big we are. """
        return self.star_rating.sizeHint()

    def paintEvent(self, event):
        """ Paint the editor, offloading the work to the StarRating class. """
        painter = QtGui.QPainter(self)
        self.star_rating.paint(painter, self.rect(), self.palette(), is_editable=True)

    def mouseMoveEvent(self, event):
        """
        As the mouse moves inside the editor, track the position and update the editor to display
        as many stars as necessary.
        """
        star = self.starAtPosition(event.x())

        if (star != self.star_rating.star_count) and (star != -1):
            self.star_rating.star_count = star
            self.update()

    def mouseReleaseEvent(self, event):
        """
        Once the user has clicked his/her chosen star rating, tell the delegate we're done editing.
        """
        self.editing_finished_signal.emit()

    def starAtPosition(self, x):
        """ Calculate which star the user's mouse cursor is currently hovering over. """
        star = (x / (self.star_rating.sizeHint().width() / self.star_rating.max_star_count)) + 1
        if (star <= 0) or (star > self.star_rating.max_star_count):
            return -1

        return star