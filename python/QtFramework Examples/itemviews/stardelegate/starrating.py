#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from math import cos, sin, pi
from PySide import QtCore, QtGui

PAINTING_SCALE_FACTOR = 20


class StarRating(object):
    """ Handle the actual painting of the stars themselves. """

    def __init__(self, star_count=1, max_star_count=5):
        self.star_count = star_count
        self.max_star_count = max_star_count

        # Create the star shape we'll be drawing
        self.star_polygon = QtGui.QPolygonF()
        self.star_polygon.append(QtCore.QPointF(1.0, 0.5))
        for i in range(1, 5):
            self.star_polygon.append(QtCore.QPointF(0.5 + 0.5 * cos(0.8 * i * pi),
                0.5 + 0.5 * sin(0.8 * i * pi)))

        # Create the diamond shape we'll show in the editor
        self.diamond_polygon = QtGui.QPolygonF()
        diamond_points = [
            QtCore.QPointF(0.4, 0.5), QtCore.QPointF(0.5, 0.4), QtCore.QPointF(0.6, 0.5),
            QtCore.QPointF(0.5, 0.6), QtCore.QPointF(0.4, 0.5) ]
        for point in diamond_points:
            self.diamond_polygon.append(point)

    def sizeHint(self):
        """ Tell the caller how big we are. """
        return PAINTING_SCALE_FACTOR * QtCore.QSize(self.max_star_count, 1)

    def paint(self, painter, rect, palette, is_editable=False):
        """ Paint the stars (and/or diamond if we're in editing mode). """
        painter.save()

        painter.setRenderHint(QtGui.QPainter.Antialiasing, True)
        painter.setPen(QtCore.Qt.NoPen)

        if is_editable:
            painter.setBrush(palette.highlight())
        else:
            painter.setBrush(palette.windowText())

        y_offset = (rect.height() - PAINTING_SCALE_FACTOR)
        painter.translate(rect.x(), rect.y() + y_offset)
        painter.scale(PAINTING_SCALE_FACTOR, PAINTING_SCALE_FACTOR)

        for i in range(self.max_star_count):
            if i < self.star_count:
                painter.drawPolygon(self.star_polygon, QtCore.Qt.WindingFill)
            elif is_editable:
                painter.drawPolygon(self.diamond_polygon, QtCore.Qt.WindingFill)
            painter.translate(1.0, 0.0)

        painter.restore()