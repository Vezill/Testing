#!/usr/bin/python
# -*- coding: utf-8 -*-
# CANBus Library test script

import PCANBasic as PCB
import ctypes
import time

class CanbusTests():

    def main(self):

        ''' Set up PCAN variables '''
        self.objPCANBus = PCB.PCANBasic()
        self.handle = PCB.PCAN_USBBUS1

        ''' Initialize device '''
        result = self.objPCANBus.Initialize(self.handle, PCB.PCAN_BAUD_125K)
        if result != PCB.PCAN_ERROR_OK:
            print self.objPCANBus.GetErrorText(result, 0)
        else:
            print 'Successfully Connected'

        ''' Check device status '''
        print 'Status: %s\n' % \
            self.objPCANBus.GetErrorText(self.objPCANBus.GetStatus(self.handle))[1]

        ''' CALL TEST FUNCTION HERE '''
        self.save_data()
        self.switch_pack(1)
        self.save_data()

        ''' Release device '''
        self.objPCANBus.Uninitialize(self.handle)

    def write_data(self):
        ''' Write to the device. NOTE: this must be formatted as a TPCANMsg '''
        CANMsg = PCB.TPCANMsg()
        CANMsg.ID = 0x600
        CANMsg.LEN = 0                                  # This can go up to 8
        CANMsg.MSGTYPE = PCB.PCAN_MESSAGE_STANDARD      # or PCAN_MESSAGE_EXTENDED

        val = raw_input('Enter value: ')
        while val != 'q':
            CANMsg.DATA[0] = int(val[:2], 16)
            CANMsg.DATA[1] = int(val[2:4], 16)
            result = self.objPCANBus.Write(self.handle, CANMsg)
            print 'Write: %s' % self.objPCANBus.GetErrorText(result, 0)[1]
            val = raw_input('Enter value: ')
        del CANMsg

    def request_data(self, num):
        ''' Request data from the device NOTE: num must be hex '''
        CANMsg = PCB.TPCANMsg()
        CANMsg.ID = num
        CANMsg.LEN = 0
        CANMsg.MSGTYPE = PCB.PCAN_MESSAGE_RTR

        result = self.objPCANBus.Write(self.handle, CANMsg)
        if result == PCB.PCAN_ERROR_OK:
            while True:
                result, msg, ts = self.objPCANBus.Read(self.handle)
                if result == PCB.PCAN_ERROR_OK:
                    msgdata = ''
                    for i in xrange(int(msg.LEN)):
                        msgdata = msgdata + ('%2s ' % hex(msg.DATA[i])[2:]).upper()
                    print '%2s: %s' % (hex(msg.ID)[2:6].upper(), msgdata)
                else:
                    print 'Read Error: %s' % self.objPCANBus.GetErrorText(result, 0)[1]
                time.sleep(2)
        else:
            'Write Error: %s' % self.objPCANBus.GetErrorText(result, 0)[1]

    def random_request(self):
        ''' Test requesting specific registers '''
        CANMsg = PCB.TPCANMsg()
        CANMsg.LEN = 0
        CANMsg.MSGTYPE = PCB.PCAN_MESSAGE_RTR
        idlist = [0x601, 0x60C, 0x661, 0x680, 0x662, 0x60A, 0x607, 0x604, 0x640]
        for idnum in idlist:
            CANMsg.ID = idnum
            print 'Requesting register: %sh ' % hex(CANMsg.ID)[2:].upper(),
            tm = time.time()
            result = self.objPCANBus.Write(self.handle, CANMsg)
            if result == PCB.PCAN_ERROR_OK:
                attempt = 0
                msg = PCB.TPCANMsg()
                while msg.ID != idnum:
                    result, msg, ts = self.objPCANBus.Read(self.handle)
                    attempt += 1
                    print '\b.',
                if result == PCB.PCAN_ERROR_OK:
                    msgdata = ''
                    for i in xrange(int(msg.LEN)):
                        msgdata = msgdata + ('%2s ' % hex(msg.DATA[i])[2:]).upper()
                    print '\n%d reads' % attempt
                    print '%sh: %s' % (hex(msg.ID)[2:5].upper(), msgdata)
                    print 'Time taken: %.2fs' % (time.time() - tm)
                else:
                    print self.objPCANBus.GetErrorText(result, 0)[1]
            else:
                print self.objPCANBus.GetErrorText(result, 0)[1]
            self.objPCANBus.Reset(self.handle)
            time.sleep(2)
            print

    def check_queue(self):
        ''' Constantly check receive queue '''
        while True:
            result, msg, ts = self.objPCANBus.Read(self.handle)
            if result == PCB.PCAN_ERROR_OK:
                msgdata = ''
                for i in xrange(int(msg.LEN)):
                    msgdata = msgdata + ('%2s ' % hex(msg.DATA[i])[2:]).upper()
                print '%sh: %s' % (hex(msg.ID)[2:5].upper(), msgdata)
            else:
                print '\n%s' % self.objPCANBus.GetErrorText(result)[1]
            time.sleep(2)

        ''' Read from the device '''
        result, msg, tmstp = self.objPCANBus.Read(self.handle)
        if result != PCB.PCAN_ERROR_OK:
            print 'Read Error: %s' % self.objPCANBus.GetErrorText(result, 0)[1]
        else:
            print '%d:%d' % (tmstp.millis, tmstp.micros), '%s: %s' % (msg.MSGTYPE, msg.DATA[0])

    def save_data(self):
        ''' Test saving values ***Relies on the device constantly sending information*** '''
        batteryData = {}
        while True:
            while True:
                result, msg, ts = self.objPCANBus.Read(self.handle)
                if result != PCB.PCAN_ERROR_QRCVEMPTY:
                    break

            if msg.ID not in batteryData:
                batteryData[msg.ID] = []
                for i in xrange(int(msg.LEN)):
                    batteryData[msg.ID].append(msg.DATA[i])
            else:
                break

        for reg in batteryData:
            print '%2s' % hex(reg)[2:5],
            for value in batteryData[reg]:
                print '%2s ' % hex(value)[2:],
            print

    def switch_pack(self, num):
        ''' Ask for pack number and write next pack '''
        CANMsg = PCB.TPCANMsg()
        CANMsg.ID = 0x400
        CANMsg.LEN = 2
        CANMsg.MSGTYPE = PCB.PCAN_MESSAGE_RTR
        CANMsg.DATA[0] = 0
        CANMsg.DATA[1] = num
        self.objPCANBus.Write(self.handle, CANMsg)
        while True:
            result, msg, tsp = self.objPCANBus.Read(self.handle)
            if result == PCB.PCAN_ERROR_OK:
                print result, hex(msg.ID), hex(msg.DATA[0])
                break


if __name__ == '__main__':
    print '\n'
    test = CanbusTests()
    test.main()
    print '\n'
