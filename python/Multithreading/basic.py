#!/usr/bin/python
# -*- encoding: utf-8 -*-
import threading

""" Create and run a thread """
# class MyThread(threading.Thread):

#     def run(self):
#         print "You called my start method, yeah."
#         print "Were you expecting something amazing?"

# MyThread().start()

""" Create and run multiple threads """
# theVar = 1

# class MyThread(threading.Thread):

#     def run(self):
#         global theVar
#         print "This is thread " + str(theVar) + " speaking."
#         print "Hello and good bye."
#         theVar = theVar + 1

# for x in xrange(20):
#     MyThread().start()
