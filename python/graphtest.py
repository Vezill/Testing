#!/usr/bin/env python
#-*- encoding: utf-8 -*-


import numpy as np
from PySide import QtGui, QtCore

import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4'] = 'PySide'
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as QFigureCanvas
from matplotlib.figure import Figure


class LineGraphWidget(QFigureCanvas):
    def __init__(self, parent=None):
        self.fig = Figure()
        self.ax = self.fig.add_subplot(111)
        self.data = [0.5]
        self.counter = 1

        QFigureCanvas.__init__(self, self.fig)

        self.initUi()

    def initUi(self):
        self.ax.grid()
        self.ax.set_ylim(0, 30)
        self.ax.set_xlim(0, 25)
        self.ax.set_xticks([i for i in range(0, 26)])
        self.ax.set_yticks([i for i in range(0, 31)])

        self.setWindowTitle('Test')

        self.ax.plot(self.data)
        self.fig.canvas.draw()
        self.timer = self.startTimer(2000)

    def timerEvent(self, event):
        self.ax.clear()
        self.ax.grid()
        self.ax.set_ylim(0, 30)
        self.ax.set_xlim(0, 25)
        self.ax.set_xticks([i for i in range(0, 26)])
        self.ax.set_yticks([i for i in range(0, 31)])

        self.ax.plot(self.data, 'black')
        self.fig.canvas.draw()

        self.data.append(np.random.randint(1, 30))
        if len(self.data) > 25:
            self.data.pop(0)

        self.counter += 1


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    root = LineGraphWidget()
    root.show()

    sys.exit(app.exec_())
