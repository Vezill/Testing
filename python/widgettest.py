#!/usr/bin/python
# -*- encoding: utf-8 -*-

#imports
import os
import sys
import time
from PySide import QtGui, QtCore
import PCANBasic as PCB

def main():
    app = QtGui.QApplication(sys.argv)
    root = MainWindow()
    root.show()
    app.exec_()


class MainWindow(QtGui.QWidget):

    def __init__(self, parent=None):
        #Variables
        self._BAUDRATES = ({
            "1M Bit/s Baud Rate":PCB.PCAN_BAUD_1M,     "800 kBit/s Baud Rate":PCB.PCAN_BAUD_800K,
            "500 kBit/s Baud Rate":PCB.PCAN_BAUD_500K, "250 kBit/s Baud Rate":PCB.PCAN_BAUD_250K,
            "125 kBit/s Baud Rate":PCB.PCAN_BAUD_125K, "100 kBit/s Baud Rate":PCB.PCAN_BAUD_100K,
            "95 kBit/s Baud Rate":PCB.PCAN_BAUD_95K,   "83 kBit/s Baud Rate":PCB.PCAN_BAUD_83K,
            "50 kBit/s Baud Rate":PCB.PCAN_BAUD_50K,   "47 kBit/s Baud Rate":PCB.PCAN_BAUD_47K,
            "33 kBit/s Baud Rate":PCB.PCAN_BAUD_33K,   "20 kBit/s Baud Rate":PCB.PCAN_BAUD_20K,
            "10 kBit/s Baud Rate":PCB.PCAN_BAUD_10K,   "5 kBit/s Baud Rate":PCB.PCAN_BAUD_5K
        })

        self._CHANNELS = ({
            "PCAN USB BUS 1":PCB.PCAN_USBBUS1, "PCAN USB BUS 2":PCB.PCAN_USBBUS2,
            "PCAN USB BUS 3":PCB.PCAN_USBBUS3, "PCAN USB BUS 4":PCB.PCAN_USBBUS4,
            "PCAN USB BUS 5":PCB.PCAN_USBBUS5, "PCAN USB BUS 6":PCB.PCAN_USBBUS6,
            "PCAN USB BUS 7":PCB.PCAN_USBBUS7, "PCAN USB BUS 8":PCB.PCAN_USBBUS8,
            "PCAN PCI BUS 1":PCB.PCAN_PCIBUS1, "PCAN PCI BUS 2":PCB.PCAN_PCIBUS2,
            "PCAN PCI BUS 3":PCB.PCAN_PCIBUS3, "PCAN PCI BUS 4":PCB.PCAN_PCIBUS4,
            "PCAN PCI BUS 5":PCB.PCAN_PCIBUS5, "PCAN PCI BUS 6":PCB.PCAN_PCIBUS6,
            "PCAN PCI BUS 7":PCB.PCAN_PCIBUS7, "PCAN PCI BUS 8":PCB.PCAN_PCIBUS8,
            "PCAN PC BUS 1":PCB.PCAN_PCCBUS1,  "PCAN PC BUS 2":PCB.PCAN_PCCBUS2,
        })

        self._BATTERY = ({
            "Update Lower Half":0, "Update Upper Half":1,
        })
        
        self.pcbObj = PCB.PCANBasic()

        QtGui.QWidget.__init__(self)
        self.setup_ui()

    def setup_ui(self):
        #Window
        self.setWindowTitle("Update Battery Pack Firmware")
        self.resize(500, 200)
        self.setMaximumSize(QtCore.QSize(500, 200))
        self.setMinimumSize(QtCore.QSize(500, 200))

        #Widgets
        self.cbxBaudRate = QtGui.QComboBox()
        self.cbxBaudRate.addItems(self._BAUDRATES.keys())

        self.cbxChannel = QtGui.QComboBox()
        self.cbxChannel.addItems(self._CHANNELS.keys())

        self.cbxBattUpdate = QtGui.QComboBox()
        self.cbxBattUpdate.addItems(self._BATTERY.keys())

        self.btnConnect = QtGui.QPushButton("Connect")
        self.btnConnect.released.connect(self.pcan_connect)

        self.btnSelFile = QtGui.QPushButton("Select File")
        self.btnSelFile.released.connect(self.select_update_file)
        self.btnSelFile.setEnabled(False)

        self.btnUpdRequest = QtGui.QPushButton("Update Request")
        self.btnUpdRequest.released.connect(self.send_update_request)
        self.btnUpdRequest.setEnabled(False)

        self.btnSendFile = QtGui.QPushButton("Send File")
        self.btnSendFile.released.connect(self.send_update_file)
        self.btnSendFile.setEnabled(False)

        self.btnBack = QtGui.QPushButton("Exit")
        self.btnBack.released.connect(self.close_window)

        self.tedConsole = QtGui.QTextEdit()
        self.tedConsole.setReadOnly(True)

        #Layouts
        self.vblControls = QtGui.QVBoxLayout()
        self.vblControls.setSpacing(1)

        self.vblControls.addWidget(self.cbxBaudRate)
        self.vblControls.addWidget(self.cbxChannel)
        self.vblControls.addWidget(self.cbxBattUpdate)
        self.vblControls.addWidget(self.btnConnect)
        self.vblControls.addWidget(self.btnSelFile)
        self.vblControls.addWidget(self.btnUpdRequest)
        self.vblControls.addWidget(self.btnSendFile)
        self.vblControls.addWidget(self.btnBack)

        self.hblWidget = QtGui.QHBoxLayout()
        self.hblWidget.addLayout(self.vblControls)
        self.hblWidget.addWidget(self.tedConsole)

        self.setLayout(self.hblWidget)

    def pcan_connect(self):
        if self.btnConnect.text() == "Connect":
            self.pcbHandle = self._CHANNELS[self.cbxChannel.currentText()]
            result = self.pcbObj.Initialize(self.pcbHandle,
                self._BAUDRATES[self.cbxBaudRate.currentText()])
            if result == PCB.PCAN_ERROR_OK:
                self.tedConsole.insertPlainText("Successfully connected to packs.\n")
                self.btnConnect.setText("Disconnect")
                self.btnSelFile.setEnabled(True)
            else:
                self.tedConsole.insertPlainText("Error connecting to pack.\n")
        else:
            self.pcan_disconnect()

    def pcan_disconnect(self):
        result = self.pcbObj.SetValue(self.pcbHandle, PCB.PCAN_MESSAGE_FILTER, PCB.PCAN_FILTER_OPEN)
        if result != PCB.PCAN_ERROR_OK:
            self.tedConsole.insertPlainText("Error resetting message filter.\n")
        result = self.pcbObj.Uninitialize(self.pcbHandle)
        if result == PCB.PCAN_ERROR_OK:
            self.tedConsole.insertPlainText("Successfully disconnected from packs.\n")
            self.btnConnect.setText("Connect")
            self.btnSelFile.setEnabled(False)
            self.btnUpdRequest.setEnabled(False)
            self.btnSendFile.setEnabled(False)
        else:
            self.tedConsole.insertPlainText("Error disconnecting from pack.\n")

    def select_update_file(self):
        self.updFile, _ = QtGui.QFileDialog.getOpenFileName(self, "Open Update File",
            os.path.expanduser(r"~\Downloads"), "*.txt")
        if not os.path.isfile(self.updFile):
            self.tedConsole.insertPlainText("File could not be opened, try again.")
            self.btnUpdRequest.setEnabled(False)
        else:
            self.tedConsole.insertPlainText("File opened successfully.\n")
            self.btnUpdRequest.setEnabled(True)

    def send_update_request(self):
        result = self.pcbObj.FilterMessages(self.pcbHandle, 0x6FF, 0x6FF, PCB.PCAN_MODE_STANDARD)
        if result == PCB.PCAN_ERROR_OK:
            self.tedConsole.insertPlainText("Message filter set.\n")
        else:
            self.tedConsole.insertPlainText("Error setting message filter.\n")

        wmsg = PCB.TPCANMsg()
        wmsg.ID = 0x000
        wmsg.LEN = 8
        wmsg.MSGTYPE = PCB.PCAN_MESSAGE_STANDARD
        for i in xrange(8):
            wmsg.DATA[i] = int("u p g r a d e 0".split()[i].encode("hex"), 16)

        result = self.pcbObj.Write(self.pcbHandle, wmsg)
        if result == PCB.PCAN_ERROR_OK:
            self.tedConsole.insertPlainText("You now have 60 seconds to send the update file.\n")
            while True:
                result, rmsg, _ = self.pcbObj.Read(self.pcbHandle)
                if rmsg.ID == 0x6FF:
                    break
            for i in xrange(rmsg.LEN):
                if rmsg.DATA[i] > 32:
                    self.tedConsole.insertPlainText(("%02x" % rmsg.DATA[i]).decode("hex"))
            self.btnSendFile.setEnabled(True)

    def send_update_file(self):
        wmsg = PCB.TPCANMsg()
        wmsg.ID = 0x000
        wmsg.LEN = 8
        wmsg.MSGTYPE = PCB.PCAN_MESSAGE_STANDARD

        with open(self.updFile, "rb") as tmpFile:
            timeout = 0
            x = 1
            rdData = tmpFile.read(1)
            while timeout != 50:
                if rdData != "":
                    for i in xrange(8):
                        wmsg.DATA[i] = int(rdData.encode("hex"), 16)
                        rdData = tmpFile.read(1)
                        x += 1
                    result = self.pcbObj.Write(self.pcbHandle, wmsg)
                    if result != PCB.PCAN_ERROR_OK:
                        self.tedConsole.insertPlainText("Error writing update, cancelling.\n")
                        break
                    if int(rdData.encode("hex"), 16) == 0x0D:
                        time.sleep(0.0040)
                    if rdData == "":
                        print "\n\n -- EOF --", x
                result, rmsg, _ = self.pcbObj.Read(self.pcbHandle)
                if result == PCB.PCAN_ERROR_OK:
                    if rmsg.ID == 0x6FF:
                        for i in xrange(rmsg.LEN):
                            self.tedConsole.insertPlainText(("%02x" % rmsg.DATA[i]).decode("hex"))
                            print ("%02x" % rmsg.DATA[i]).decode("hex")
                            timeout = 0
                else:
                    timeout += 1
                    print timeout,

    def closeEvent(self, event):
        self.close_window()

    def close_window(self):
        try:
            self.pcan_disconnect()
        except Exception:
            pass
        self.close()


if __name__ == "__main__":
    main()