def validIP(add):
	
	parts = add.split('.')
	if len(parts) != 4:
		return False

	for num in parts:
		try:
			if not 0 <= int(num) <= 255:
				return False

		except:
			return False

	return True


checkIP('192.168.1.1')