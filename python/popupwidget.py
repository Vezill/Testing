#!/usr/bin/python
# -*- encoding: utf-8 -*-

import sys
from PySide import QtGui, QtCore
import os

class MyPopup(QtGui.QWidget):

    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.initui()

    def initui(self):
        self.setWindowTitle('Update Battery Pack Firmware')
        self.resize(450, 220)
        self.setMinimumSize(QtCore.QSize(450, 220))
        self.setMaximumSize(QtCore.QSize(450, 200))


        self.edConsole = QtGui.QTextEdit()
        self.edConsole.setReadOnly(True)
        self.btnUpdate = QtGui.QPushButton('Select File && Update')
        self.btnBack = QtGui.QPushButton('Close this Window')

        self.btnUpdate.released.connect(self.open_file)
        self.btnBack.released.connect(self.close)

        self.hBox = QtGui.QHBoxLayout()
        self.hBox.addWidget(self.btnUpdate)
        self.hBox.addWidget(self.btnBack)
        self.vBox = QtGui.QVBoxLayout()
        self.vBox.addWidget(self.edConsole)
        self.vBox.addLayout(self.hBox)
        self.setLayout(self.vBox)

    def open_file(self):
        fname, _ = QtGui.QFileDialog.getOpenFileName(self, 'Open Update File', os.path.expanduser(r'~\Downloads'), '*.txt')
        self.edConsole.append(fname)
        self.update_battery(fname)

    def update_battery(self, fname):
        pass


class MainWindow(QtGui.QMainWindow):

    def __init__(self, *args):
        QtGui.QMainWindow.__init__(self, *args)
        self.cw = QtGui.QWidget(self)
        self.setCentralWidget(self.cw)
        self.btn1 = QtGui.QPushButton('Click Me', self.cw)
        self.btn1.setGeometry(QtCore.QRect(0, 0, 100, 30))
        self.btn1.released.connect(self.doit)
        self.w = None

    def doit(self):
        print 'Opening a new popup window...'
        self.w = MyPopup()
        self.w.setGeometry(QtCore.QRect(100, 100, 400, 200))
        self.w.show()

class App(QtGui.QApplication):

    def __init__(self, *args):
        QtGui.QApplication.__init__(self, *args)
        self.main = MainWindow()
        self.lastWindowClosed.connect(self.byebye)
        self.main.show()

    def byebye(self):
        self.exit(0)

def main(args):
    global app
    app = App(args)
    app.exec_()

if __name__ == '__main__':
    main(sys.argv)
