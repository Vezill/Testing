import random

class StackException(Exception): pass
class StackOverflow(StackException): pass
class StackUnderflow(StackException): pass

class StackObject(list):
    MAX_SIZE = 10

    def push(self, value):
        if len(self) <= self.MAX_SIZE:
            self.append(value)
        else:
            raise StackOverflow, 'Error: Stack is full - Stack Overflow raised.'

    def pull(self):
        if self:
            value = self[len(self)-1]
            self.remove(value)
            return value
        else:
            raise StackUnderflow, 'Error: Stack is empty - Stack Underflow raised.'

if __name__ == '__main__':
    stack = StackObject()
    for i in range(20):
        try:
            value = random.randint(1, 250)
            stack.push(value)
            print '{0} - Adding {1} to stack'.format(i, value)
        except Exception as error:
            print error
            break

    print '\nCurrent stack: {0}\n'.format(stack)

    for i in range(20):
        try:
            print '{0} - Removing {1} from stack'.format(i, stack.pull())
        except Exception as error:
            print error
            break
