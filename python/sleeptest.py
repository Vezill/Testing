#!/usr/bin/python
# Testing sleep and global interrupt
import time

tSec = int(raw_input('Sleep for: '))
iSec = int(raw_input('Check every: '))

cSec = 0
print 'Sleeping for %d out of %d seconds...' % (cSec, tSec)
print 'Press keyboard interrupt to skip'
try:
    while cSec != tSec:
        time.sleep(iSec)
        cSec += iSec
        print 'Sleeping for %d out of %d seconds...' % (cSec, tSec)
        print 'Press keyboard interrupt to skip'
except KeyboardInterrupt:
    print 'Skipping long sleep...'
