#!/usr/bin/env python
# -*- encoding: utf-8 -*-

""" Convert to and from Roman numerals. """

# Define exceptions
class RomanError(Exception): pass
class OutOfRangeError(RomanError): pass
class NotIntegerError(RomanError): pass
class InvalidRomanNumeralError(RomanError): pass

# Define digit mapping
roman_numeral_map = (
    ('M',  1000),
    ('CM', 900),
    ('D',  500),
    ('CD', 400),
    ('C',  100),
    ('XC', 90),
    ('L',  50),
    ('XL', 40),
    ('X',  10),
    ('IX', 9),
    ('V',  5),
    ('IV', 4),
    ('I',  1) )

def toRoman(n):
    """ Convert integer to Roman numeral. """
    if not 0 < n < 4000:
        raise OutOfRangeError, 'Number out of range (must be 1 to 3999).'
    if int(n) != n:
        raise NotIntegerError, 'Non-integers can not be converted.'

    result = ''
    for numeral, integer in roman_numeral_map:
        while n >= integer:
            result += numeral
            n -= integer
    return result

def fromRoman(s):
    """ Convert Roman numeral to integer. """
    pass