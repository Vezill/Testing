#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# Convert to and from Roman numerals

import re

# Define exceptions
class RomanError(Exception): pass
class OutOfRangeError(RomanError): pass
class NotIntegerError(RomanError): pass
class InvalidRomanNumeralError(RomanError): pass

# Define digit mapping
roman_numeral_map = (
    ('M',  1000),
    ('CM', 900),
    ('D',  500),
    ('CD', 400),
    ('C',  100),
    ('XC', 90),
    ('L',  50),
    ('XL', 40),
    ('X',  10),
    ('IX', 9),
    ('V',  5),
    ('IV', 4),
    ('I',  1) )

def toRoman(n):
    """ Convert integer to Roman numeral """
    if not 0 < n < 4000:
        raise OutOfRangeError, 'Number out of range (must be 1 to 3999).'
    if not isinstance(n, int):
        raise NotIntegerError, 'Non-integers can not be converted.'

    result = ''
    for numeral, integer in roman_numeral_map:
        while n >= integer:
            result += numeral
            n -= integer
    return result

# Define pattern to detect valid Roman numerals
roman_numeral_pattern = '^M?M?M?(CM|CD|D?C?C?C?)(XC|XL|L?X?X?X?)(IX|IV|V?I?I?I?)$'

def fromRoman(s):
    """ Convert Roman numeral to integer """
    if not s:
        raise InvalidRomanNumeralError, 'Input can not be blank.'
    if not re.search(roman_numeral_pattern, s):
        raise InvalidRomanNumeralError, 'Invalid Roman numeral: {0}.'.format(s)

    result = 0
    index = 0
    for numeral, integer in roman_numeral_map:
        while s[index:index+len(numeral)] == numeral:
            result += integer
            index += len(numeral)
    return result