#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# Convert to and from Roman numerals

import re

# Define exceptions
class RomanError(Exception): pass
class OutOfRangeError(RomanError): pass
class NotIntegerError(RomanError): pass
class InvalidRomanNumeralError(RomanError): pass

# Roman numerals must be less than 5000
MAX_ROMAN_NUMERAL = 4999

# Define digit mapping
roman_numeral_map = (
    ('M',  1000),
    ('CM', 900),
    ('D',  500),
    ('CD', 400),
    ('C',  100),
    ('XC', 90),
    ('L',  50),
    ('XL', 40),
    ('X',  10),
    ('IX', 9),
    ('V',  5),
    ('IV', 4),
    ('I',  1) )

# Create tables for fast conversion of roman numerals (see fillLookupTables())
to_roman_table = [ None ] # Skip an index since Roman numerals have no zero
from_roman_table = {}

def toRoman(n):
    """ Convert integer to Roman numeral """
    if not 0 < n <= MAX_ROMAN_NUMERAL:
        raise OutOfRangeError, 'Number out of range (must be 1 to {0}).'.format(MAX_ROMAN_NUMERAL)
    if not isinstance(n, int):
        raise NotIntegerError, 'Non-integers can not be converted.'

    return to_roman_table[n]

def fromRoman(s):
    """ Convert Roman numeral to integer """
    if not s:
        raise InvalidRomanNumeralError, 'Input can not be blank.'
    if not from_roman_table.has_key(s):
        raise InvalidRomanNumeralError, 'Invalid Roman numeral: {0}.'.format(s)

    return from_roman_table[s]

def toRomanDynamic(n):
    """ Convert integer to Roman numeral using dynamic programming. """
    result = ''
    for numeral, integer in roman_numeral_map:
        if n >= integer:
            result = numeral
            n -= integer
            break

    if n > 0:
        result += to_roman_table[n]
    return result

def fillLookupTables():
    """ Compute all the possible Roman numerals. """
    # Save the values in two global tables to convert to and from integers.
    for integer in range(1, MAX_ROMAN_NUMERAL + 1):
        roman_number = toRomanDynamic(integer)
        to_roman_table.append(roman_number)
        from_roman_table[roman_number] = integer

fillLookupTables()