#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# Convert to and from Roman numerals

import re

# Define exceptions
class RomanError(Exception): pass
class OutOfRangeError(RomanError): pass
class NotIntegerError(RomanError): pass
class InvalidRomanNumeralError(RomanError): pass

# Define digit mapping
roman_numeral_map = (
    ('M',  1000),
    ('CM', 900),
    ('D',  500),
    ('CD', 400),
    ('C',  100),
    ('XC', 90),
    ('L',  50),
    ('XL', 40),
    ('X',  10),
    ('IX', 9),
    ('V',  5),
    ('IV', 4),
    ('I',  1) )

def toRoman(n):
    """ Convert integer to Roman numeral """
    if not 0 < n < 5000:
        raise OutOfRangeError, 'Number out of range (must be 1 to 4999).'
    if not isinstance(n, int):
        raise NotIntegerError, 'Non-integers can not be converted.'

    result = ''
    for numeral, integer in roman_numeral_map:
        while n >= integer:
            result += numeral
            n -= integer
    return result

# Define pattern to detect valid Roman numerals
# roman_numeral_pattern = re.compile('^M?M?M?M?(CM|CD|D?C?C?C?)(XC|XL|L?X?X?X?)(IX|IV|V?I?I?I?)$')
# roman_numeral_pattern = re.compile('^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$')
roman_numeral_pattern = re.compile('''
^                   # Beginning of string
M{0,4}              # Thousands - 0 to 4 M's
(CM|CD|D?C{0,3})    # Hundreds - 900 (CM), 400 (CD), 0-300 (0 to 3 C's), or 500-800 (D + 0 to 3 C's)
(XC|XL|L?X{0,3})    # Tens - 90 (XC), 40 (XL), 0-30 (0 to 3 X's) or 50-80 (L + 0 to 3 X's)
(IX|IV|V?I{0,3})    # Ones - 9 (IX), 4 (IV), 0-3 (0 to 3 I's) or 5-8 (V + 0 to 3 I's)
$                   # End of string
''', re.VERBOSE)

def fromRoman(s):
    """ Convert Roman numeral to integer """
    if not s:
        raise InvalidRomanNumeralError, 'Input can not be blank.'
    if not roman_numeral_pattern.search(s):
        raise InvalidRomanNumeralError, 'Invalid Roman numeral: {0}.'.format(s)

    result = 0
    index = 0
    for numeral, integer in roman_numeral_map:
        while s[index:index+len(numeral)] == numeral:
            result += integer
            index += len(numeral)
    return result