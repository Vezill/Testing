#!/usr/bin/env python
# -*- encoding: utf-8 -*-

""" Convert to and from Roman numerals. """

# Define exceptions
class RomanError(Exception): pass
class OutOfRangeError(RomanError): pass
class NotIntegerError(RomanError): pass
class InvalidRomanNumeralError(RomanError): pass

def toRoman(n):
    """ Convert integer to Roman numeral. """
    pass

def fromRoman(s):
    """ Convert Roman numeral to integer. """
    pass