#!/usr/bin/env python
# -*- encoding: utf-8 -*-


""" Convert to and from Roman numerals. """

# define exceptions
class RomanError(Exception): pass
class OutOfRangeError(RomanError): pass
class NotIntegerError(RomanError): pass
class InvalidRomanNumeralError(RomanError): pass

# define digit mapping
roman_numeral_map = (
    ('M',  1000),
    ('CM', 900),
    ('D',  500),
    ('CD', 400),
    ('C',  100),
    ('XC', 90),
    ('L',  50),
    ('XL', 40),
    ('X',  10),
    ('IX', 9),
    ('V',  5),
    ('IV', 4),
    ('I',  1) )

def toRoman(n):
    """ Convert integer to Roman numeral. """
    result = ""
    for numeral, integer in roman_numeral_map:
        while n >= integer:
            result += numeral
            n -= integer
    return result

def fromRoman(s):
    """ Convert Roman numeral to integer. """
    pass