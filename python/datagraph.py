#!/usr/bin/env python
#-*- encoding: utf-8 -*-


import random
from PySide import QtCore, QtGui

import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4'] = 'PySide'
from matplotlib import pyplot


class MainWindow(QtGui.QWidget):
    def __init__(self):
        QtGui.QWidget.__init__(self)

        ui_display_graph_btn = QtGui.QPushButton('Display Graph')
        ui_update_graph_btn = QtGui.QPushButton('Update Graph')
        ui_exit_btn = QtGui.QPushButton('Exit')

        ui_display_graph_btn.released.connect(self.displayGraph)
        ui_exit_btn.released.connect(self.close)

        ui_main_layout = QtGui.QVBoxLayout()
        ui_main_layout.addWidget(ui_display_graph_btn)
        ui_main_layout.addWidget(ui_update_graph_btn)
        ui_main_layout.addWidget(ui_exit_btn)
        self.setLayout(ui_main_layout)

        self.setWindowTitle('Graph Test')

    def displayGraph(self):
        self.figure = pyplot.figure()
        self.figure.subplots_adjust(right=0.75)

        self.ax1 = self.figure.add_subplot(111)
        self.ax2 = self.ax1.twinx()
        self.ax3 = self.ax2.twinx()

        self.ax3.spines['right'].set_position(('axes', 1.2))
        self.ax3.set_frame_on(True)
        self.ax3.patch.set_visible(False)
        for spine in self.ax3.spines.itervalues():
            spine.set_visible(False)
        self.ax3.spines['right'].set_visible(True)

        self.plot1, = self.ax1.plot([0, 1, 2], [0, 1, 2], 'b-', label='Density')
        self.plot2, = self.ax2.plot([0, 1, 2], [0, 3, 2], 'r-', label='Temperature')
        self.plot3, = self.ax3.plot([0, 1, 2], [50, 30, 15], 'g-', label='Velocity')

        self.ax1.set_xlim(0, 2)
        self.ax1.set_ylim(0, 2)
        self.ax2.set_ylim(0, 4)
        self.ax3.set_ylim(1, 65)

        self.ax1.set_xlabel('Distance')
        self.ax1.set_ylabel(self.plot1.get_label())
        self.ax2.set_ylabel(self.plot2.get_label())
        self.ax3.set_ylabel(self.plot3.get_label())

        self.ax1.yaxis.label.set_color(self.plot1.get_color())
        self.ax2.yaxis.label.set_color(self.plot2.get_color())
        self.ax3.yaxis.label.set_color(self.plot3.get_color())

        tkw = dict(size=4, width=1.5)
        self.ax1.tick_params(axis='x', **tkw)
        self.ax1.tick_params(axis='y', colors=self.plot1.get_color(), **tkw)
        self.ax2.tick_params(axis='y', colors=self.plot2.get_color(), **tkw)
        self.ax3.tick_params(axis='y', colors=self.plot3.get_color(), **tkw)

        pyplot.xticks()

        lines = [self.plot1, self.plot2, self.plot3]
        self.ax1.legend(lines, [line.get_label() for line in lines])

        pyplot.show()


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    root = MainWindow()
    root.show()

    sys.exit(app.exec_())
