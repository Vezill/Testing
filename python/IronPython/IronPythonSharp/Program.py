﻿#!/usr/bin/env ironpython

import clr
clr.AddReferenceToFile("./dlls/USB Adapter Driver")

from USB_Adapter_Driver import USB_Adapter

device = USB_Adapter()
device.debugCapture(True, "./debug.log")

print "Device Connected: {0}".format(device.targetAttached())
print "Device Registered: {0}".format(device.register())
print "Device Version: {0}".format(device.version("")[1])

read_data = None
print device.i2cReadGeneric(1, array(0x20), 1, 11, read_data)