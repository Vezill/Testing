# pqt_progressbar1.py
# explore PyQT's progress bar and simple timer widgets
# Henri

import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *

class MyFrame(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        # setGeometry(x_pos, y_pos, width, height)
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('QProgressBar')

        self.pbar = QProgressBar(self)
        self.pbar.setGeometry(30, 40, 200, 25)

        self.button = QPushButton('Start', self)
        self.button.setFocusPolicy(Qt.NoFocus)
        # absolute positioning
        self.button.move(40, 80)

        self.connect(self.button, SIGNAL('clicked()'), self.onStart)

        # QBasicTimer is simplified timer
        # use QTimer for more options and control
        self.timer = QBasicTimer()
        self.step = 0;

    def timerEvent(self, event):
        """preset method for timer widget"""
        if self.step >= 100:
            self.timer.stop()
            return
        self.step += 1
        self.pbar.setValue(self.step)

    def onStart(self):
        """toggle button action and label"""
        if self.timer.isActive():
            self.timer.stop()
            self.button.setText('Start')
        else:
            # timer interval of 100 milliseconds
            self.timer.start(100, self)
            self.button.setText('Stop')


app = QApplication(sys.argv)
frame = MyFrame()
frame.show()
app.exec_()
