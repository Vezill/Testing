#!/bin/python

from PyQt4 import QtGui, QtCore
import sys

class MainWindow(QtGui.QWidget):

	def __init__(self):

		super(MainWindow, self).__init__()

		self.initUI()


	def initUI(self):

		button1 = QtGui.QPushButton('README')
		button1.released.connect(self.readme)

		hBox = QtGui.QHBoxLayout()
		hBox.addStretch(1)
		hBox.addWidget(button1)

		self.setLayout(hBox)

		self.resize(300, 150)
		self.setWindowTitle('Testing')


	def readme(self):

		rm = QtGui.QDialog(self)
		rm.resize(950, 800)

		eb = QtGui.QTextEdit(rm)
		eb.setText(open('README.txt').read())
		eb.setReadOnly(True)
		eb.setCurrentFont

		rmlayout = QtGui.QVBoxLayout()
		rmlayout.addWidget(eb)
		rm.setLayout(rmlayout)

		rm.show()


if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	root = MainWindow()
	root.show()

	sys.exit(app.exec_())