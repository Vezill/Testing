-==================================================================================================-
							Panacis Modbus Status Gui README!
-==================================================================================================-

PMSG v1.9.3
Python v2.7.1
Author: Nick Donais
Email: nick.donais@gmail.com
Company: Panacis
Website: panacis.com

-==================-
 Usage Instructions
-==================-

Controls from left to right

Slave IP EditBox:
input a valid IP address of a slave device to connect to. Press enter or the save button next to
the editbox to confirm entry. Number must be in format n.n.n.n where n is 0 <= n <= 255.

Refresh Rate EditBox:
input how often in seconds the program should update all values on the GUI. Press enter or the save
button next to the editbox to confirm entry.

Max Packs EditBox:
input the maximum number of packs on the slave device. This value is used when selecting the next
or previous pack, or entering a custom pack in. Press enter or the save button next to the editbox
to confirm entry.

Pack Selection Buttons:
' < ' Select Previous Pack
' # ' Select Custom Pack, number must be between 1 and Maximum Packs
' > ' Select Next Pack
Note: Current selected pack is displayed in Pack Status title, along with pack firmware. Also, when
selecting a pack there is a timeout of 5 seconds to make sure the pack selection lines up correctly.
An error message will be displayed if it doesn't get the right pack returned.

Enable Logging Button:
This enables logging to a file. The file is saved as 'PMSG (date).log' in the working directory.

Send Time Button:
Send the current time as a UNIX timestamp to the slave device. Can only be pressed once every 2
seconds.

Force Refresh Button:
Force Refresh all values on the GUI. NOTE! This can sometimes return 0 values if the data is
refreshed too quickly. This is a modbus update limitation, so don't panic right away (wait an extra
update or two before panicing).

Quit Button:
Self explainitory =D


Menubar Actions

Most menubar actions do exactly as the text displays. For more information look at the statusbar at
the bottom of the GUI.


System Information

First display is System Alerts, under it is Pack Faults.


Package Information

First display is Pack Status, under it is Pack Operational State


-==========================-
 Custom Library Information
-==========================-

modbus-tk v0.4.1
---------------
Modbus Test Kit
code.google.com/p/modbus-tk/

PyQt4 v4.8.4
-----------
Python Qt v4 Libraries
http://www.riverbankcomputing.co.uk/software/pyqt/intro

!NOTE!
Need to switch to PySide if GUI is to be used commercially, or purchase a PyQt4 Commercial License!

-=========-
 Changelog
-=========-

1.9.3
- added help (displays this file) and about actions in
- debug GUI designed, no code for it yet

1.9.0
- slight GUI touchups and adjustments
- added Help menu to menubar, features to come asap
- send time button now locks out for 2 seconds once clicked
- redesigned how logging and console output works, only need to call one or the other now as logging
  calls console output
- all errors now display a messagebox with the error to the user
- pack firmware now displayed in pack status title
- lots of user error checking added

1.8.5
- all widgets have statusbar text
- more bugfixes
- debugMode disabled again for another redesign/recode coming later

1.8.0
- bugfix release

1.7.0
- first release with major code rewrite
- still buggy
- system and pack information now displays returned hex value
- pack state displays english state and hex
- logging and console output redesigned and greatly cleaned up
- timestamps added to logging
- console output is for testing purposes mostly and only works when run from source files
- pack switching is handled by one method instead of multiple methods handling it on their own
- statusbar text for almost all displayed widgets added
- broken icons fixed
- now supports negative number returns (signed ints)
- modbus handling recoded

0.9.5
- fixed last few bugs
- cleaned up system and pack information output

0.8.5
- system and pack information now shows 3 different status icons!
- cleaned up layout of user controls

0.8.0
- Major GUI layout redesign
- can now send UNIX timestamp to slave
- debugMode enabled again, although still not happy with it

0.6.5
- displayed values now have measurement units also
- system and pack information now displays 'human-readable' information *
* may be reverted later

0.6
- logging feature added
- console output cleaned up
- debugMode scrapped and disabled for now
- can define custom slave IP
- selected pack shown in Pack Status titlebar
- force refresh button added

0.5
- menubar added
- menubar debugmode, console output (called logging) and quit added
- statusbar text added for most widgets
- can define custom pack, maximum packs and refresh rate
- now displays system and pack information, and system status text!

0.2
- GUI designed
- switch to next/previous pack
- quit button works!
- console output working
- basic debugMode scripted
- GUI automatically refreshes every 5 seconds

0.1
- First working version
- connect to hardcoded slave and display voltage and temperature values