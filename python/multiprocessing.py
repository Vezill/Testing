import os
from multiprocessing import Pool


def g(x):
    info('function g('+str(x)+')')
    return x * x

if __name__ == '__main__':
    info('main')
    pool = Pool(2)
    print pool.map(g, range(100))
