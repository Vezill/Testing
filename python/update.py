#!/usr/bin/python
# ./update.py
'''Testing PyQt.QTimer function for refreshing'''

import sys
import random

from PyQt4 import QtGui, QtCore

class Testing(QtGui.QWidget):

	def __init__(self, parent=None):

		super(Testing, self).__init__(parent)

		self.initUI()

	def initUI(self):

		self.randMin = 1
		self.randMax = 50

#===================================================================================================
# Min and Max settings area
		self.minLabel = QtGui.QLabel(self)
		self.minLabel.setText('Min')
		self.minLabel.setAlignment(QtCore.Qt.AlignCenter)
		self.minLabel.setGeometry(20, 10, 40, 15)

		self.minInfo = QtGui.QLineEdit(self)
		self.minInfo.setText(str(self.randMin))
		self.minInfo.setAlignment(QtCore.Qt.AlignCenter)
		self.minInfo.setGeometry(20, 25, 40, 15)

		self.avgLabel = QtGui.QLabel(self)
		self.avgLabel.setText('Avg')
		self.avgLabel.setAlignment(QtCore.Qt.AlignCenter)
		self.avgLabel.setGeometry(65, 10, 40, 15)

		self.avgInfo = QtGui.QLabel(self)
		self.avgInfo.setText('#!')
		self.avgInfo.setAlignment(QtCore.Qt.AlignCenter)
		self.avgInfo.setGeometry(65, 25, 40, 15)

		self.maxLabel = QtGui.QLabel(self)
		self.maxLabel.setText('Max')
		self.maxLabel.setAlignment(QtCore.Qt.AlignCenter)
		self.maxLabel.setGeometry(110, 10, 40, 15)

		self.maxInfo = QtGui.QLineEdit(self)
		self.maxInfo.setText(str(self.randMax))
		self.maxInfo.setAlignment(QtCore.Qt.AlignCenter)
		self.maxInfo.setGeometry(110, 25, 40, 15)

		self.mmSave = QtGui.QPushButton(self)
		self.mmSave.setText('Save')
		self.mmSave.setGeometry(175, 15, 50, 25)
		self.connect(self.mmSave, QtCore.SIGNAL('released()'), self.chgSetting)

#===================================================================================================
# Random Number area, testing QTimer
		self.random1 = QtGui.QLabel(self)
		self.random1.setText('#!')
		self.random1.setAlignment(QtCore.Qt.AlignCenter)
		self.random1.setGeometry(20, 70, 40, 15)

		self.random2 = QtGui.QLabel(self)
		self.random2.setText('#!')
		self.random2.setAlignment(QtCore.Qt.AlignCenter)
		self.random2.setGeometry(65, 70, 40, 15)

		self.random3 = QtGui.QLabel(self)
		self.random3.setText('#!')
		self.random3.setAlignment(QtCore.Qt.AlignCenter)
		self.random3.setGeometry(110, 70, 40, 15)

		self.random4 = QtGui.QLabel(self)
		self.random4.setText('#!')
		self.random4.setAlignment(QtCore.Qt.AlignCenter)
		self.random4.setGeometry(20, 90, 40, 15)

		self.random5 = QtGui.QLabel(self)
		self.random5.setText('#!')
		self.random5.setAlignment(QtCore.Qt.AlignCenter)
		self.random5.setGeometry(65, 90, 40, 15)

		self.random6 = QtGui.QLabel(self)
		self.random6.setText('#!')
		self.random6.setAlignment(QtCore.Qt.AlignCenter)
		self.random6.setGeometry(110, 90, 40, 15)

		self.random7 = QtGui.QLabel(self)
		self.random7.setText('#!')
		self.random7.setAlignment(QtCore.Qt.AlignCenter)
		self.random7.setGeometry(20, 110, 40, 15)

		self.random8 = QtGui.QLabel(self)
		self.random8.setText('#!')
		self.random8.setAlignment(QtCore.Qt.AlignCenter)
		self.random8.setGeometry(65, 110, 40, 15)

		self.random9 = QtGui.QLabel(self)
		self.random9.setText('#!')
		self.random9.setAlignment(QtCore.Qt.AlignCenter)
		self.random9.setGeometry(110, 110, 40, 15)

#===================================================================================================
# Update intervals settings
		self.updateLabel = QtGui.QLabel(self)
		self.updateLabel.setText('Refresh (s)')
		self.updateLabel.setAlignment(QtCore.Qt.AlignCenter)
		self.updateLabel.setGeometry(170, 70, 60, 20)

		self.upInt = QtGui.QLineEdit(self)
		self.upInt.setText('5')
		self.upInt.setGeometry(175, 90, 50, 20)

		self.upSave = QtGui.QPushButton(self)
		self.upSave.setText('Save')
		self.upSave.setGeometry(175, 110, 50, 20)
		self.connect(self.upSave, QtCore.SIGNAL('released()'), self.upSetting)

#===================================================================================================
# Setup timers
		self.refreshTimer = QtCore.QTimer()
		self.connect(self.refreshTimer, QtCore.SIGNAL('timeout()'), self.refreshNumbers)

#===================================================================================================
# Main Window
		self.setWindowTitle('Refresh Test')
		self.setGeometry(300, 300, 250, 150)
		self.refreshTimer.start(5000)

	def chgSetting(self):

		if (int(self.minInfo.text()) > 0 and int(self.minInfo.text()) < self.randMax):
			self.randMin = int(self.minInfo.text())
		else:
			self.minInfo.setText(str(self.randMin))
		if (int(self.maxInfo.text()) > self.randMin and int(self.maxInfo.text()) < 100):
			self.randMax = int(self.maxInfo.text())
		else:
			self.maxInfo.setText(str(self.randMax))

	def upSetting(self):

		if (int(self.upInt.text()) % 5 == 0 and int(self.upInt.text()) < 61):
			self.refreshTimer.stop()
			self.refreshTimer.start(int(self.upInt.text()) * 1000)
		else:
			self.upInt.setText(str(self.update))

	def refreshNumbers(self):

		self.random1.setText(str(random.randint(self.randMin, self.randMax)))
		self.random2.setText(str(random.randint(self.randMin, self.randMax)))
		self.random3.setText(str(random.randint(self.randMin, self.randMax)))
		self.random4.setText(str(random.randint(self.randMin, self.randMax)))
		self.random5.setText(str(random.randint(self.randMin, self.randMax)))
		self.random6.setText(str(random.randint(self.randMin, self.randMax)))
		self.random7.setText(str(random.randint(self.randMin, self.randMax)))
		self.random8.setText(str(random.randint(self.randMin, self.randMax)))
		self.random9.setText(str(random.randint(self.randMin, self.randMax)))

		avg = (int(self.random1.text()) + int(self.random2.text()) + int(self.random3.text()) + 
			   int(self.random4.text()) + int(self.random5.text()) + int(self.random6.text()) + 
			   int(self.random7.text()) + int(self.random8.text()) + int(self.random9.text()))
		avg = avg / 9
		self.avgInfo.setText(str(avg))


if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	root = Testing()
	root.show()
	app.exec_()