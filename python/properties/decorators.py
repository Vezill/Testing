#!/usr/bin/env python
# -*- encoding: utf-8 -*-

class C(object):
    @property
    def x(self):
        try:
            return self._x
        except:
            return 'This variable does not exist!'

    @x.setter
    def x(self, value):
        self._x = value

    @x.deleter
    def x(self):
        del self._x


test = C()
test.x = 'test'
print test.x
del test.x
print test.x