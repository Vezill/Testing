#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import unittest


class Person(object):
    def __init__(self, first_name, last_name):
        self._first_name = first_name
        self._last_name = last_name

    @property
    def first_name(self):   # first_name = property(first_name)
        """ doc comment for first_name getter """
        return self._first_name

    @first_name.setter
    def first_name(self, first_name):   # first_name = first_name.setter(first_name)
        self._first_name = first_name

    @property
    def last_name(self):
        """ doc comment for last_name getter """
        return self._last_name

    @last_name.setter
    def last_name(self, last_name):
        self._last_name = last_name

    def __str__(self):
        return '{0} {1}'.format(self.first_name, self.last_name)


class PersonTests(unittest.TestCase):
    def test_init(self):
        person = Person('eddie', 'vedder')
        self.assertEqual('eddie vedder', str(person), 'init')

    def test_first_name(self):
        person = Person('eddie', 'vedder')
        person.first_name = 'bill'
        self.assertEqual('bill vedder', str(person), 'first_name')

    def test_setters(self):
        person = Person('eddie', 'vedder')
        person.first_name = 'bill'
        person.last_name = 'murray'
        self.assertEqual('bill murray', str(person), 'first_name + last_name')


if __name__ == '__main__':
    unittest.main()
