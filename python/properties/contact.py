#!/usr/bin/env python
# -*- encoding: utf-8 -*-


class Contact(object):
    def __init__(self, first_name=None, last_name=None, display_name=None, email=None):
        self.first_name = first_name
        self.last_name = last_name
        self.display_name = display_name
        self.email = email

    def print_info(self):
        print '{0} <{1}>'.format(self.display_name, self.email)

    def set_email(self, value):
        if '@' not in value:
            raise Exception("This doesn't look like an email address.")
        self._email = value

    def get_email(self):
        return self._email

    email = property(get_email, set_email)


contact = Contact('Test', 'Testif', 'bleach', 'y@y.com')
contact.email = 'x@x.com'
contact.print_info()