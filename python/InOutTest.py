#!/usr/bin/env python2.7
# -*- encoding: utf-8 -*-

import sys
import time

def main():
    with open("bl-AirforceTower.txt", "rb") as rfile:
        with open("outfile.txt", "ab+") as wfile:
            wfile.write(rfile.readline)


if __name__ == "__main__":
    main()
