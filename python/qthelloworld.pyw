#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from PySide import QtGui


class MainWindow(QtGui.QWidget):

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        ui_hello_lbl = QtGui.QLabel('Hello, World!')
        ui_quit_btn = QtGui.QPushButton('Close Window')

        ui_quit_btn.released.connect(self.close)

        ui_vert_layout = QtGui.QVBoxLayout()
        ui_vert_layout.addStretch(1)
        ui_vert_layout.addWidget(ui_hello_lbl)
        ui_vert_layout.addWidget(ui_quit_btn)
        ui_vert_layout.addStretch(1)

        ui_main_layout = QtGui.QHBoxLayout()
        ui_main_layout.addStretch(1)
        ui_main_layout.addLayout(ui_vert_layout)
        ui_main_layout.addStretch(1)

        ui_quit_btn.setFocus()

        self.setLayout(ui_main_layout)
        self.setWindowTitle('Hello World Executable')


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    root = MainWindow()
    root.show()

    sys.exit(app.exec_())