import sys
from PyQt4 import QtGui, QtCore
from modbusTest import Ui_rootWindow

class mainWindow(QtGui.QMainWindow, Ui_rootWindow):

	def __init__(self, parent=None):

		super(mainWindow, self).__init__(parent)
		self.setupUi(self)


if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	gui = mainWindow()
	gui.show()
	app.exec_()