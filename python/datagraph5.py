#!/usr/bin/env python
#-*- encoding: utf-8 -*-


import random
from datetime import datetime
from PySide import QtCore, QtGui

import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4'] = 'PySide'
from matplotlib import dates
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as QFigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as QNavigationToolbar
from matplotlib.figure import Figure


class GraphWidget(QFigureCanvas):
    """ Custom matplotlib graph widget for testing purposes. """
    def __init__(self, data, parent=None):
        self.figure = Figure()

        QFigureCanvas.__init__(self, self.figure)
        self.setParent(parent)

        self.initGraph(data)

    def initGraph(self, data):
        # Set up axes
        self.axes = [None]*3
        self.axes[0] = self.figure.add_subplot(311)
        self.axes[1] = self.figure.add_subplot(312)
        self.axes[2] = self.figure.add_subplot(313)

        # Set xaxis format to time
        format = dates.DateFormatter('%H:%M:%S')
        for ax in self.axes:
            ax.xaxis.set_major_formatter(format)

        # Set up plot data
        curr = [[data['curr'][i][0] for i in range(len(data['curr']))],
            [data['curr'][i][1] for i in range(len(data['curr']))]]
        volt = [[data['volt'][i][0] for i in range(len(data['volt']))],
            [data['volt'][i][1] for i in range(len(data['volt']))]]
        temp = [[data['temp'][i][0] for i in range(len(data['temp']))],
            [data['temp'][i][1] for i in range(len(data['temp']))]]

        self.plots = [None]*3
        self.plots[0], = self.axes[0].plot(curr[0], curr[1], 'b-', label='Current')
        self.plots[1], = self.axes[1].plot(volt[0], volt[1], 'r-', label='Voltage')
        self.plots[2], = self.axes[2].plot(temp[0], temp[1], 'g-', label='Temperature')

        # Set up appearance
        tkw = dict(size=4, width=1.5)
        for i in range(3):
            self.axes[i].set_ylabel(self.plots[i].get_label())
            self.axes[i].yaxis.label.set_color(self.plots[i].get_color())
            self.axes[i].tick_params(axis='x', **tkw)

        #self.figure.set_facecolor('#CD8500')

    def setRanges(self, curr=None, volt=None, temp=None):
        """ """
        if curr: self.axes[0].set_ylim(curr[0], curr[1])
        if volt: self.axes[1].set_ylim(volt[0], volt[1])
        if temp: self.axes[2].set_ylim(temp[0], temp[1])

        low = QtCore.QDateTime.currentDateTime().addSecs(-60).toPython()
        upp = QtCore.QDateTime.currentDateTime().addSecs(2).toPython()

        for i in range(3):
            self.axes[i].set_xlim(low, upp)

    def updateGraph(self, data=None):
        """ """
        if data:
            curr = [[data['curr'][i][0] for i in range(len(data['curr']))],
                [data['curr'][i][1] for i in range(len(data['curr']))]]
            volt = [[data['volt'][i][0] for i in range(len(data['volt']))],
                [data['volt'][i][1] for i in range(len(data['volt']))]]
            temp = [[data['temp'][i][0] for i in range(len(data['temp']))],
                [data['temp'][i][1] for i in range(len(data['temp']))]]

            self.plots[0].set_data(curr[0], curr[1])
            self.plots[1].set_data(volt[0], volt[1])
            self.plots[2].set_data(temp[0], temp[1])

            self.setRanges()

        self.figure.canvas.draw()


class GraphWindow(QtGui.QMainWindow):
    """ """
    def __init__(self, data):
        QtGui.QMainWindow.__init__(self)

        now = QtCore.QDateTime.currentDateTime().toPython()
        curr, min_curr, max_curr = data[0]
        volt, min_volt, max_volt = data[1]
        temp, min_temp, max_temp = data[2]

        self.data = dict(curr=[], volt=[], temp=[])
        self.data['curr'].append([now, curr])
        self.data['volt'].append([now, volt])
        self.data['temp'].append([now, temp])

        self.ranges = dict(curr=[min_curr, max_curr], volt=[min_volt, max_volt],
            temp=[min_temp, max_temp])

        self.initUi()

    def initUi(self):
        # Create and set up graph
        self.ui_graph_widget = GraphWidget(self.data)
        self.ui_graph_widget.setRanges(curr=self.ranges['curr'], volt=self.ranges['volt'],
            temp=self.ranges['temp'])

        # Set up and attach the navigation bar
        self.ui_nav_tbar = QNavigationToolbar(self.ui_graph_widget, self, coordinates=False)
        self.ui_nav_tbar.setMovable(False)

        actions = self.ui_nav_tbar.actions()
        self.ui_nav_tbar.removeAction(actions[7])
        self.ui_nav_tbar.removeAction(actions[8])

        self.addToolBar(self.ui_nav_tbar)
        self.setCentralWidget(self.ui_graph_widget)

    def updateData(self, new_data):
        now = QtCore.QDateTime.currentDateTime().toPython()

        for item in self.data:
            if len(item) > 1800:
                item.pop(0)

        self.data['curr'].append([now, new_data[0]])
        self.data['volt'].append([now, new_data[1]])
        self.data['temp'].append([now, new_data[2]])

        self.ui_graph_widget.updateGraph(self.data)


class DataDialog(QtGui.QDialog):
    def __init__(self):
        QtGui.QDialog.__init__(self)
        self.initUi()

    def initUi(self):
        data = [[8, -15, 15], [3550, 3400, 3700], [25, 10, 30]]
        self.ui_graph_window = GraphWindow(data)

        self.ui_graph_display_btn = QtGui.QPushButton('Display Graph')
        self.ui_timer_state_btn = QtGui.QPushButton('Stop Updating')
        ui_exit_btn = QtGui.QPushButton('Exit')

        ui_main_layout = QtGui.QVBoxLayout()
        ui_main_layout.addWidget(self.ui_graph_display_btn)
        ui_main_layout.addWidget(self.ui_timer_state_btn)
        ui_main_layout.addWidget(ui_exit_btn)
        self.setLayout(ui_main_layout)

        self.update_timer = QtCore.QTimer()

        self.ui_graph_display_btn.released.connect(self.displayGraphWindow)
        self.ui_timer_state_btn.released.connect(self.setTimerState)
        ui_exit_btn.released.connect(self.close)
        self.update_timer.timeout.connect(self.sendDataUpdate)

    def closeEvent(self, event):
        self.ui_graph_window.close()
        event.accept()

    def displayGraphWindow(self):
        self.update_timer.start(2000)
        self.ui_graph_window.show()

    def setTimerState(self):
        if 'Stop' in self.ui_timer_state_btn.text():
            self.update_timer.stop()
            self.ui_timer_state_btn.setText('Start Updating')

        elif 'Start' in self.ui_timer_state_btn.text():
            self.update_timer.start(2000)
            self.ui_timer_state_btn.setText('Stop Updating')

    def sendDataUpdate(self):
        data = [random.randint(0, 8), random.randint(3500, 3600), random.randint(20, 25)]
        self.ui_graph_window.updateData(data)


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    root = DataDialog()
    root.show()

    sys.exit(app.exec_())
