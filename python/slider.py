#!/usr/bin/python
# -*- coding: utf-8 -*- 

# slider.py

import sys
from PyQt4 import QtGui, QtCore

class Example(QtGui.QWidget):

	def __init__(self):

		super(Example, self).__init__()

		self.initUI()

	def initUI(self):

		self.curSpinBox = QtGui.QSpinBox(self)
		self.curSpinBox.setMinimum(1)
		self.curSpinBox.setMaximum(30)
		self.curSpinBox.setValue(1)
		self.curSpinBox.setSingleStep(1)
		self.curSpinBox.setGeometry(20, 40, 49, 24)
		self.connect(self.curSpinBox, QtCore.SIGNAL('valueChanged(int)'), self.packChange)

		self.slider = QtGui.QSlider(QtCore.Qt.Horizontal, self)
		self.slider.setFocusPolicy(QtCore.Qt.NoFocus)
		self.slider.setMinimum(1)
		self.slider.setMaximum(30)
		self.slider.setValue(1)
		self.slider.setTickInterval(1)
		self.slider.setGeometry(75, 40, 100, 30)
		self.connect(self.slider, QtCore.SIGNAL('valueChanged(int)'), self.packChange)

		self.maxSpinBox = QtGui.QSpinBox(self)
		self.maxSpinBox.setMinimum(1)
		self.maxSpinBox.setMaximum(99)
		self.maxSpinBox.setValue(30)
		self.maxSpinBox.setSingleStep(1)
		self.maxSpinBox.setGeometry(190, 40, 49, 24)
		self.connect(self.maxSpinBox, QtCore.SIGNAL('valueChanged(int)'), self.maxChange)

		self.label = QtGui.QLabel(self)
		self.label.setText("0")
		self.label.setGeometry(120, 20, 80, 30)

		self.setWindowTitle('Slider')
		self.setGeometry(300, 300, 250, 150)

	def packChange(self, value):

		if value <= self.maxSpinBox.value() and value > 0:
			self.label.setText("%d" % value)
			self.curSpinBox.setValue(value)
			self.slider.setValue(value)

	def maxChange(self, value):

		if self.curSpinBox.value() > value:
			self.curSpinBox.setValue(value)
		self.curSpinBox.setMaximum(value)

		if self.slider.value() > value:
			self.slider.setValue(value)
		self.slider.setMaximum(value)

	def keyPressEvent(self, event):
		
		if event.key() == QtCore.Qt.Key_Left:
			self.packChange(self.slider.value() - 1)
		if event.key() == QtCore.Qt.Key_Right:
			self.packChange(self.slider.value() + 1)


if __name__ == '__main__':

	app = QtGui.QApplication(sys.argv)
	ex = Example()
	ex.show()
	app.exec_()