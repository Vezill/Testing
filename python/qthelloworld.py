#!/usr/bin/env python2.7
# -*- encoding: utf-8 -*-

from PySide import QtCore, QtGui


class QtHelloWorld(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        ui_hello_lbl = QtGui.QLabel("Hello, Qt!")
        ui_hello_lbl.setAlignment(QtCore.Qt.AlignCenter)
        ui_close_btn = QtGui.QPushButton("Bye, Qt!")

        layout = QtGui.QVBoxLayout()
        layout.addWidget(ui_hello_lbl)
        layout.addWidget(ui_close_btn)
        self.setLayout(layout)

        ui_close_btn.clicked.connect(self.close)
        ui_close_btn.setFocus()

        self.setWindowTitle("Hello world! Qt Style!")


if __name__ == "__main__":
    import sys

    app = QtGui.QApplication([])
    root = QtHelloWorld()
    root.show()

    sys.exit(app.exec_())
