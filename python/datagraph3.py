#!/usr/bin/env python
#-*- encoding: utf-8 -*-


import datetime
import random
from PySide import QtCore, QtGui

import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4'] = 'PySide'
from matplotlib import dates
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as QFigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as QNavigationToolbar
from matplotlib.figure import Figure


class GraphWidget(QFigureCanvas):
    def __init__(self, data, parent=None):
        self.fig = Figure()
        self.fig.subplots_adjust(left=0.2)

        QFigureCanvas.__init__(self, self.fig)
        self.setParent(parent)

        self.axes = [None]*3
        self.axes[0] = self.figure.add_subplot(111)
        self.axes[1] = self.axes[0].twinx()
        self.axes[2] = self.axes[1].twinx()

        self.axes[1].spines['left'].set_position(('axes', -0.12))
        self.axes[1].set_frame_on(True)
        self.axes[1].patch.set_visible(False)
        for spine in self.axes[1].spines.itervalues():
            spine.set_visible(False)
        self.axes[1].spines['left'].set_visible(True)

        format = dates.DateFormatter('%H:%M:%S')
        for ax in self.axes:
#            ax.xaxis.set_major_locator(dates.SecondLocator())
            ax.xaxis.set_major_formatter(format)

        self.plots = [None]*3
        curr = [[data['curr'][i][0] for i in range(len(data['curr']))], 
            [data['curr'][i][1] for i in range(len(data['curr']))]]
        volt = [[data['volt'][i][0] for i in range(len(data['volt']))],
            [data['volt'][i][1] for i in range(len(data['volt']))]]
        temp = [[data['temp'][i][0] for i in range(len(data['temp']))],
            [data['temp'][i][1] for i in range(len(data['temp']))]]
        
        self.plots[0], = self.axes[0].plot(curr[0], curr[1], 'b-', label='Current')
        self.plots[1], = self.axes[1].plot(volt[0], volt[1], 'r-', label='Voltage')
        self.plots[2], = self.axes[2].plot(temp[0], temp[1], 'g-', label='Temperature')

        self.axes[0].set_ylabel(self.plots[0].get_label())
        self.axes[1].set_ylabel(self.plots[1].get_label())
        self.axes[2].set_ylabel(self.plots[2].get_label())

        self.axes[1].yaxis.set_label_position('left')

        self.axes[0].yaxis.label.set_color(self.plots[0].get_color())
        self.axes[1].yaxis.label.set_color(self.plots[1].get_color())
        self.axes[2].yaxis.label.set_color(self.plots[2].get_color())

        tkw = dict(size=4, width=1.5)
        self.axes[0].tick_params(axis='x', **tkw)
        self.axes[0].tick_params(axis='y', colors=self.plots[0].get_color(), **tkw)
        self.axes[1].tick_params(axis='y', colors=self.plots[1].get_color(), **tkw)
        self.axes[2].tick_params(axis='y', colors=self.plots[2].get_color(), **tkw)

        for lbl in self.axes[0].get_xticklabels():
            lbl.set_rotation(25)

    def setRanges(self, curr=None, volt=None, temp=None):
        if curr:
            self.axes[0].set_ylim(curr[0], curr[1])
        if volt:
            self.axes[1].set_ylim(volt[0], volt[1])
        if temp:
            self.axes[2].set_ylim(temp[0], temp[1])

        now = QtCore.QTime.currentTime()
        low = datetime.datetime.strptime(now.addSecs(-60).toString('HH:mm:ss'), '%H:%M:%S')
        upp = datetime.datetime.strptime(now.addSecs(2).toString('HH:mm:ss'), '%H:%M:%S')

        self.axes[0].set_xlim(low, upp)

    def changeVisible(self, plot, state):
        self.plots[plot].set_visible(state)
        self.axes[plot].yaxis.set_visible(state)
        for lbl in self.axes[plot].get_yticklabels():
            lbl.set_visible(state)

        if plot == 1:
            self.axes[1].spines['left'].set_visible(state)

        if self.plots[0].get_visible() is False:
            self.axes[1].spines['left'].set_position(('axes', 0))
        else:
            self.axes[1].spines['left'].set_position(('axes', -0.12))

        self.updateDisplay()

    def updateDisplay(self, data=None):
        if data:
            curr = [[data['curr'][i][0] for i in range(len(data['curr']))],
                [data['curr'][i][1] for i in range(len(data['curr']))]]
            volt = [[data['volt'][i][0] for i in range(len(data['volt']))],
                [data['volt'][i][1] for i in range(len(data['volt']))]]
            temp = [[data['temp'][i][0] for i in range(len(data['temp']))],
                [data['temp'][i][1] for i in range(len(data['temp']))]]

            self.plots[0].set_data(curr[0], curr[1])
            self.plots[1].set_data(volt[0], volt[1])
            self.plots[2].set_data(temp[0], temp[1])

            self.setRanges()

        self.fig.canvas.draw()


class DataWidget(QtGui.QWidget):
    def __init__(self, data, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.initUi(data)

    def initUi(self, data):

        self.ui_graph_widget = GraphWidget(data)
        self.ui_graph_widget.setRanges(curr=[-50, 50], volt=[3400, 3800], temp=[10, 30])

        # Basic display settings widgets
        ui_display_curr_chk = QtGui.QCheckBox('Current')
        ui_display_volt_chk = QtGui.QCheckBox('Voltage')
        ui_display_temp_chk = QtGui.QCheckBox('Temperature')

        ui_display_curr_chk.setStyleSheet('QCheckBox { color: blue; }')
        ui_display_volt_chk.setStyleSheet('QCheckBox { color: red; }')
        ui_display_temp_chk.setStyleSheet('QCheckBox { color: green; }')

        ui_display_curr_chk.setChecked(True)
        ui_display_volt_chk.setChecked(True)
        ui_display_temp_chk.setChecked(True)

        ui_basic_layout = QtGui.QVBoxLayout()
        ui_basic_layout.addWidget(ui_display_curr_chk)
        ui_basic_layout.addWidget(ui_display_volt_chk)
        ui_basic_layout.addWidget(ui_display_temp_chk)

        ui_basic_grp = QtGui.QGroupBox('Display Settings')
        self.setFontBold(ui_basic_grp)
        ui_basic_grp.setLayout(ui_basic_layout)

        # Advanced display settings widget

        ui_advanced_layout = QtGui.QVBoxLayout()
        
        ui_advanced_grp = QtGui.QGroupBox('Advanced Settings')
        self.setFontBold(ui_advanced_grp)
        ui_advanced_grp.setLayout(ui_advanced_layout)
        ui_advanced_grp.setVisible(False)

        # Everything put together
        self.ui_enable_adv_btn = QtGui.QPushButton('Advanced Settings')
        self.ui_enable_adv_btn.setCheckable(True)

        ui_settings_layout = QtGui.QVBoxLayout()
        ui_settings_layout.addStretch(1)
        ui_settings_layout.addWidget(ui_basic_grp)
        ui_settings_layout.addWidget(ui_advanced_grp)
        ui_settings_layout.addStretch(1)
        ui_settings_layout.addWidget(self.ui_enable_adv_btn)

        ui_main_layout = QtGui.QHBoxLayout()
        ui_main_layout.addWidget(self.ui_graph_widget, stretch=1)
        ui_main_layout.addLayout(ui_settings_layout, stretch=0)

        self.ui_navigation_tbar = QNavigationToolbar(self.ui_graph_widget, self)
        self.setLayout(ui_main_layout)


        # Signals
        ui_display_curr_chk.toggled.connect(self.changeDisplay)
        ui_display_volt_chk.toggled.connect(self.changeDisplay)
        ui_display_temp_chk.toggled.connect(self.changeDisplay)

        self.ui_enable_adv_btn.toggled.connect(ui_advanced_grp.setVisible)

    def changeDisplay(self, state):
        source = self.sender()

        if source.text() == 'Current':
            plot = 0
        elif source.text() == 'Voltage':
            plot = 1
        elif source.text() == 'Temperature':
            plot = 2
        else:
            return

        self.ui_graph_widget.changeVisible(plot, state)

    def updateGraph(self, data):
        self.ui_graph_widget.updateDisplay(data)
        
    def setFontBold(self, widget):
        bold_font = widget.font()
        bold_font.setBold(True)
        widget.setFont(bold_font)


class MainWindow(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.data = dict(curr=[], volt=[], temp=[])
        time_str = QtCore.QTime.currentTime().toString('HH:mm:ss')
        now = datetime.datetime.strptime(time_str, '%H:%M:%S')

        self.data['curr'].append([now, 8])
        self.data['volt'].append([now, 3550])
        self.data['temp'].append([now, 25])
        
        self.initUi()

    def initUi(self):
        self.ui_data_display_widget = DataWidget(self.data)

        self.ui_display_graph_btn = QtGui.QPushButton('Display Graph')
        ui_exit_btn = QtGui.QPushButton('Exit')

        ui_main_layout = QtGui.QVBoxLayout()
        ui_main_layout.addWidget(self.ui_display_graph_btn)
        ui_main_layout.addWidget(ui_exit_btn)
        self.setLayout(ui_main_layout)
        
        self.update_timer = QtCore.QTimer()
        self.update_timer.timeout.connect(self.sendDataUpdate)

        self.ui_display_graph_btn.released.connect(self.displayDataWidget)
        ui_exit_btn.released.connect(self.close)

    def closeEvent(self, event):
        self.ui_data_display_widget.close()
        event.accept()

    def displayDataWidget(self):
        self.update_timer.start(2000)
        self.ui_data_display_widget.show()
        
    def sendDataUpdate(self):
        time_str = QtCore.QTime.currentTime().toString('HH:mm:ss')
        now = datetime.datetime.strptime(time_str, '%H:%M:%S')
        
        self.data['curr'].append([now, random.randint(-50, 50)])
        self.data['volt'].append([now, random.randint(3400, 3800)])
        self.data['temp'].append([now, random.randint(10, 30)])

        self.ui_data_display_widget.updateGraph(self.data)


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    root = MainWindow()
    root.show()

    sys.exit(app.exec_())
