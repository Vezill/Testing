#!/usr/bin/python
# -*- encoding: utf-8 -*-
tmpData = []
with open("./bl-AirforceTower.txt", "rb") as f:
    with open("./testoutput.txt", "wb") as w:
        tmpRead = f.read(1)
        i = 0
        x = 0
        while tmpRead != '':
            i += 1
            w.write(tmpRead)
            print "%s" % tmpRead.encode("hex").upper(),
            #print '%s' % tmpRead.upper(),
            tmpRead = f.read(1)
            if not i % 8:
                print "|",
            if not i % 16:
                x += 1
                print
        print "Line Count: %d (%s)" % (x-1, hex((x-1)*16))