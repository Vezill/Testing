#!/usr/bin/env python
#-*- encoding: utf-8 -*-


import random
from PySide import QtCore, QtGui

import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4'] = 'PySide'
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as QFigureCanvas
from matplotlib.figure import Figure


class GraphWidget(QFigureCanvas):
    def __init__(self):
        self.figure = Figure()
        self.figure.subplots_adjust(right=0.75)

        QFigureCanvas.__init__(self, self.figure)

        self.axes = [None]*3
        self.axes[0] = self.figure.add_subplot(111)
        self.axes[1] = self.axes[0].twinx()
        self.axes[2] = self.axes[1].twinx()

        self.axes[2].spines['right'].set_position(('axes', 1.1))
        self.axes[2].set_frame_on(True)
        self.axes[2].patch.set_visible(False)
        for spine in self.axes[2].spines.itervalues():
            spine.set_visible(False)
        self.axes[2].spines['right'].set_visible(True)

        self.plots = [None]*3
        self.plots[0], = self.axes[0].plot([0, 1, 2], [0, 1, 2], 'b-', label='Density')
        self.plots[1], = self.axes[1].plot([0, 1, 2], [0, 3, 2], 'r-', label='Temperature')
        self.plots[2], = self.axes[2].plot([0, 1, 2], [50, 30, 15], 'g-', label='Velocity')

        self.axes[0].set_xlim(0, 2)
        self.axes[0].set_ylim(0, 2)
        self.axes[1].set_ylim(0, 4)
        self.axes[2].set_ylim(1, 65)

        self.axes[0].set_xlabel('Distance')
        self.axes[0].set_ylabel(self.plots[0].get_label())
        self.axes[1].set_ylabel(self.plots[1].get_label())
        self.axes[2].set_ylabel(self.plots[2].get_label())

        self.axes[0].yaxis.label.set_color(self.plots[0].get_color())
        self.axes[1].yaxis.label.set_color(self.plots[1].get_color())
        self.axes[2].yaxis.label.set_color(self.plots[2].get_color())

        tkw = dict(size=4, width=1.5)
        self.axes[0].tick_params(axis='x', **tkw)
        self.axes[0].tick_params(axis='y', colors=self.plots[0].get_color(), **tkw)
        self.axes[1].tick_params(axis='y', colors=self.plots[1].get_color(), **tkw)
        self.axes[2].tick_params(axis='y', colors=self.plots[2].get_color(), **tkw)

        lines = [self.plots[0], self.plots[1], self.plots[2]]
        self.axes[0].legend(lines, [line.get_label() for line in lines])
        
    def changeVisible(self, plot, state):
        self.plots[plot].set_visible(state)
        self.axes[plot].yaxis.label.set_visible(state)
        for lbl in self.axes[plot].get_yticklabels():
            lbl.set_visible(state)
        self.updateDisplay()
        
    def updateDisplay(self):
        self.figure.canvas.draw()
        
    
class DataWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.initUi()
        
    def initUi(self):
        self.ui_graph_widget = GraphWidget()

        ui_display_lbl = QtGui.QLabel('Display Settings')
        bold_font = ui_display_lbl.font()
        bold_font.setBold(True)
        ui_display_lbl.setFont(bold_font)

        ui_display_curr_chk = QtGui.QCheckBox('Current')
        ui_display_volt_chk = QtGui.QCheckBox('Voltage')
        ui_display_temp_chk = QtGui.QCheckBox('Temperature')

        ui_display_curr_chk.setChecked(True)
        ui_display_volt_chk.setChecked(True)
        ui_display_temp_chk.setChecked(True)

        ui_settings_layout = QtGui.QVBoxLayout()
        ui_settings_layout.addStretch(1)
        ui_settings_layout.addWidget(ui_display_lbl)
        ui_settings_layout.addWidget(ui_display_curr_chk)
        ui_settings_layout.addWidget(ui_display_volt_chk)
        ui_settings_layout.addWidget(ui_display_temp_chk)
        ui_settings_layout.addStretch(1)

        ui_main_layout = QtGui.QHBoxLayout()
        ui_main_layout.addWidget(self.ui_graph_widget)
        ui_main_layout.addLayout(ui_settings_layout)
        self.setLayout(ui_main_layout)

        ui_display_curr_chk.toggled.connect(self.changeDisplay)
        ui_display_volt_chk.toggled.connect(self.changeDisplay)
        ui_display_temp_chk.toggled.connect(self.changeDisplay)
        
    def changeDisplay(self, state):
        source = self.sender()

        if source.text() == 'Current':
            plot = 0
        elif source.text() == 'Voltage':
            plot = 1
        elif source.text() == 'Temperature':
            plot = 2
        else:
            return

        self.ui_graph_widget.changeVisible(plot, state)


class MainWindow(QtGui.QWidget):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.initUi()

    def initUi(self):
        self.ui_data_display_widget = DataWidget()

        self.ui_display_graph_btn = QtGui.QPushButton('Display Graph')
        self.ui_animate_btn = QtGui.QPushButton('Start')
        ui_exit_btn = QtGui.QPushButton('Exit')

        ui_main_layout = QtGui.QVBoxLayout()
        ui_main_layout.addWidget(self.ui_display_graph_btn)
        ui_main_layout.addWidget(self.ui_animate_btn)
        ui_main_layout.addWidget(ui_exit_btn)
        self.setLayout(ui_main_layout)

        self.ui_display_graph_btn.released.connect(self.displayGraph)
        self.ui_animate_btn.released.connect(self.startData)
        ui_exit_btn.released.connect(self.close)

    def closeEvent(self, event):
        self.ui_data_display_widget.close()
        event.accept()

    def displayGraph(self):
        self.ui_display_graph_btn.setText('Close Graph')
        self.ui_display_graph_btn.released.disconnect()
        self.ui_display_graph_btn.released.connect(self.closeGraph)
        self.ui_data_display_widget.show()

    def closeGraph(self):
        self.ui_display_graph_btn.setText('Display Graph')
        self.ui_display_graph_btn.released.disconnect()
        self.ui_display_graph_btn.released.connect(self.displayGraph)
        self.ui_data_display_widget.close()

    def startData(self):
        pass


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    root = MainWindow()
    root.show()

    sys.exit(app.exec_())
