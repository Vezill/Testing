#!/usr/bin/python
# -*- encoding: utf-8 -*-

#imports
import os
import sys
import time
from PySide import QtGui, QtCore
import PCANBasic as PCB

def main():
    app = QtGui.QApplication(sys.argv)
    root = MainWindow()
    root.show()
    app.exec_()


class MainWindow(QtGui.QWidget):

    def __init__(self, parent=None):
        #Variables
        self._BAUDRATES = ({
            "1M Bit/s Baud Rate":PCB.PCAN_BAUD_1M,     "800 kBit/s Baud Rate":PCB.PCAN_BAUD_800K,
            "500 kBit/s Baud Rate":PCB.PCAN_BAUD_500K, "250 kBit/s Baud Rate":PCB.PCAN_BAUD_250K,
            "125 kBit/s Baud Rate":PCB.PCAN_BAUD_125K, "100 kBit/s Baud Rate":PCB.PCAN_BAUD_100K,
            "95 kBit/s Baud Rate":PCB.PCAN_BAUD_95K,   "83 kBit/s Baud Rate":PCB.PCAN_BAUD_83K,
            "50 kBit/s Baud Rate":PCB.PCAN_BAUD_50K,   "47 kBit/s Baud Rate":PCB.PCAN_BAUD_47K,
            "33 kBit/s Baud Rate":PCB.PCAN_BAUD_33K,   "20 kBit/s Baud Rate":PCB.PCAN_BAUD_20K,
            "10 kBit/s Baud Rate":PCB.PCAN_BAUD_10K,   "5 kBit/s Baud Rate":PCB.PCAN_BAUD_5K
        })

        self._CHANNELS = ({
            "PCAN USB BUS 1":PCB.PCAN_USBBUS1, "PCAN USB BUS 2":PCB.PCAN_USBBUS2,
            "PCAN USB BUS 3":PCB.PCAN_USBBUS3, "PCAN USB BUS 4":PCB.PCAN_USBBUS4,
            "PCAN USB BUS 5":PCB.PCAN_USBBUS5, "PCAN USB BUS 6":PCB.PCAN_USBBUS6,
            "PCAN USB BUS 7":PCB.PCAN_USBBUS7, "PCAN USB BUS 8":PCB.PCAN_USBBUS8,
            "PCAN PCI BUS 1":PCB.PCAN_PCIBUS1, "PCAN PCI BUS 2":PCB.PCAN_PCIBUS2,
            "PCAN PCI BUS 3":PCB.PCAN_PCIBUS3, "PCAN PCI BUS 4":PCB.PCAN_PCIBUS4,
            "PCAN PCI BUS 5":PCB.PCAN_PCIBUS5, "PCAN PCI BUS 6":PCB.PCAN_PCIBUS6,
            "PCAN PCI BUS 7":PCB.PCAN_PCIBUS7, "PCAN PCI BUS 8":PCB.PCAN_PCIBUS8,
            "PCAN PC BUS 1":PCB.PCAN_PCCBUS1,  "PCAN PC BUS 2":PCB.PCAN_PCCBUS2,
            })

        self._BATTERY = ({
            "Update Lower Half":0, "Update Upper Half":1,
            })

        self._CHUNKSIZE = 6 #Amount of bytes to send before delay
        self._DELAYTIME = 40 #Milliseconds between chunks

        self.pcbObj = PCB.PCANBasic()

        QtGui.QWidget.__init__(self)
        self.setup_ui()

    def setup_ui(self):
        #Window
        self.setWindowTitle("Update Battery Pack Firmware")
        self.resize(500, 200)
        self.setMaximumSize(QtCore.QSize(500, 200))
        self.setMinimumSize(QtCore.QSize(500, 200))

        #Widgets
        self.cbxBaudRate = QtGui.QComboBox()
        self.cbxBaudRate.addItems(self._BAUDRATES.keys())

        self.cbxChannel = QtGui.QComboBox()
        self.cbxChannel.addItems(self._CHANNELS.keys())

        self.cbxBattUpdate = QtGui.QComboBox()
        self.cbxBattUpdate.addItems(self._BATTERY.keys())

        self.btnConnect = QtGui.QPushButton("Connect")
        self.btnConnect.released.connect(self.pcan_connect)

        self.btnSelFile = QtGui.QPushButton("Select File")
        self.btnSelFile.released.connect(self.select_update_file)
        self.btnSelFile.setEnabled(False)

        self.btnUpdRequest = QtGui.QPushButton("Update Request")
        self.btnUpdRequest.released.connect(self.send_update_request)
        self.btnUpdRequest.setEnabled(False)

        self.btnSendFile = QtGui.QPushButton("Send File")
        self.btnSendFile.released.connect(self.send_update_file)
        self.btnSendFile.setEnabled(False)

        self.btnBack = QtGui.QPushButton("Exit")
        self.btnBack.released.connect(self.close_window)

        self.tedConsole = QtGui.QTextEdit()
        self.tedConsole.setReadOnly(True)

        #Layouts
        self.vblControls = QtGui.QVBoxLayout()
        self.vblControls.setSpacing(1)

        self.vblControls.addWidget(self.cbxBaudRate)
        self.vblControls.addWidget(self.cbxChannel)
        self.vblControls.addWidget(self.cbxBattUpdate)
        self.vblControls.addWidget(self.btnConnect)
        self.vblControls.addWidget(self.btnSelFile)
        self.vblControls.addWidget(self.btnUpdRequest)
        self.vblControls.addWidget(self.btnSendFile)
        self.vblControls.addWidget(self.btnBack)

        self.hblWidget = QtGui.QHBoxLayout()
        self.hblWidget.addLayout(self.vblControls)
        self.hblWidget.addWidget(self.tedConsole)

        self.setLayout(self.hblWidget)

    def pcan_connect(self):
        self.tedConsole.insertPlainText("Connection Successfully opened.\n")
        self.btnSelFile.setEnabled(True)

    def pcan_disconnect(self):
        self.tedConsole.insertPlainText("Connection Closed.\n")

    def select_update_file(self):
        self.updFile, _ = QtGui.QFileDialog.getOpenFileName(self, "Open Update File",
            os.path.expanduser(r"~\Downloads"), "*.txt")

        if not os.path.isfile(self.updFile):
            self.tedConsole.insertPlainText("File could not be opened, try again.")
            self.btnUpdRequest.setEnabled(False)

        else:
            self.tedConsole.insertPlainText("File opened successfully.\n")
            self.btnUpdRequest.setEnabled(True)

    def send_update_request(self):
        self.tedConsole.insertPlainText("Update request sent...\n")
        self.tedConsole.insertPlainText("RDY: ")
        self.tedConsole.insertPlainText("\nYou know have 1 minute to send the update.\n")
        self.btnSendFile.setEnabled(True)

    def send_update_file(self):
        self.rdPosition = 0
        self.tmrUpdate = QtCore.QTimer()
        self.trkTime = time.time()
        self.tmrUpdate.singleShot(200, self.send_update_chunk)

    def send_update_chunk(self):
        try:
            with open(self.updFile, "rb") as rfile, open("./textoutput.txt", "ab") as wfile:
                rfile.seek(self.rdPosition, 0)

                for i in xrange(self._CHUNKSIZE):
                    wmsg = []

                    for x in xrange(8):
                        self.rdPosition += 1
                        wmsg.append(rfile.read(1))

                    while len(wmsg) > 0:
                        wfile.write(wmsg.pop(0))

                print self.rdPosition
                self.tedConsole.insertPlainText(".")

                if len(rfile.read(1)):
                    self.tmrUpdate.singleShot(self._DELAYTIME, self.send_update_chunk)

                else:
                    self.tedConsole.insertPlainText(" END\n")
                    self.tedConsole.insertPlainText("Time: %d" % (time.time() - self.trkTime))

        except IOError as e:
            QtGui.QMessageBox.warning(self, "An error occurred.", "You selected an invalid file")
            print str(e)

    def closeEvent(self, event):
        self.close_window()

    def close_window(self):
        try:
            self.pcan_disconnect()
        except Exception:
            pass
        self.close()


if __name__ == "__main__":
    main()