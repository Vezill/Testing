#!/usr/bin/env python
"""
For detailed comments on animation and the techniques used here, see the wiki entry
http://www.scipy.org/Cookbook/Matplotlib/Animations
"""


import numpy as np
import os
import sys
import time
from PySide import QtCore, QtGui

import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4'] = 'PySide'
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas


ITERS = 1000

class BlitQT(FigureCanvas):
    def __init__(self):
        FigureCanvas.__init__(self, Figure())

        self.ax = self.figure.add_subplot(111)
        self.ax.grid()
        self.draw()

        self.old_size = self.ax.bbox.width, self.ax.bbox.height
        self.ax_background = self.copy_from_bbox(self.ax.bbox)
        self.cnt = 0

        self.x = np.arange(0, 2*np.pi, 0.01)
        self.sin_line, = self.ax.plot(self.x, np.sin(self.x), animated=True)
        self.cos_line, = self.ax.plot(self.x, np.cos(self.x), animated=True)
        self.draw()

        self.tstart = time.time()
        self.startTimer(10)

    def timerEvent(self, evt):
        current_size = self.ax.bbox.width, self.ax.bbox.height
        if self.old_size != current_size:
            self.old_size = current_size
            self.ax.clear()
            self.ax.grid()
            self.draw()
            self.ax_background = self.copy_from_bbox(self.ax.bbox)

        self.restore_region(self.ax_background)

        # Update the data
        self.sin_line.set_ydata(np.sin(self.x + self.cnt / 10.0))
        self.cos_line.set_ydata(np.cos(self.x + self.cnt / 10.0))
        # Just draw the animated artist
        self.ax.draw_artist(self.sin_line)
        self.ax.draw_artist(self.cos_line)
        # Just redraw the axes rectangle
        self.blit(self.ax.bbox)

        self.figure.canvas.draw()

        if self.cnt == 0:
            self.draw()

        if self.cnt == ITERS:
            # Print the timing info and quit
            QtGui.QMessageBox.information(self, '', "{0:.0f} fps".format(
                ITERS / (time.time() - self.tstart)))
            sys.exit()
        else:
            self.cnt += 1


if __name__ == '__main__':
    app = QtGui.QApplication([])
    root = BlitQT()
    root.show()

    sys.exit(app.exec_())
