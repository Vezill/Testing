from distutils.core import setup
import glob
import matplotlib
import py2exe

setup(
    name = 'matplotlib animation fps test',
    zipfile = None,

    windows = [{
        'script': 'pyside_animation_blit_qt4_old.py'
    }],

    options = {
        'py2exe': {
            'bundle_files': 1,
            'dist_dir': 'Animation_fps_test',
            'includes': ['matplotlib.backends.backend_qt4agg'],
            'excludes': ['_tkagg', '_gtkagg', '_wxagg', '_pdf', '_gtk', '_cairo', '_gtkcairo', '_tk', 'tk', 'tcl', 'wx']
    }},

    data_files = [
        (r'mpl-data', glob.glob(r'C:\Users\NDonais\AppData\Roaming\Python\Python27\site-packages\matplotlib\mpl-data\*.*')),
        (r'mpl-data', [r'C:\Users\NDonais\AppData\Roaming\Python\Python27\site-packages\matplotlib\mpl-data\matplotlibrc']),
        (r'mpl-data\images', glob.glob(r'C:\Users\NDonais\AppData\Roaming\Python\Python27\site-packages\matplotlib\mpl-data\images\*.*')),
        (r'mpl-data\fonts', glob.glob(r'C:\Users\NDonais\AppData\Roaming\Python\Python27\site-packages\matplotlib\mpl-data\fonts\*.*'))
    ]
)
