#!/usr/bin/env python
#-*- encoding: utf-8 -*-


import sys, os, random
from PySide import QtCore, QtGui
from numpy import arange, sin, pi

import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4'] = 'PySide'
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as QFigureCanvas
from matplotlib.figure import Figure


progname = os.path.basename(sys.argv[0])
progversion = '0.1'


class MyMplCanvas(QFigureCanvas):
    """ Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.) """
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        # We want the axes cleared every time plot() is called
        self.axes.hold(False)

        self.computeInitialFigure()

        QFigureCanvas.__init__(self, fig)
        self.setParent(parent)

        QFigureCanvas.setSizePolicy(self, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        QFigureCanvas.updateGeometry(self)

    def computeInitialFigure(self):
        pass


class MyStaticMplCanvas(MyMplCanvas):
    """ Simple canvas with a sin plot. """
    def computeInitialFigure(self):
        t = arange(0.0, 3.0, 0.01)
        s = sin(2*pi*t)
        self.axes.plot(t, s)


class MyDynamicMplCanvas(MyMplCanvas):
    """ A canvas that updates itself every second with a new plot. """
    def __init__(self, *args, **kwargs):
        MyMplCanvas.__init__(self, *args, **kwargs)
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.updateFigure)
        timer.start(1000)

    def computeInitialFigure(self):
        self.axes.plot([0, 1, 2, 3], [1, 2, 0, 4], 'r')

    def updateFigure(self):
        # build a list of 4 random integers between 0 and 10 (both inclusive)
        l = [random.randint(0, 10) for i in range(4)]
        self.axes.plot([0, 1, 2 ,3], l, 'r')
        self.draw()


class ApplicationWindow(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setWindowTitle("Application Main Window")

        self.file_menu = QtGui.QMenu('&File', self)
        self.file_menu.addAction('&Quit', self.close, QtCore.Qt.CTRL + QtCore.Qt.Key_Q)
        self.menuBar().addMenu(self.file_menu)

        self.help_menu = QtGui.QMenu('&Help', self)
        self.menuBar().addSeparator()
        self.menuBar().addMenu(self.help_menu)
        self.help_menu.addAction('&About', self.about)

        self.main_widget = QtGui.QWidget(self)

        l = QtGui.QVBoxLayout(self.main_widget)
        sc = MyStaticMplCanvas(self.main_widget, width=5, height=4, dpi=100)
        dc = MyDynamicMplCanvas(self.main_widget, width=5, height=4, dpi=100)
        l.addWidget(sc)
        l.addWidget(dc)

        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)

        self.statusBar().showMessage('All hail matplotlib!', 2000)

    def about(self):
        QtGui.QMessageBox(self, 'About',
            """embedding_in_qt4.py example
            Copyright 2005 Florent Rougon, 2006 Darren Dale

            This program is a simple example of a Qt4 application embedding matplotlib
            canvases.

            It may be used and modified with no restriction; raw copies as well as
            modified versions may be distributed without limitation.""")


if __name__ == '__main__':
    app = QtGui.QApplication([])
    root = ApplicationWindow()
    root.show()
    sys.exit(app.exec_())
