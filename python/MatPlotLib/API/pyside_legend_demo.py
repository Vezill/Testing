#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
PySide test version of legend_demo.py
"""

import sys
import numpy as np
from PySide import QtCore, QtGui

import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4'] = 'PySide'
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas


a = np.arange(0, 3, .02)
b = np.arange(0, 3, .02)
c = np.exp(a)
d = c[::-1]

fig = Figure()
ax = fig.add_subplot(111)
ax.plot(a, c, 'k--', a, d, 'k:', a, c+d, 'k')
leg = ax.legend(('Model Length', 'Data Length', 'Total Message Length'), 'upper center',
    shadow=True)
ax.set_ylim([-1, 20])
ax.grid(False)
ax.set_xlabel('Model Complexity --->')
ax.set_ylabel('Message Length --->')
ax.set_title('Minimum Message Length')

ax.set_yticklabels([])
ax.set_xticklabels([])

frame = leg.get_frame()
frame.set_facecolor('0.80')

for t in leg.get_texts():
    t.set_fontsize('small')

for l in leg.get_lines():
    l.set_linewidth(1.5)

app = QtGui.QApplication([])
canvas = FigureCanvas(fig)

win = QtGui.QMainWindow()
win.setCentralWidget(canvas)
win.show()

sys.exit(app.exec_())
