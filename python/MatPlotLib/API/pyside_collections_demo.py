#!/usr/bin/env python
# -*- encoding: utf-8 -*-

import sys
import numpy as np
from PySide import QtCore, QtGui

import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4'] = 'PySide'
from matplotlib import collections, axes, transforms
from matplotlib.colors import colorConverter
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas


nverts = 50
npts = 100

# Make some spirals
r = np.array(range(nverts))
theta = np.array(range(nverts)) * (2 * np.pi) / (nverts - 1)
xx = r * np.sin(theta)
yy = r * np.cos(theta)
spiral = zip(xx, yy)

# Make some offsets
rs = np.random.RandomState([12345678])
xo = rs.randn(npts)
yo = rs.randn(npts)
xyo = zip(xo, yo)

# Make a list of colors cycling through the rgbcmyk series.
colors = [colorConverter.to_rgba(c) for c in ('r', 'g', 'b', 'c', 'y', 'm', 'k')]

fig = Figure()
a = fig.add_subplot(2, 2, 1)
col = collections.LineCollection([spiral], offsets=xyo, transOffset=a.transData)
trans = fig.dpi_scale_trans + transforms.Affine2D().scale(1.0 / 72.0)
col.set_transform(trans)
a.add_collection(col, autolim=True)

# Make a transform for the line segments such that their size is given in points:
col.set_color(colors)
a.autoscale_view()
a.set_title('LineConnection using Offsets')

# The same data as above, but fill the curves.
a = fig.add_subplot(2, 2, 2)
col = collections.PolyCollection([spiral], offsets=xyo, transOffset=a.transData)
trans = transforms.Affine2D().scale(fig.dpi / 72.0)
col.set_transform(trans)
a.add_collection(col, autolim=True)
col.set_color(colors)

a.autoscale_view()
a.set_title('PolyCollection using Offsets')

# 7-sided regular polygons.
a = fig.add_subplot(2, 2, 3)
col = collections.RegularPolyCollection(7, sizes=np.fabs(xx)*10.0, offsets=xyo,
    transOffset=a.transData)
trans = transforms.Affine2D().scale(fig.dpi / 72.0)
col.set_transform(trans)
a.add_collection(col, autolim=True)
a.autoscale_view()
a.set_title('RegularPolyCollection using Offsets')

# Simulate a series of ocean current profiles, successively offset by 0.1 m/s so they form what is
# sometimes called a 'waterfall' plot or a 'stagger' plot
a = fig.add_subplot(2, 2, 4)
nverts = 60
ncurves = 20
offs = (0.1, 0.0)

yy = np.linspace(0, 2 * np.pi, nverts)
ym = np.amax(yy)
xx = (0.2 + (ym - yy) / ym) ** 2 * np.cos(yy - 0.4) * 0.5
segs = []
for i in range(ncurves):
    xxx = xx + 0.02 * rs.randn(nverts)
    curve = zip(xxx, yy * 100)
    segs.append(curve)

col = collections.LineCollection(segs, offsets=offs)
a.add_collection(col, autolim=True)
col.set_color(colors)
a.autoscale_view()
a.set_title('Successive Data Offsets')
a.set_xlabel('Zonal Velocity Component (m/s)')
a.set_ylabel('Depth (m)')
# Reverse the y-axis so depth increases downward
a.set_ylim(a.get_ylim()[::-1])

app = QtGui.QApplication([])
canvas = FigureCanvas(fig)

root = QtGui.QMainWindow()
root.setCentralWidget(canvas)
root.show()

sys.exit(app.exec_())
