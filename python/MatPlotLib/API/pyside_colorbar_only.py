#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
Make a colorbar as a separate figure.
"""


import numpy
import sys
from PySide import QtCore, QtGui

import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4'] = 'PySide'
from matplotlib import mpl
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas


fig = Figure(figsize=(8, 3))
ax1 = fig.add_axes([0.05, 0.65, 0.9, 0.15])
ax2 = fig.add_axes([0.05, 0.25, 0.9, 0.15])

cmap = mpl.cm.cool
norm = mpl.colors.Normalize(vmin=5, vmax=10)

cb1 = mpl.colorbar.ColorbarBase(ax1, cmap=cmap, norm=norm, orientation='horizontal')
cb1.set_label('Some Units')

cmap = mpl.colors.ListedColormap(['r', 'g', 'b', 'c'])
cmap.set_over('0.25')
cmap.set_under('0.75')

bounds = [1, 2, 4, 7, 8]
norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
cb2 = mpl.colorbar.ColorbarBase(ax2, cmap=cmap, norm=norm, boundaries=[0]+bounds+[13],
    extend='both', ticks=bounds, spacing='proportional', orientation='horizontal')
cb2.set_label('Discrete Intervals, Some Other Units')

app = QtGui.QApplication([])
canvas = FigureCanvas(fig)

root = QtGui.QMainWindow()
root.setCentralWidget(canvas)
root.show()

sys.exit(app.exec_())
