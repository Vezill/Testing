#!/usr/bin/env python
#-*- encoding: utf-8 -*-


import random
import sys
from PySide import QtCore, QtGui

import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4'] = 'PySide'
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as QFigureCanvas
from matplotlib.figure import Figure


data = {'curr': [], 'volt': [], 'temp': []}
for i in range(10):
    data['curr'].append([i, random.randint(30, 66)])
    data['volt'].append([i, random.randint(50, 95)])
    data['temp'].append([i, random.randint(20, 38)])

fig = Figure()
fig.subplots_adjust(right=0.75)
canvas = QFigureCanvas(fig)

ax1 = fig.add_subplot(111)
ax2 = ax1.twinx()
ax3 = ax2.twinx()

ax3.spines['right'].set_position(('axes', 1.2))
ax3.set_frame_on(True)
ax3.patch.set_visible(False)
for spine in ax3.spines.itervalues():
    spine.set_visible(False)
ax3.spines['right'].set_visible(True)

p1, = ax1.plot([data['curr'][i][0] for i in range(len(data['curr']))],
               [data['curr'][i][1] for i in range(len(data['curr']))], 'g-', label='Current')
p2, = ax2.plot([data['volt'][i][0] for i in range(len(data['volt']))],
               [data['volt'][i][1] for i in range(len(data['volt']))], 'b-', label='Voltage')
p3, = ax3.plot([data['temp'][i][0] for i in range(len(data['temp']))],
               [data['temp'][i][1] for i in range(len(data['temp']))], 'r-', label='Temperature')

ax1.set_xlim(0, 9)
ax1.set_ylim(30, 66)
ax2.set_ylim(50, 95)
ax3.set_ylim(20, 38)

xlbls = [data['curr'][i][0] for i in range(len(data['curr']))]
ax1.set_xticklabels(xlbls)

ax1.set_xlabel('Time')
ax1.set_ylabel(p1.get_label())
ax2.set_ylabel(p2.get_label())
ax3.set_ylabel(p3.get_label())

ax1.yaxis.label.set_color(p1.get_color())
ax2.yaxis.label.set_color(p2.get_color())
ax3.yaxis.label.set_color(p3.get_color())

ax1.grid(True)

tkw = dict(size=4, width=1.5)
ax1.tick_params(axis='y', colors=p1.get_color(), **tkw)
ax2.tick_params(axis='y', colors=p2.get_color(), **tkw)
ax3.tick_params(axis='y', colors=p3.get_color(), **tkw)
ax1.tick_params(axis='x', **tkw)

lines = [p1, p2, p3]
ax1.legend(lines, [lbl.get_label() for lbl in lines])

canvas.show()
sys.exit(QtGui.qApp.exec_())
