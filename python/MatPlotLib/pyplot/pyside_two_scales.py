#!/usr/bin/env python
#-*- encoding: utf-8 -*-


import numpy
import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4'] = 'PySide'
from matplotlib import pyplot


fig = pyplot.figure()
ax1 = fig.add_subplot(111)
t = numpy.arange(0.01, 10.0, 0.01)
s1 = numpy.exp(t)
ax1.plot(t, s1, 'b-')
ax1.set_xlabel('time (s)')
ax1.set_ylabel('exp', color='b')
for tl in ax1.get_yticklabels():
    tl.set_color('b')

ax2 = ax1.twinx()
s2 = numpy.sin(2*numpy.pi*t)
ax2.plot(t, s2, 'r.')
ax2.set_ylabel('sin', color='r')
for tl in ax2.get_yticklabels():
    tl.set_color('r')

pyplot.show()
