#!/usr/bin/env python
"""
Pure OO (Look Ma, no pylab!) example using the QtAgg backend
"""


import sys
from PySide import QtCore, QtGui

import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4'] = 'PySide'
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as QFigureCanvas
from matplotlib.figure import Figure


fig = Figure()
canvas = QFigureCanvas(fig)
ax = fig.add_subplot(111)
ax.plot([1,2,3])
ax.set_title('hi mom')
ax.grid(True)
ax.set_xlabel('time')
ax.set_ylabel('volts')

canvas.show()
sys.exit(QtGui.qApp.exec_())
