#!/usr/bin/env python
#-*- encoding: utf-8 -*-


import random
from datetime import datetime # can be replaced using QtCore.QTime.currentTime().toPython()
from PySide import QtCore, QtGui

import matplotlib
matplotlib.use('Qt4Agg')
matplotlib.rcParams['backend.qt4'] = 'PySide'
from matplotlib import dates
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as QFigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as QNavigationToolbar
from matplotlib.figure import Figure


class GraphWidget(QFigureCanvas):
    """ Custom Matplotlib graph widget. """
    def __init__(self, data, parent=None):
        self.fig = Figure()
        self.fig.subplots_adjust(left=0.2)

        QFigureCanvas.__init__(self, self.fig)
        self.setParent(parent)

        self.initGraph(data)

    def initGraph(self, data):
        # Set up axes
        self.axes = [None]*3
        self.axes[0] = self.fig.add_subplot(111)
        self.axes[1] = self.axes[0].twinx()
        self.axes[2] = self.axes[1].twinx()

        # Move the second axes over to make labels cleaner
        self.axes[1].spines['left'].set_position(('axes', -0.12))
        self.axes[1].set_frame_on(True)
        self.axes[1].patch.set_visible(False)
        for spine in self.axes[1].spines.itervalues():
            spine.set_visible(False)
        self.axes[1].spines['left'].set_visible(True)

        # Set xaxis format to time
        format = dates.DateFormatter('%H:%M:%S')
        for ax in self.axes:
            #ax.axis.set_major_locator(dates.SecondLocator())
            ax.xaxis.set_major_formatter(format)

        # set up plot data
        curr = [[data['curr'][i][0] for i in range(len(data['curr']))],
            [data['curr'][i][1] for i in range(len(data['curr']))]]
        volt = [[data['volt'][i][0] for i in range(len(data['volt']))],
            [data['volt'][i][1] for i in range(len(data['volt']))]]
        temp = [[data['temp'][i][0] for i in range(len(data['temp']))],
            [data['temp'][i][1] for i in range(len(data['temp']))]]

        self.plots = [None]*3
        self.plots[0], = self.axes[0].plot(curr[0], curr[1], 'b-', label='Current')
        self.plots[1], = self.axes[1].plot(volt[0], volt[1], 'r-', label='Voltage')
        self.plots[2], = self.axes[2].plot(temp[0], temp[1], 'g-', label='Temperature')

        # Set up appearance stuff
        tkw = dict(size=4, width=1.5)
        for i in range(3):
            self.axes[i].set_ylabel(self.plots[i].get_label())
            self.axes[i].yaxis.label.set_color(self.plots[i].get_color())
            self.axes[i].tick_params(axis='y', colors=self.plots[i].get_color(), **tkw)

        self.axes[0].tick_params(axis='x', **tkw)
        self.axes[1].yaxis.set_label_position('left')

        for lbl in self.axes[0].get_xticklabels():
            lbl.set_rotation(25)

        self.axes[0].yaxis.label.set_color(self.plots[0].get_color())
        self.axes[1].yaxis.label.set_color(self.plots[1].get_color())

    def setRanges(self, curr=None, volt=None, temp=None):
        if curr: self.axes[0].set_ylim(curr[0], curr[1])
        if volt: self.axes[1].set_ylim(volt[0], volt[1])
        if temp: self.axes[2].set_ylim(temp[0], temp[1])

        now = QtCore.QTime.currentTime()
        low = datetime.strptime(now.addSecs(-60).toString('HH:mm:ss'), '%H:%M:%S')
        upp = datetime.strptime(now.addSecs(2).toString('HH:mm:ss'), '%H:%M:%S')

        self.axes[0].set_xlim(low, upp)

    def changeVisible(self, plot, state):
        self.plots[plot].set_visible(state)
        self.axes[plot].yaxis.set_visible(state)
        for lbl in self.axes[plot].get_yticklabels():
            lbl.set_visible(state)

        if plot == 1:
            self.axes[1].spines['left'].set_visible(state)

        if self.plots[0].get_visible():
           self.axes[1].spines['left'].set_position(('axes', -0.12))
        else:
            self.axes[1].spines['left'].set_position(('axes', 0))
        self.axes[1].set_ylabel(self.plots[1].get_label())

        self.updateGraph()

    def updateGraph(self, data=None):
        if data:
            curr = [[data['curr'][i][0] for i in range(len(data['curr']))],
                [data['curr'][i][1] for i in range(len(data['curr']))]]
            volt = [[data['volt'][i][0] for i in range(len(data['volt']))],
                [data['volt'][i][1] for i in range(len(data['volt']))]]
            temp = [[data['temp'][i][0] for i in range(len(data['temp']))],
                [data['temp'][i][1] for i in range(len(data['temp']))]]

            self.plots[0].set_data(curr[0], curr[1])
            self.plots[1].set_data(volt[0], volt[1])
            self.plots[2].set_data(temp[0], temp[1])

            self.setRanges()

        self.fig.canvas.draw()


class MainGraphWindow(QtGui.QMainWindow):
    """ Main Window for graph widget. """
    def __init__(self, data):
        """ Data must be in the following format:
            [ [current, min current, max current],
              [voltage, min voltage, max voltage],
              [temperature, min temperature, max temperature] ]
            #TODO: -- FIND A BETTER WAY TO EXPLAIN THIS / IMPLEMENT THIS --
        """
        QtGui.QMainWindow.__init__(self)

        time_str = QtCore.QTime.currentTime().toString('HH:mm:ss')
        now = datetime.strptime(time_str, '%H:%M:%S')

        curr, min_curr, max_curr = data[0]
        volt, min_volt, max_volt = data[1]
        temp, min_temp, max_temp = data[2]

        self.data = dict(curr=[], volt=[], temp=[])
        self.data['curr'].append([now, curr])
        self.data['volt'].append([now, volt])
        self.data['temp'].append([now, temp])

        self.ranges = dict(curr=[min_curr, max_curr], volt=[min_volt, max_volt],
            temp=[min_temp, max_temp])

        self.initUi()

    def initUi(self):
        self.ui_graph_widget = GraphWidget(self.data)
        self.ui_graph_widget.setRanges(curr=self.ranges['curr'], volt=self.ranges['volt'],
            temp=self.ranges['temp'])

        self.ui_nav_tbar = QNavigationToolbar(self.ui_graph_widget, self, coordinates=False)
        actions = self.ui_nav_tbar.actions()
        self.ui_nav_tbar.removeAction(actions[7])
        self.ui_nav_tbar.removeAction(actions[8])
        self.ui_nav_tbar.setMovable(False)

        ui_visible_lbl = QtGui.QLabel('Display:')
        ui_curr_visible_btn = QtGui.QPushButton('Current')
        ui_volt_visible_btn = QtGui.QPushButton('Voltage')
        ui_temp_visible_btn = QtGui.QPushButton('Temperature')

        ui_curr_visible_btn.setCheckable(True)
        ui_curr_visible_btn.setChecked(True)
        ui_volt_visible_btn.setCheckable(True)
        ui_volt_visible_btn.setChecked(True)
        ui_temp_visible_btn.setCheckable(True)
        ui_temp_visible_btn.setChecked(True)

        self.ui_nav_tbar.addSeparator()
        self.ui_nav_tbar.addWidget(ui_visible_lbl)
        self.ui_nav_tbar.addWidget(ui_curr_visible_btn)
        self.ui_nav_tbar.addWidget(ui_volt_visible_btn)
        self.ui_nav_tbar.addWidget(ui_temp_visible_btn)

        self.addToolBar(self.ui_nav_tbar)
        self.setCentralWidget(self.ui_graph_widget)

        ui_curr_visible_btn.toggled.connect(self.changeVisible)
        ui_volt_visible_btn.toggled.connect(self.changeVisible)
        ui_temp_visible_btn.toggled.connect(self.changeVisible)

    def changeVisible(self, state):
        source = self.sender()

        if 'Curr' in source.text():
            plot = 0
        elif 'Volt' in source.text():
            plot = 1
        elif 'Temp' in source.text():
            plot = 2
        else:
            return

        self.ui_graph_widget.changeVisible(plot, state)

    def updateData(self, new_data):
        time_str = QtCore.QTime.currentTime().toString('HH:mm:ss')
        now = datetime.strptime(time_str, '%H:%M:%S')

        for item in self.data:
            if len(item) > 1800:
                item.pop(0)

        self.data['curr'].append([now, new_data[0]])
        self.data['volt'].append([now, new_data[1]])
        self.data['temp'].append([now, new_data[2]])

        self.ui_graph_widget.updateGraph(self.data)

class MainWidget(QtGui.QDialog):
    """ Widget window for testing commands and other dev related functions. """
    def __init__(self):
        QtGui.QDialog.__init__(self)
        self.initUi()

    def initUi(self):
        data = [[8, -50, 50], [3550, 3400, 3800], [25, 10, 30]]
        self.ui_graph_win = MainGraphWindow(data)

        self.ui_graph_display_btn = QtGui.QPushButton('Display Graph')
        self.ui_timer_state_btn = QtGui.QPushButton('Stop Updating')
        ui_exit_btn = QtGui.QPushButton('Exit')

        ui_main_layout = QtGui.QVBoxLayout()
        ui_main_layout.addWidget(self.ui_graph_display_btn)
        ui_main_layout.addWidget(self.ui_timer_state_btn)
        ui_main_layout.addWidget(ui_exit_btn)
        self.setLayout(ui_main_layout)

        self.update_timer = QtCore.QTimer()

        self.ui_graph_display_btn.released.connect(self.displayDataWindow)
        self.ui_timer_state_btn.released.connect(self.setTimerState)
        ui_exit_btn.released.connect(self.close)
        self.update_timer.timeout.connect(self.sendDataUpdate)

    def closeEvent(self, event):
        self.ui_graph_win.close()
        event.accept()

    def displayDataWindow(self):
        self.update_timer.start(2000)
        self.ui_graph_win.show()

    def setTimerState(self):
        if 'Stop' in self.ui_timer_state_btn.text():
            self.update_timer.stop()
            self.ui_timer_state_btn.setText('Start Updating')

        elif 'Start' in self.ui_timer_state_btn.text():
            self.update_timer.start(2000)
            self.ui_timer_state_btn.setText('Stop Updating')

    def sendDataUpdate(self):
        data = [random.randint(0, 8), random.randint(3500, 3600), random.randint(20, 25)]
        self.ui_graph_win.updateData(data)


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication([])
    root = MainWidget()
    root.show()

    sys.exit(app.exec_())
